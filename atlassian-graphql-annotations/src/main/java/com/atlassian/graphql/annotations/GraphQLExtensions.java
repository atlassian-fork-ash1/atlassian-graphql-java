package com.atlassian.graphql.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a class as a graph-ql extension point. The class must implement either:
 * a) GraphQLTypeContributor which is invoked for each graph-ql type.
 * b) GraphQLExtensions to extend the graph-ql type system generally.
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface GraphQLExtensions {
}
