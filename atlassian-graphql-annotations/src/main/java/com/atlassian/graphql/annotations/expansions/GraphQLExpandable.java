package com.atlassian.graphql.annotations.expansions;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a field or field type (such as a Reference class) as an expansion field.
 * An expansion field is one that can be expanded via the 'expand' query parameter on an Atlassian REST API.
 */
@Target({ElementType.FIELD, ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface GraphQLExpandable {

    /**
     * @return the explicit path to use to expand this node.  If not provided,
     * defaults to the current node name.
     */
    String value() default "";

    /**
     * @return Whether to skip this field in an expandable calculation or not.
     */
    boolean skip() default false;
}
