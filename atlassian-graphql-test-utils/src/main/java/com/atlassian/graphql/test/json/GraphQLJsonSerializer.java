package com.atlassian.graphql.test.json;

import com.atlassian.graphql.test.json.spi.GraphQLTestTypeBuilder;
import com.atlassian.graphql.test.json.spi.GraphQLTestTypeBuilderResult;
import com.google.common.collect.Sets;
import com.google.common.reflect.TypeParameter;
import com.google.common.reflect.TypeToken;
import graphql.ExceptionWhileDataFetching;
import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.GraphQLError;
import graphql.execution.ExecutionStrategy;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLOutputType;
import graphql.schema.GraphQLSchema;
import graphql.schema.GraphQLType;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

/**
 * Helper class to serialize a graphql object to Json, using the graphql-java API.
 */
public class GraphQLJsonSerializer {
    private final com.fasterxml.jackson.databind.ObjectMapper fasterxmlObjectMapper;
    private final org.codehaus.jackson.map.ObjectMapper codehausObjectMapper;
    private final EnumSet<GraphQLJsonSerializerOptions> options;

    public GraphQLJsonSerializer(
            final com.fasterxml.jackson.databind.ObjectMapper objectMapper,
            final EnumSet<GraphQLJsonSerializerOptions> options) {

        this.fasterxmlObjectMapper = requireNonNull(objectMapper);
        this.codehausObjectMapper = null;
        this.options = requireNonNull(options);
    }

    public GraphQLJsonSerializer(
            final org.codehaus.jackson.map.ObjectMapper objectMapper,
            final EnumSet<GraphQLJsonSerializerOptions> options) {

        this.fasterxmlObjectMapper = null;
        this.codehausObjectMapper = requireNonNull(objectMapper);
        this.options = requireNonNull(options);
    }

    /**
     * Serialize an object using a graphql query over a graphql type.
     * @param typeBuilder The object that will create the graphql {@link GraphQLOutputType}
     *                    the contains the field fetchers that will ultimately perform the serialization
     * @param type The java-type from which the graphql type will be built from
     * @param obj The object to serialize
     * @return The serialized object in Json format
     * @throws IOException
     */
    public String serializeUsingGraphQL(final GraphQLTestTypeBuilder typeBuilder, final Type type, final Object obj)
            throws IOException {

        requireNonNull(typeBuilder);
        requireNonNull(type);
        requireNonNull(obj);

        if (options.contains(GraphQLJsonSerializerOptions.ADD_ROOT_FIELD)) {
            final Type rootType = rootOf(TypeToken.of(type));
            final Map map = (Map) serializeToObjectUsingGraphQL(typeBuilder, rootType, new Root(obj));
            return serializeJsonObject(map.get("root"));
        } else {
            final Object serialized = serializeToObjectUsingGraphQL(typeBuilder, type, obj);
            return serializeJsonObject(serialized);
        }
    }

    private static <T> Type rootOf(final TypeToken<T> type) {
        return new TypeToken<Root<T>>(){}
               .where(new TypeParameter<T>(){}, type)
               .getType();
    }

    public Object serializeToObjectUsingGraphQL(
            final GraphQLTestTypeBuilder typeBuilder,
            final Type type,
            final Object obj) throws IOException {

        return serializeToObjectUsingGraphQL(typeBuilder, type, obj, null);
    }

    public Object serializeToObjectUsingGraphQL(
            final GraphQLTestTypeBuilder typeBuilder,
            final Type type,
            final Object obj,
            final String query) throws IOException {

        return serializeToObjectUsingGraphQL(typeBuilder, type, obj, query, null);
    }

    public Object serializeToObjectUsingGraphQL(
            final GraphQLTestTypeBuilder typeBuilder,
            final Type type,
            final Object obj,
            String query,
            final ExecutionStrategy queryExecutionStrategy) throws IOException {

        return serializeToObjectUsingGraphQL(typeBuilder, type, null, obj, query, queryExecutionStrategy);
    }

    public Object serializeToObjectUsingGraphQL(
            final GraphQLTestTypeBuilder typeBuilder,
            final Type type,
            final Object context,
            final Object obj,
            String query,
            final ExecutionStrategy queryExecutionStrategy) throws IOException {

        final GraphQLTestTypeBuilderResult typeBuilderResult = typeBuilder.buildType(type);
        return serializeToObjectUsingGraphQL(
                (GraphQLObjectType) typeBuilderResult.getType(),
                typeBuilderResult.getAllTypes(),
                context,
                obj,
                query,
                queryExecutionStrategy);
    }

    public Object serializeToObjectUsingGraphQL(
            final GraphQLObjectType graphqlType,
            final Map<String, GraphQLType> allTypes,
            final Object obj,
            String query,
            final ExecutionStrategy queryExecutionStrategy) throws IOException {

        return serializeToObjectUsingGraphQL(graphqlType, allTypes, null, obj, query, queryExecutionStrategy);
    }

    public Object serializeToObjectUsingGraphQL(
            final GraphQLObjectType graphqlType,
            final Map<String, GraphQLType> allTypes,
            final Object context,
            final Object obj,
            String query,
            final ExecutionStrategy queryExecutionStrategy) throws IOException {

        if (query == null) {
            query = GraphQLQueryBuilder.build(graphqlType, allTypes, null);
        }
        final GraphQLSchema schema =
                GraphQLSchema.newSchema()
                .query(graphqlType)
                .build(Sets.newLinkedHashSet(allTypes.values()));

        final GraphQL graphql = createGraphQL(schema, queryExecutionStrategy);
        final ExecutionResult result = graphql.execute(ExecutionInput.newExecutionInput(query).context(context).root(obj).build());
        throwErrors(result);
        return result.getData();
    }

    private static GraphQL createGraphQL(final GraphQLSchema schema, final ExecutionStrategy queryExecutionStrategy) {
        GraphQL.Builder graphqlBuilder = GraphQL.newGraphQL(schema);
        if (queryExecutionStrategy != null) {
            graphqlBuilder = graphqlBuilder.queryExecutionStrategy(queryExecutionStrategy);
        }
        return graphqlBuilder.build();
    }

    /**
     * Performs the Json serialization of the graphql query result object.
     */
    public String serializeJsonObject(final Object obj) throws IOException {
        if (options.contains(GraphQLJsonSerializerOptions.REMOVE_NULL_VALUES)) {
            removeNullEntries(obj);
        }
        if (fasterxmlObjectMapper != null) {
            return fasterxmlObjectMapper
                    .writer()
                    .with(new com.fasterxml.jackson.core.util.DefaultPrettyPrinter())
                    .writeValueAsString(obj);
        } else {
            return codehausObjectMapper
                    .writer()
                    .withPrettyPrinter(new org.codehaus.jackson.util.DefaultPrettyPrinter())
                    .writeValueAsString(obj);
        }
    }

    /**
     * Remove all null fields from a graphql generated result map, to
     * make it easier to compare with Json (jackson) serialization results.
     */
    private static void removeNullEntries(final Object obj) {
        if (obj instanceof List) {
            for (Object item : (List) obj) {
                removeNullEntries(item);
            }
        } else if (obj instanceof Map) {
            final Map map = (Map) obj;

            final Iterator i = map.entrySet().iterator();
            while (i.hasNext()) {
                final Map.Entry entry = (Map.Entry) i.next();
                removeNullEntries(entry.getValue());

                // remove null entries
                if (entry.getValue() == null) {
                    i.remove();
                }

                // remove entries that have all null fields
                if (entry.getValue() instanceof Map && ((Map) entry.getValue()).isEmpty()) {
                    i.remove();
                }
            }
        }
    }

    private static void throwErrors(ExecutionResult result) {
        // throw any actual Exceptions
        for (final GraphQLError error : result.getErrors()) {
            if (error instanceof ExceptionWhileDataFetching) {
                throw new RuntimeException(((ExceptionWhileDataFetching) error).getException());
            }
        }

        // otherwise report a list of error messages
        final List<String> errorList =
                result.getErrors().stream()
                .map(GraphQLError::getMessage)
                .collect(Collectors.toList());
        final String errors = String.join("\n", errorList);

        if (!errors.isEmpty()) {
            throw new RuntimeException(errors);
        }
    }

    /**
     * Used to implement {@link GraphQLJsonSerializerOptions}.ADD_ROOT_FIELD.
     */
    public static class Root<T> {
        public Root(final T root) {
            this.root = root;
        }

        @org.codehaus.jackson.annotate.JsonProperty
        @com.fasterxml.jackson.annotation.JsonProperty
        private T root;
    }
}
