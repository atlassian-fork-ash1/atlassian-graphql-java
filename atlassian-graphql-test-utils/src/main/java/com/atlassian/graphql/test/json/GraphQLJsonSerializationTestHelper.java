package com.atlassian.graphql.test.json;

import com.atlassian.graphql.test.json.spi.GraphQLTestTypeBuilder;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.EnumSet;
import java.util.function.Function;

import static com.atlassian.graphql.test.utils.ReflectionUtils.getClazz;
import static java.util.Objects.requireNonNull;
import static org.junit.Assert.assertEquals;

/**
 * A helper class for verifying that graphql serialization and Json (jackson) serialization of a java type
 * provide the same Json representation.
 */
public class GraphQLJsonSerializationTestHelper {
    private final com.fasterxml.jackson.databind.ObjectMapper fasterxmlObjectMapper;
    private final org.codehaus.jackson.map.ObjectMapper codehausObjectMapper;
    private final GraphQLTestTypeBuilder typeBuilder;
    private final GraphQLJsonSerializer serializer;

    public GraphQLJsonSerializationTestHelper(final GraphQLTestTypeBuilder typeBuilder) {
        this(typeBuilder, new com.fasterxml.jackson.databind.ObjectMapper());
    }

    public GraphQLJsonSerializationTestHelper(final GraphQLTestTypeBuilder typeBuilder, final com.fasterxml.jackson.databind.ObjectMapper objectMapper) {
        this.typeBuilder = requireNonNull(typeBuilder);
        this.fasterxmlObjectMapper = requireNonNull(objectMapper);
        this.codehausObjectMapper = null;
        this.serializer = new GraphQLJsonSerializer(
                objectMapper,
                EnumSet.of(GraphQLJsonSerializerOptions.ADD_ROOT_FIELD, GraphQLJsonSerializerOptions.REMOVE_NULL_VALUES));
    }

    public GraphQLJsonSerializationTestHelper(final GraphQLTestTypeBuilder typeBuilder, final org.codehaus.jackson.map.ObjectMapper objectMapper) {
        this.typeBuilder = requireNonNull(typeBuilder);
        this.fasterxmlObjectMapper = null;
        this.codehausObjectMapper = requireNonNull(objectMapper);
        this.serializer = new GraphQLJsonSerializer(
                objectMapper,
                EnumSet.of(GraphQLJsonSerializerOptions.ADD_ROOT_FIELD, GraphQLJsonSerializerOptions.REMOVE_NULL_VALUES));
    }

    /**
     * Assert that graphql serialization and Json (jackson) serialization of a java type
     * provide the same Json representation.
     */
    public void assertGraphAndJsonSerializeSame(final Object obj) throws IOException {
        assertGraphAndJsonSerializeSame(obj.getClass(), obj, false, null);
    }

    /**
     * Assert that graphql serialization and Json (jackson) serialization of a java type
     * provide the same Json representation.
     */
    public void assertGraphAndJsonSerializeSame(final Type type, final Object obj) throws IOException {
        assertGraphAndJsonSerializeSame(type, obj, false, null);
    }

    /**
     * Assert that graphql serialization and Json (jackson) serialization of a java type
     * provide the same Json representation. Performs the following steps:
     * - Generates a graphql query over all fields in the graph
     * - Serializes an object using graphql query execution and field fetchers to json
     * - Serializes an object using Json (jackson) annotations to json
     * - Deserializes and re-serializes the resulting json results using Json (jackson) annotations,
     *   so that the resulting json can be compared and match if the objects are of the same structure
     * @param type The java type of the object to serialize
     * @param obj The object to serialize
     * @param afterDeserialize A converter that's run just before re-serialization (to accomodate enrichers and such)
     * @throws IOException
     */
    public void assertGraphAndJsonSerializeSame(
            final Type type,
            final Object obj,
            final boolean deserializeAndSerializeAfter,
            final Function<Object, Object> afterDeserialize) throws IOException {

        assertEquals(
                serializeObjectToJson(type, obj, deserializeAndSerializeAfter, afterDeserialize),
                serializeGraphToJson(type, obj, deserializeAndSerializeAfter, afterDeserialize));
    }

    /**
     * Serialize an object using Json annotations.
     */
    public String serializeObjectToJson(final Type type, final Object obj) throws IOException {
        return serializeObjectToJson(type, obj, false, null);
    }

    /**
     * Serialize an object using Json annotations.
     */
    public String serializeObjectToJson(
            final Type type,
            final Object obj,
            final boolean deserializeAndSerializeAfter,
            final Function<Object, Object> afterDeserialize) throws IOException {

        final String json = serializer.serializeJsonObject(obj);
        return deserializeAndSerializeAfter
               ? deserializeSerialize(json, getClazz(type), afterDeserialize)
               : json;
    }

    /**
     * Serialize an object using graphql query execution and field fetchers.
     */
    public String serializeGraphToJson(final Type type, final Object obj) throws IOException {
        return serializeGraphToJson(type, obj, false, null);
    }

    /**
     * Serialize an object using graphql query execution and field fetchers.
     */
    public String serializeGraphToJson(
            final Type type,
            final Object obj,
            final boolean deserializeAndSerializeAfter,
            final Function<Object, Object> afterDeserialize) throws IOException {

        final String json = serializer.serializeUsingGraphQL(typeBuilder, type, obj);
        return deserializeAndSerializeAfter
               ? deserializeSerialize(json, getClazz(type), afterDeserialize)
               : json;
    }

    private String deserializeSerialize(
            final String json,
            final Class clazz,
            final Function<Object, Object> afterDeserialize) throws IOException {

        Object deserialized =
                fasterxmlObjectMapper != null
                ? fasterxmlObjectMapper.readValue(json, clazz)
                : codehausObjectMapper.readValue(json, clazz);
        if (deserialized == null) {
            return null;
        }
        if (afterDeserialize != null) {
            deserialized = afterDeserialize.apply(deserialized);
        }
        return serializer.serializeJsonObject(deserialized);
    }
}
