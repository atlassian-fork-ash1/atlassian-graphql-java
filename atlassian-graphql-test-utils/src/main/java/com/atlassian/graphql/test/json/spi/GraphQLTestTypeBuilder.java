package com.atlassian.graphql.test.json.spi;

import graphql.schema.GraphQLOutputType;

import java.lang.reflect.Type;

/**
 * Provides the test module with a way to build graph-ql types (without a dependency on atlassian-graphql).
 * This interface is usually implemented to call through to GraphQLTypeBuilder.
 */
public interface GraphQLTestTypeBuilder {
    /**
     * Build a {@link GraphQLOutputType} object from a java type.
     */
    GraphQLTestTypeBuilderResult buildType(final Type type);
}
