package com.atlassian.graphql.test.json;

/**
 * Provides serialization options for {@link GraphQLJsonSerializer}.
 */
public enum GraphQLJsonSerializerOptions {
    /**
     * Provides a surrounding object with a single 'root' field, and serializes the content of the root field.
     * This is necessary when the type being serialized is a scalar, and not an object.
     */
    ADD_ROOT_FIELD,

    /**
     * Removes null fields (and their parents where all fields are null) before serializing the graphql
     * response object to Json.
     */
    REMOVE_NULL_VALUES
}
