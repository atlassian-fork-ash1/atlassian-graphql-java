package com.atlassian.graphql.datafetcher;

import com.atlassian.graphql.GraphQLContext;
import com.atlassian.graphql.GraphQLTestTypeBuilderImpl;
import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.annotations.GraphQLNonNull;
import com.atlassian.graphql.test.json.GraphQLJsonSerializer;
import com.atlassian.graphql.test.json.GraphQLJsonSerializerOptions;
import com.atlassian.graphql.utils.AsyncExecutionStrategyWithExecutionListenerSupport;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.EnumSet;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class MethodDataFetcher_ResolveParentFieldArgumentsTest {
    private final GraphQLJsonSerializer serializer = new GraphQLJsonSerializer(
            new ObjectMapper(),
            EnumSet.noneOf(GraphQLJsonSerializerOptions.class));

    @SuppressWarnings("unchecked")
    @Test
    public void testParentFieldArguments() throws Exception {
        final ParentObject obj = new ParentObject();
        final GraphQLContext graphqlContext = new GraphQLContext();

        final String query =
               ("{\n" +
                "  child(parentArg: 'parent') {\n" +
                "    grandChild(childArg: 'child') {\n" +
                "      field(grandChildArg: 'grandchild')\n" +
                "    }\n" +
                "  }\n" +
                "}\n").replace("'", "\"");

        final Map<String, Map<String, Map<String, Object>>> response = (Map) serializer.serializeToObjectUsingGraphQL(
                new GraphQLTestTypeBuilderImpl(),
                ParentObject.class,
                graphqlContext,
                obj,
                query,
                new AsyncExecutionStrategyWithExecutionListenerSupport());
        assertEquals("parent_child_grandchild", response.get("child").get("grandChild").get("field"));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testNonNullParentFieldArguments() throws Exception {
        final NonNullParentObject obj = new NonNullParentObject();
        final GraphQLContext graphqlContext = new GraphQLContext();

        final String query =
                ("{\n" +
                        "  childNonNull(parentArg: 'parent') {\n" +
                        "    grandChild(childArg: 'child') {\n" +
                        "      field(grandChildArg: 'grandchild')\n" +
                        "    }\n" +
                        "  }\n" +
                        "}\n").replace("'", "\"");

        final Map<String, Map<String, Map<String, Object>>> response = (Map) serializer.serializeToObjectUsingGraphQL(
                new GraphQLTestTypeBuilderImpl(),
                NonNullParentObject.class,
                graphqlContext,
                obj,
                query,
                new AsyncExecutionStrategyWithExecutionListenerSupport());
        assertEquals("parent_child_grandchild", response.get("childNonNull").get("grandChild").get("field"));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testParentFieldArgumentsNotFound() throws Exception {
        final ChildObject obj = new ChildObject();
        final GraphQLContext graphqlContext = new GraphQLContext();

        final String query =
                ("{\n" +
                 "    grandChild(childArg: 'child') {\n" +
                 "      field(grandChildArg: 'grandchild')\n" +
                 "    }\n" +
                 "}\n").replace("'", "\"");

        final Map<String, Map<String, Object>> response = (Map) serializer.serializeToObjectUsingGraphQL(
                new GraphQLTestTypeBuilderImpl(),
                ChildObject.class,
                graphqlContext,
                obj,
                query,
                new AsyncExecutionStrategyWithExecutionListenerSupport());
        assertEquals("null_child_grandchild", response.get("grandChild").get("field"));
    }

    public static class ParentObject {
        @GraphQLName("child")
        public ChildObject parentMethod(@GraphQLName("parentArg") String parentArg) {
            return new ChildObject();
        }
    }

    public static class NonNullParentObject {
        @GraphQLName("childNonNull")
        @GraphQLNonNull
        public ChildObject parentMethod(@GraphQLName("parentArg") String parentArg) {
            return new ChildObject();
        }
    }

    public static class ChildObject {
        @GraphQLName("grandChild")
        public GrandChildObject childMethod(@GraphQLName("childArg") String childArgument) {
            return new GrandChildObject();
        }
    }

    public static class GrandChildObject {
        @GraphQLName("field")
        public String grandChildMethod(
                @GraphQLName("../../parentArg") String parentArg,
                @GraphQLName("../childArg") String childArg,
                @GraphQLName("grandChildArg") String grandChildArg) {

            return parentArg + "_" + childArg + "_" + grandChildArg;
        }
    }
}
