package com.atlassian.graphql.rest;


import com.atlassian.graphql.GraphQLContext;
import com.atlassian.graphql.spi.GraphQLProviders;
import com.atlassian.graphql.annotations.GraphQLBatchLoader;
import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.annotations.GraphQLProvider;
import org.dataloader.BatchLoader;
import org.dataloader.DataLoaderRegistry;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;

public class GraphQLRestServerWithDataLoadersTest {

    @SuppressWarnings("unchecked")
    @Test
    public void integrationTest() throws IOException {

        MyProvider provider = new MyProvider();
        GraphQLRestServer restServer = GraphQLRestServer.builder()
                .provider(new GraphQLProviders("", provider))
                .build();
        Map<String, String> result = (Map<String, String>) restServer.execute(new GraphQLRestRequest("{f1: foo(id:1)\nf2: foo(id:2) }"), new GraphQLContext()).get("data");
        assertEquals(2, result.size());
        assertEquals("id:1", result.get("f1"));
        assertEquals("id:2", result.get("f2"));
        assertEquals(1, provider.callCount.get());
    }

    @GraphQLProvider
    public static class MyProvider {

        AtomicInteger callCount = new AtomicInteger();

        @GraphQLName("foo")
        public CompletableFuture<String> foo(@GraphQLName("id") int id, DataLoaderRegistry registry) {
            return registry.<Integer, String>getDataLoader("blah").load(id);
        }

        @GraphQLBatchLoader("blah")
        public BatchLoader<Integer, String> loadById() {
            return keys -> {
                callCount.incrementAndGet();
                return CompletableFuture.completedFuture(
                        keys.stream().map(id -> "id:" + id).collect(toList()));
            };
        }

    }
}
