package com.atlassian.graphql.spi;

import com.google.common.collect.Lists;
import graphql.schema.DataFetcher;
import org.junit.Test;

import java.lang.reflect.Member;
import java.lang.reflect.Type;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class CombinedGraphQLExtensionsTest {
    @Test
    public void testGetExpansionRootPaths() {
        final GraphQLExtensions extensions1 = new GraphQLExtensions() {
            @Override
            public List<String> getExpansionRootPaths(final Type responseType) {
                return Lists.newArrayList("nodes");
            }
        };
        final GraphQLExtensions extensions2 = new GraphQLExtensions() {
            @Override
            public List<String> getExpansionRootPaths(final Type responseType) {
                return Lists.newArrayList("edges");
            }
        };
        assertEquals(Lists.newArrayList("nodes", "edges"), CombinedGraphQLExtensions.combine(extensions1, extensions2).getExpansionRootPaths(null));
    }

    @Test
    public void testGetExpansionRootPaths_Null() {
        final GraphQLExtensions extensions = new GraphQLExtensions() {
            @Override
            public List<String> getExpansionRootPaths(final Type responseType) {
                return null;
            }
        };
        assertEquals(null, CombinedGraphQLExtensions.combine(extensions, extensions).getExpansionRootPaths(null));
    }

    @Test
    public void testGetExpansionRootPaths_RemoveDuplicates() {
        final GraphQLExtensions extensions = new GraphQLExtensions() {
            @Override
            public List<String> getExpansionRootPaths(final Type responseType) {
                return Lists.newArrayList("nodes");
            }
        };
        assertEquals(Lists.newArrayList("nodes"), CombinedGraphQLExtensions.combine(extensions, extensions).getExpansionRootPaths(null));
    }

    @Test
    public void testGetExpansionRootPaths_NullAndRootPaths() {
        final GraphQLExtensions extensions1 = new GraphQLExtensions() {
            @Override
            public List<String> getExpansionRootPaths(final Type responseType) {
                return null;
            }
        };
        final GraphQLExtensions extensions2 = new GraphQLExtensions() {
            @Override
            public List<String> getExpansionRootPaths(final Type responseType) {
                return Lists.newArrayList("nodes");
            }
        };
        assertEquals(Lists.newArrayList("nodes"), CombinedGraphQLExtensions.combine(extensions1, extensions2).getExpansionRootPaths(null));
        assertEquals(Lists.newArrayList("nodes"), CombinedGraphQLExtensions.combine(extensions2, extensions1).getExpansionRootPaths(null));
    }

    @Test
    public void testGetDataFetcherThunk() throws Exception {
        final GraphQLExtensions extensions1 = new GraphQLExtensions() {
            @Override
            public DataFetcher getDataFetcherThunk(final Type type, final Member accessor, final DataFetcher dataFetcher) {
                return env -> dataFetcher.get(null) + "2";
            }
        };
        final GraphQLExtensions extensions2 = new GraphQLExtensions() {
            @Override
            public DataFetcher getDataFetcherThunk(final Type type, final Member accessor, final DataFetcher dataFetcher) {
                return env -> dataFetcher.get(null) + "3";
            }
        };
        final GraphQLExtensions extensions = CombinedGraphQLExtensions.combine(extensions1, extensions2);
        final DataFetcher dataFetcher = extensions.getDataFetcherThunk(null, null, env -> "1");
        assertEquals("123", dataFetcher.get(null));
    }

    @Test
    public void testCombineSubclassOfCombinedGraphQLExtensions() {
        final CombinedGraphQLExtensions extensions1 = new CombinedGraphQLExtensions(Lists.newArrayList()) {
            @Override
            public String contributeTypeName(final String typeName, final Type type, final GraphQLTypeBuilderContext context) {
                if (typeName.equals("type1")) {
                    return "type1_override";
                }
                return null;
            }
        };

        final CombinedGraphQLExtensions extensions2 = new CombinedGraphQLExtensions(Lists.newArrayList()) {
            @Override
            public String contributeTypeName(final String typeName, final Type type, final GraphQLTypeBuilderContext context) {
                if (typeName.equals("type2")) {
                    return "type2_override";
                }
                return null;
            }
        };

        final GraphQLExtensions extensions = CombinedGraphQLExtensions.combine(extensions1, extensions2);
        assertEquals("extensions1 method override used", "type1_override", extensions.contributeTypeName("type1", null, null));
        assertEquals("extensions2 method override used", "type2_override", extensions.contributeTypeName("type2", null, null));
    }
}
