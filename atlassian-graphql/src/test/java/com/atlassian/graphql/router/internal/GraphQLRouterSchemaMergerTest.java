package com.atlassian.graphql.router.internal;

import com.atlassian.graphql.router.GraphQLRouteRules;
import com.atlassian.graphql.router.LocalGraphQLRoute;
import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.common.io.CharStreams;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class GraphQLRouterSchemaMergerTest {
    @Mock
    private GraphQLSchema graphQLSchema;

    @Test
    @SuppressWarnings("unchecked")
    public void testMergeSchemasWithInlineQueryFields() throws Exception {
        final ObjectMapper mapper = new ObjectMapper();
        final GraphQLRouterSchemaMerger merger = new GraphQLRouterSchemaMerger("ApplicationQueryType");

        final Map localSchema = mapper.readValue(new StringReader(loadResource("localSchema.txt")), Map.class);
        final Map confluenceSchema = mapper.readValue(new StringReader(loadResource("confluenceSchema.txt")), Map.class);

        final LocalGraphQLRoute localRoute = new LocalGraphQLRoute(GraphQLRouteRules.builder().allFields().build(), GraphQL.newGraphQL(graphQLSchema).build());
        final LocalGraphQLRoute confluenceRoute = new LocalGraphQLRoute(GraphQLRouteRules.builder().allFields().build(), graphQLSchema);
        final Map<String, Object> combinedSchema = merger.mergeSchemas(
                Lists.newArrayList((Map<String, Object>) localSchema.get("data"), (Map<String, Object>) confluenceSchema.get("data")),
                Lists.newArrayList(localRoute, confluenceRoute));

        final StringWriter writer = new StringWriter();
        mapper.writerWithDefaultPrettyPrinter().writeValue(writer, combinedSchema);
        assertEquals(loadResource("expectedInlineQueryFields.txt"), writer.toString());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testMergeSchemasWithDedicatedQueryField() throws Exception {
        final ObjectMapper mapper = new ObjectMapper();
        final GraphQLRouterSchemaMerger merger = new GraphQLRouterSchemaMerger("ApplicationQueryType");

        final Map localSchema = mapper.readValue(new StringReader(loadResource("localSchema.txt")), Map.class);
        final Map confluenceSchema = mapper.readValue(new StringReader(loadResource("confluenceSchema.txt")), Map.class);

        final LocalGraphQLRoute localRoute = new LocalGraphQLRoute(GraphQLRouteRules.builder().allFields().build(), GraphQL.newGraphQL(graphQLSchema).build());
        final LocalGraphQLRoute confluenceRoute = new LocalGraphQLRoute(
                GraphQLRouteRules.builder()
                .allFields()
                .rootFieldName("confluence")
                .build(), graphQLSchema);
        final Map<String, Object> combinedSchema = merger.mergeSchemas(
                Lists.newArrayList((Map<String, Object>) localSchema.get("data"), (Map<String, Object>) confluenceSchema.get("data")),
                Lists.newArrayList(localRoute, confluenceRoute));

        final StringWriter writer = new StringWriter();
        mapper.writerWithDefaultPrettyPrinter().writeValue(writer, combinedSchema);
        assertEquals(loadResource("expectedDedicatedQueryField.txt"), writer.toString());
    }

    private static String loadResource(final String name) throws IOException {
        final String resourceName = "com/atlassian/graphql/router/internal/" + name;
        final InputStream stream = GraphQLRouterSchemaMergerTest.class.getClassLoader().getResourceAsStream(resourceName);
        if (stream == null) {
            throw new IllegalArgumentException("Failed to find test resource '" + name + "'");
        }
        return CharStreams.toString(new InputStreamReader(stream, Charsets.UTF_8));
    }
}
