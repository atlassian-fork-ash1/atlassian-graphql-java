package com.atlassian.graphql.utils;

import com.google.common.reflect.TypeToken;
import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import static com.atlassian.graphql.utils.ReflectionUtils.getClazz;
import static com.atlassian.graphql.utils.ReflectionUtils.getRealFieldType;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class ReflectionUtilsTest {
    @Test
    public void testGetFieldType() throws Exception {
        Type fieldType = assertGetRealFieldType(Obj.class, "field", NestedObj.class, String.class, Integer.class, Long.class);
        assertGetRealFieldType(fieldType, "value", String.class);
        assertGetRealFieldType(fieldType, "list", List.class, String.class);
        assertGetRealFieldType(fieldType, "listOfMap", List.class, new TypeToken<Map<Integer, Long>>(){}.getType());
    }

    @Test
    public void testGetFieldTypeFromSuperTypeGenericVar() throws Exception {
        assertGetRealFieldType(Subclass.class, BaseClass.class, "list", List.class, String.class);
    }

    private static Type assertGetRealFieldType(final Type declaringType, final String fieldName, final Class expectedRawType, final Type... expectedParameters) throws Exception {
        return assertGetRealFieldType(declaringType, declaringType, fieldName, expectedRawType, expectedParameters);
    }

    private static Type assertGetRealFieldType(final Type declaringType, final Type typeWithField, final String fieldName, final Class expectedRawType, final Type... expectedParameters) throws Exception {
        final Field field = getClazz(typeWithField).getField(fieldName);
        final Type fieldType = getRealFieldType(declaringType, field);
        assertParameterizedType(fieldType, expectedRawType, expectedParameters);
        return fieldType;
    }

    private static void assertParameterizedType(final Type type, final Class expectedRawType, final Type... expectedParameters) {
        if (expectedParameters.length > 0) {
            assertEquals("Type must be a ParameterizedType", true, type instanceof ParameterizedType);
            final ParameterizedType parameterizedType = (ParameterizedType) type;
            assertEquals("rawType", expectedRawType, parameterizedType.getRawType());
            assertArrayEquals("actualTypeArguments", expectedParameters, parameterizedType.getActualTypeArguments());
        }
        assertEquals(expectedRawType, expectedRawType);
    }

    private static class BaseClass<T> {
        public List<T> list;
    }

    private static class Subclass extends BaseClass<String> {
    }

    private static class Obj {
        public NestedObj<String, Integer, Long> field;
    }

    private static class NestedObj<T, TKey, TValue> {
        public T value;
        public List<T> list;
        public List<Map<TKey, TValue>> listOfMap;
    }
}
