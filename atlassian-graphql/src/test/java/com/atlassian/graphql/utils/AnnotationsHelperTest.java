package com.atlassian.graphql.utils;

import com.atlassian.graphql.annotations.GraphQLIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AnnotationsHelperTest {
    @Test
    public void testGetAnnotation_JsonTypeInfo() {
        final JsonTypeInfo typeInfo = AnnotationsHelper.getAnnotation(TestObject.class, JsonTypeInfo.class);
        assertEquals("type", "type", typeInfo.property());
        assertEquals("use", JsonTypeInfo.Id.NAME, typeInfo.use());
        assertEquals("include", JsonTypeInfo.As.PROPERTY, typeInfo.include());
    }

    @Test
    public void testGetAnnotation_JsonSubTypes() {
        final JsonSubTypes subtypes = AnnotationsHelper.getAnnotation(TestObject.class, JsonSubTypes.class);
        assertEquals("name", "subclass", subtypes.value()[0].name());
        assertEquals("value", Object.class, subtypes.value()[0].value());
    }

    @Test
    public void testGetAnnotationIncludingInherited() throws Exception {
        assertNotNull(AnnotationsHelper.getAnnotationIncludingInherited(
                null, Subclass.class.getMethod("field"), GraphQLIgnore.class));
        assertNotNull(AnnotationsHelper.getAnnotationIncludingInherited(
                null, ClassWithInterface.class.getMethod("field"), GraphQLIgnore.class));
        assertNotNull(AnnotationsHelper.getAnnotationIncludingInherited(
                Subclass.class, BaseBaseClass.class.getMethod("field"), GraphQLIgnore.class));
    }

    @com.atlassian.graphql.mock.jackson.annotate.JsonTypeInfo(
            use = com.atlassian.graphql.mock.jackson.annotate.JsonTypeInfo.Id.NAME,
            include = com.atlassian.graphql.mock.jackson.annotate.JsonTypeInfo.As.PROPERTY,
            property = "type"
    )
    @com.atlassian.graphql.mock.jackson.annotate.JsonSubTypes({
            @com.atlassian.graphql.mock.jackson.annotate.JsonSubTypes.Type(
                    name = "subclass", value = Object.class)})
    public static class TestObject {
    }

    public static abstract class BaseBaseClass {
        public abstract void field();
    }

    public static abstract class BaseClass {
        @GraphQLIgnore
        public abstract void field();
    }

    public static class Subclass extends BaseClass {
        @Override
        public void field() {
        }
    }

    public interface BaseInterface {
        @GraphQLIgnore
        public abstract void field();
    }

    public static class ClassWithInterface implements BaseInterface {
        @Override
        public void field() {
        }
    }
}
