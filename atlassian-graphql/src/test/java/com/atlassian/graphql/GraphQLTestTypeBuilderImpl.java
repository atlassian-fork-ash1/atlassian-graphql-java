package com.atlassian.graphql;

import com.atlassian.graphql.json.types.JsonRootGraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLExtensions;
import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.atlassian.graphql.test.json.spi.GraphQLTestTypeBuilder;
import com.atlassian.graphql.test.json.spi.GraphQLTestTypeBuilderResult;

import java.lang.reflect.Type;

public class GraphQLTestTypeBuilderImpl implements GraphQLTestTypeBuilder {
    private GraphQLTypeBuilder typeBuilder;
    private final GraphQLExtensions extensions;

    public GraphQLTestTypeBuilderImpl() {
        this((GraphQLExtensions) null);
    }

    public GraphQLTestTypeBuilderImpl(final GraphQLExtensions extensions) {
        this(new JsonRootGraphQLTypeBuilder(extensions), extensions);
    }

    public GraphQLTestTypeBuilderImpl(final GraphQLTypeBuilder typeBuilder) {
        this(typeBuilder, null);
    }

    public GraphQLTestTypeBuilderImpl(final GraphQLTypeBuilder typeBuilder, final GraphQLExtensions extensions) {
        this.typeBuilder = typeBuilder;
        this.extensions = extensions;
    }

    public GraphQLTestTypeBuilderResult buildType(final Type type) {
        final GraphQLTypeBuilderContext context = new GraphQLTypeBuilderContext(typeBuilder, extensions);
        return new GraphQLTestTypeBuilderResult(typeBuilder.buildType(type, context), context.getTypes());
    }
}
