package com.atlassian.graphql.provider;


import com.atlassian.graphql.GraphQLContext;
import com.atlassian.graphql.spi.GraphQLProviders;
import com.atlassian.graphql.annotations.GraphQLBatchLoader;
import com.atlassian.graphql.annotations.GraphQLProvider;
import com.atlassian.graphql.spi.GraphQLExtensions;
import graphql.ExecutionInput;
import graphql.execution.instrumentation.ChainedInstrumentation;
import graphql.execution.instrumentation.Instrumentation;
import graphql.execution.instrumentation.InstrumentationContext;
import graphql.execution.instrumentation.SimpleInstrumentation;
import graphql.execution.instrumentation.parameters.InstrumentationExecutionParameters;
import graphql.schema.GraphQLSchema;
import org.dataloader.BatchLoader;
import org.dataloader.DataLoaderRegistry;
import org.junit.Test;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class DataLoaderGraphQLExceptionsTest {

    @Test
    public void loadRegistrationsFromProviders() {
        DataLoaderGraphQLExtensions extensions = new DataLoaderGraphQLExtensions(emptyList(), emptyList());
        List<Consumer<DataLoaderRegistry>> consumers = extensions.loadRegistrationsFromProviders(singletonList(
                new SimpleProvider()));

        DataLoaderRegistry registry = new DataLoaderRegistry();
        consumers.forEach(c -> c.accept(registry));

        assertEquals(2, registry.getDataLoaders().size());
        assertTrue(registry.getDataLoader("blah") != null);
        assertTrue(registry.getDataLoader("another") != null);
    }

    @Test
    public void loadRegistrationsFromExtensions() {
        DataLoaderGraphQLExtensions extensions = new DataLoaderGraphQLExtensions(emptyList(), emptyList());
        List<Consumer<DataLoaderRegistry>> consumers = extensions.loadRegistrationsFromProviders(singletonList(
                new SimpleExtension()));

        DataLoaderRegistry registry = new DataLoaderRegistry();
        consumers.forEach(c -> c.accept(registry));

        assertEquals(1, registry.getDataLoaders().size());
        assertTrue(registry.getDataLoader("extensionLoader") != null);
    }

    @Test
    public void populateDataLoaderRegistryNoProviders() {
        DataLoaderRegistry registry = new DataLoaderRegistry();
        GraphQLContext context = new GraphQLContext();
        DataLoaderGraphQLExtensions extensions = new DataLoaderGraphQLExtensions(emptyList(), emptyList());
        extensions.populateDataLoaderRegistry(registry, context);
        assertTrue(context.getInjectedParameterValue(DataLoaderRegistry.class) == null);
    }

    @Test
    public void populateDataLoaderRegistryWithProviders() {
        DataLoaderRegistry registry = new DataLoaderRegistry();
        GraphQLContext context = new GraphQLContext();
        DataLoaderGraphQLExtensions extensions = new DataLoaderGraphQLExtensions(emptyList(), singletonList(
                new GraphQLProviders("foo", new SimpleProvider())));
        extensions.populateDataLoaderRegistry(registry, context);
        assertTrue(context.getRegistry() != null);
        assertTrue(context.getInjectedParameterValue(DataLoaderRegistry.class) == registry);
    }

    @Test(expected = IllegalArgumentException.class)
    public void loadRegistrationsFromProvidersWithDuplicates() {
        DataLoaderGraphQLExtensions extensions = new DataLoaderGraphQLExtensions(emptyList(), emptyList());
        extensions.loadRegistrationsFromProviders(singletonList(
                new DuplicateProvider()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void populateDataLoaderRegistryWithWrongContext() {
        DataLoaderRegistry registry = new DataLoaderRegistry();
        DataLoaderGraphQLExtensions extensions = new DataLoaderGraphQLExtensions(emptyList(), emptyList());
        extensions.populateDataLoaderRegistry(registry, new Object());
    }

    @Test
    public void getInstrumentationWithProviders() {
        DataLoaderGraphQLExtensions extensions = new DataLoaderGraphQLExtensions(emptyList(), singletonList(
                new GraphQLProviders("foo", new SimpleProvider())));
        Instrumentation inst = extensions.getInstrumentation();
        assertTrue(inst instanceof ChainedInstrumentation);

        ExecutionInput input = mock(ExecutionInput.class);
        GraphQLContext context = new GraphQLContext();
        when(input.getContext()).thenReturn(context);
        InstrumentationContext instrContext  = inst.beginExecution(new InstrumentationExecutionParameters(input, mock(GraphQLSchema.class), new ChainedInstrumentation(emptyList()).createState(null)));

        assertTrue(instrContext != null);
    }

    @Test
    public void getInstrumentationWithNoProviders() {
        DataLoaderGraphQLExtensions extensions = new DataLoaderGraphQLExtensions(emptyList(), emptyList());
        Instrumentation inst = extensions.getInstrumentation();
        assertTrue(inst == SimpleInstrumentation.INSTANCE);
    }

    @GraphQLProvider
    public static class SimpleProvider {

        @GraphQLBatchLoader("blah")
        public BatchLoader<Integer, String> loadById() {
            return keys -> CompletableFuture.completedFuture(
                    keys.stream().map(id -> "id:" + id).collect(toList()));
        }

        @GraphQLBatchLoader
        public BatchLoader<Integer, String> another() {
            return keys -> CompletableFuture.completedFuture(
                    keys.stream().map(id -> "id:" + id).collect(toList()));
        }
    }

    public static class SimpleExtension implements GraphQLExtensions {

        @GraphQLBatchLoader
        public BatchLoader<Integer, String> extensionLoader() {
            return keys -> CompletableFuture.completedFuture(
                    keys.stream().map(id -> "id:" + id).collect(toList()));
        }
    }

    @GraphQLProvider
    public static class DuplicateProvider {

        @GraphQLBatchLoader("blah")
        public BatchLoader<Integer, String> loadById() {
            return keys -> CompletableFuture.completedFuture(
                    keys.stream().map(id -> "id:" + id).collect(toList()));
        }

        @GraphQLBatchLoader("blah")
        public BatchLoader<Integer, String> another() {
            return keys -> CompletableFuture.completedFuture(
                    keys.stream().map(id -> "id:" + id).collect(toList()));
        }
    }
}
