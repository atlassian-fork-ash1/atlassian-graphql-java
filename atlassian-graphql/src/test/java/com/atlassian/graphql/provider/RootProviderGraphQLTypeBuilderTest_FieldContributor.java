package com.atlassian.graphql.provider;

import com.atlassian.graphql.annotations.GraphQLExtensions;
import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.spi.GraphQLProviders;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.atlassian.graphql.spi.GraphQLTypeContributor;
import com.google.common.collect.Lists;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLSchema;
import org.junit.Test;

import java.lang.reflect.Type;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class RootProviderGraphQLTypeBuilderTest_FieldContributor {
    @Test
    public void testFieldContributorProvider() {
        final RootProviderGraphQLTypeBuilder providerTypeBuilder = new RootProviderGraphQLTypeBuilder();
        final List<GraphQLProviders> providers = Lists.newArrayList(new GraphQLProviders(
                "field",
                Lists.newArrayList(new Provider()),
                com.atlassian.graphql.spi.GraphQLExtensions.of(new TestFieldContributorProvider())));

        final GraphQLSchema schema = providerTypeBuilder.buildSchema("rootQuery", "rootMutation", providers, null);
        final GraphQLObjectType type = (GraphQLObjectType) schema.getQueryType().getFieldDefinitions().get(0).getType();
        assertEquals("TestEntity", type.getName());
        assertEquals(2, type.getFieldDefinitions().size());
        assertEquals("newField", type.getFieldDefinitions().get(0).getName());
        assertEquals("field", type.getFieldDefinitions().get(1).getName());
    }

    public static class Provider {
        @GraphQLName
        public TestEntity field() {
            return new TestEntity();
        }
    }

    @GraphQLExtensions
    public static class TestFieldContributorProvider implements GraphQLTypeContributor {
        @Override
        public String contributeTypeName(
                final String typeName,
                final Type type,
                final GraphQLTypeBuilderContext context) {

            return null;
        }

        @Override
        public void contributeFields(
                final String typeName,
                final Type type,
                final List<GraphQLFieldDefinition> fields,
                final GraphQLTypeBuilderContext context) {

            // attach fields to all TestEntity entities
            if (!context.isCurrentType(TestEntity.class)) {
                return;
            }

            // build the 'addedField' field using this object as the provider
            fields.addAll(context.buildProviderGraphQLType("query", this).getFieldDefinitions());
        }

        @GraphQLName("newField")
        public String newField() {
            return "newField";
        }
    }

    public static class TestEntity {
        @GraphQLName
        public String field() {
            return "field";
        }
    }
}
