package com.atlassian.graphql.json.types;

import com.atlassian.graphql.GraphQLTestTypeBuilderImpl;
import com.atlassian.graphql.annotations.GraphQLIDType;
import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.test.json.GraphQLJsonSerializer;
import com.atlassian.graphql.test.json.GraphQLJsonSerializerOptions;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import com.google.common.reflect.TypeToken;
import graphql.Scalars;
import graphql.schema.GraphQLOutputType;
import graphql.schema.idl.SchemaPrinter;
import org.junit.Test;

import java.util.EnumSet;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class JsonValueOnMethodTypeBuilderTest {
    private final GraphQLJsonSerializer serializer = new GraphQLJsonSerializer(
            new ObjectMapper(),
            EnumSet.of(GraphQLJsonSerializerOptions.ADD_ROOT_FIELD));

    @Test
    public void testSerialize() throws Exception {
        final ClassWithJsonValue obj = new ClassWithJsonValue("fieldValue");
        assertEquals(
                "\"fieldValue\"",
                serializer.serializeUsingGraphQL(new GraphQLTestTypeBuilderImpl(), ClassWithJsonValue.class, obj));
    }

    @Test
    public void testSerializeMapWithJsonValueAsValue() throws Exception {
        final Map<String, ClassWithJsonValue> map = ImmutableMap.of("key", new ClassWithJsonValue("jsonvalue"));
        assertEquals(
                "[ {\n" +
                "  \"key\" : \"key\",\n" +
                "  \"value\" : \"jsonvalue\"\n" +
                "} ]",
                serializer.serializeUsingGraphQL(new GraphQLTestTypeBuilderImpl(), new TypeToken<Map<String, ClassWithJsonValue>>(){}.getType(), map));
    }

    @Test
    public void testUseAsInputType() throws Exception {
        final JsonRootGraphQLTypeBuilder typeBuilder = new JsonRootGraphQLTypeBuilder();
        final GraphQLOutputType type = (GraphQLOutputType) typeBuilder.buildType(ProviderWithJsonValueInput.class);
        assertEquals(
                "type ProviderWithJsonValueInput {\n" +
                "  query(input: String): String\n" +
                "}", new SchemaPrinter().print(type).trim());
    }

    @Test
    public void testWithIDType() throws Exception {
        final JsonRootGraphQLTypeBuilder typeBuilder = new JsonRootGraphQLTypeBuilder();
        final GraphQLOutputType type = (GraphQLOutputType) typeBuilder.buildType(ClassWithJsonValueOfTypeID.class);
        assertEquals(Scalars.GraphQLID, type);
    }

    public static class ClassWithJsonValue {
        private String value;

        public ClassWithJsonValue(final String value) {
            this.value = value;
        }

        @JsonValue
        public String getValue() {
            return value;
        }
    }

    public static class ClassWithJsonValueOfTypeID {
        private String value;

        public ClassWithJsonValueOfTypeID(final String value) {
            this.value = value;
        }

        @JsonValue
        @GraphQLIDType
        public String getValue() {
            return value;
        }
    }

    public static class ProviderWithJsonValueInput {
        @GraphQLName
        public String query(@GraphQLName("input") final ClassWithJsonValue input) {
            return input.getValue();
        }
    }
}
