package com.atlassian.graphql.json.types;

import com.atlassian.graphql.GraphQLTestTypeBuilderImpl;
import com.atlassian.graphql.test.json.GraphQLJsonSerializationTestHelper;
import org.junit.Test;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import static org.junit.Assert.assertEquals;

public class JsonOrXmlObjectTypeBuilderTest {
    private final GraphQLJsonSerializationTestHelper testHelper =
            new GraphQLJsonSerializationTestHelper(new GraphQLTestTypeBuilderImpl(new JsonOrXmlRootGraphQLTypeBuilder()));

    @Test
    public void testXmlAnnotatedObject() throws Exception {
        final ObjectWithXmlAnnotations obj = new ObjectWithXmlAnnotations();
        assertEquals(
                ("{\n" +
                "  'elementFieldValue' : 'elementField',\n" +
                "  'attributeFieldValue' : 'attributeField',\n" +
                "  'attributeMethodValue' : 'attributeMethod',\n" +
                "  'elementMethodValue' : 'elementMethod'\n" +
                "}").replace("'", "\""),
                testHelper.serializeGraphToJson(ObjectWithXmlAnnotations.class, obj, false, null));
    }

    public static class ObjectWithXmlAnnotations {
        @XmlElement
        private String elementFieldValue = "elementField";

        @XmlAttribute
        private String attributeFieldValue = "attributeField";

        @XmlElement
        public String getElementMethodValue() {
            return "elementMethod";
        }

        @XmlAttribute
        public String getAttributeMethodValue() {
            return "attributeMethod";
        }
    }
}
