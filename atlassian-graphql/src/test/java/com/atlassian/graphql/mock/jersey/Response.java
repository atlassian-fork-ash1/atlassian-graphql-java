package com.atlassian.graphql.mock.jersey;

/**
 * A mock jersey javax.ws.rs.core.Response object.
 */
public abstract class Response {
    public abstract Object getEntity();

    public int getStatus() {
        return 200;
    }
}
