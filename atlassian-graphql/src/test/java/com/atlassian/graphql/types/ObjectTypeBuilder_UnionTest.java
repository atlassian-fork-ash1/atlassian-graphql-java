package com.atlassian.graphql.types;

import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.annotations.GraphQLUnion;
import com.atlassian.graphql.schema.GraphQLTypeSchemaPrinter;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import graphql.schema.GraphQLUnionType;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ObjectTypeBuilder_UnionTest {
    @Test
    public void testBuildUnionType() {
        final GraphQLTypeSchemaPrinter printer = new GraphQLTypeSchemaPrinter(GraphQLTypeSchemaPrinter.Style.Flat);
        final GraphQLTypeBuilderContext context = new GraphQLTypeBuilderContext();
        final GraphQLUnionType unionType = (GraphQLUnionType) new RootGraphQLTypeBuilder().buildType(UnionType.class, context);

        assertEquals(
                "union UnionType = ConcreteClass1 | ConcreteClass2",
                printer.print(unionType, context.getTypes()));
    }

    @GraphQLUnion(possibleTypes = { ConcreteClass1.class, ConcreteClass2.class })
    public interface UnionType {
    }

    public static class ConcreteClass1 implements UnionType {
        @GraphQLName
        private String concreteField1;
    }

    public static class ConcreteClass2 implements UnionType {
        @GraphQLName
        private String concreteField2;
    }
}
