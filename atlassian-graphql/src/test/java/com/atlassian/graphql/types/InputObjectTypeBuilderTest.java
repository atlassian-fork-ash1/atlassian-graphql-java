package com.atlassian.graphql.types;

import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import graphql.schema.GraphQLInputObjectType;
import graphql.schema.GraphQLType;
import org.junit.Test;

import java.lang.reflect.Parameter;

import static org.junit.Assert.assertEquals;

public class InputObjectTypeBuilderTest {
    @Test
    public void testBuildInputObject() throws Exception {
        final Parameter parameter = ObjectWithMethod.class.getMethod("method", InputType.class).getParameters()[0];
        final GraphQLType type = new RootGraphQLTypeBuilder().buildType(InputType.class, parameter, new GraphQLTypeBuilderContext());
        assertEquals("Expected input type", true, type instanceof GraphQLInputObjectType);
        assertEquals("Expected 1 field", 1, ((GraphQLInputObjectType) type).getFieldDefinitions().size());
    }

    public static class ObjectWithMethod {
        public void method(final InputType input) {
        }
    }

    public static class InputType {
        @GraphQLName
        private String field;
    }
}
