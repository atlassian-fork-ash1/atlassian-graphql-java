package com.atlassian.graphql.types;

import com.atlassian.graphql.GraphQLTestTypeBuilderImpl;
import com.atlassian.graphql.annotations.GraphQLIDType;
import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.annotations.GraphQLNonNull;
import com.atlassian.graphql.annotations.GraphQLType;
import com.atlassian.graphql.schema.GraphQLTypeSchemaPrinter;
import com.atlassian.graphql.test.json.GraphQLJsonSerializer;
import com.atlassian.graphql.test.json.GraphQLJsonSerializerOptions;
import com.fasterxml.jackson.databind.ObjectMapper;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLSchema;
import org.junit.Test;

import java.util.EnumSet;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class GraphQLIDTypeBuilderTest {
    private final GraphQLJsonSerializer serializer = new GraphQLJsonSerializer(
            new ObjectMapper(),
            EnumSet.of(GraphQLJsonSerializerOptions.ADD_ROOT_FIELD));

    @Test
    public void testSerialize() throws Exception {
        final ObjectWithIDField obj = new ObjectWithIDField("id");
        assertEquals(
                "{\n" +
                "  \"idField\" : \"id\"\n" +
                "}",
                serializer.serializeUsingGraphQL(new GraphQLTestTypeBuilderImpl(), ObjectWithIDField.class, obj));
    }

    @Test
    public void testSchema() {
        final RootGraphQLTypeBuilder typeBuilder = new RootGraphQLTypeBuilder();
        final GraphQLObjectType type = (GraphQLObjectType) typeBuilder.buildType(ObjectWithIDField.class);
        final GraphQLSchema schema = GraphQLSchema.newSchema().query(type).build();
        assertEquals(
                "type ObjectWithIDField {\n" +
                "  idField: ID\n" +
                "}",
                new GraphQLTypeSchemaPrinter(GraphQLTypeSchemaPrinter.Style.Flat).print(schema.getQueryType()));
    }

    @Test
    public void testSchemaWithNonNullId() {
        final RootGraphQLTypeBuilder typeBuilder = new RootGraphQLTypeBuilder();
        final GraphQLObjectType type = (GraphQLObjectType) typeBuilder.buildType(ObjectWithNonNullIDField.class);
        final GraphQLSchema schema = GraphQLSchema.newSchema().query(type).build();
        assertEquals(
                "type ObjectWithNonNullIDField {\n" +
                        "  idField: ID!\n" +
                        "}",
                new GraphQLTypeSchemaPrinter(GraphQLTypeSchemaPrinter.Style.Flat).print(schema.getQueryType()));
    }

    @Test
    public void testSchemaWithNonNullTypedObject() {
        final RootGraphQLTypeBuilder typeBuilder = new RootGraphQLTypeBuilder();
        final GraphQLObjectType type = (GraphQLObjectType) typeBuilder.buildType(ObjectWithMapField.class);
        final GraphQLSchema schema = GraphQLSchema.newSchema().query(type).build();
        assertEquals(
                "type ObjectWithMapField {\n" +
                        "  map: MapOfKeyEnumToString!\n" +
                        "}\n" +
                        "\n" +
                        "type MapOfKeyEnumToString {\n" +
                        "  key1: String\n" +
                        "  KEY2: String\n" +
                        "}",
                new GraphQLTypeSchemaPrinter(GraphQLTypeSchemaPrinter.Style.Flat).print(schema.getQueryType()));
    }

    public static class ObjectWithIDField {
        @GraphQLName
        @GraphQLIDType
        private String idField;

        public ObjectWithIDField(final String idField) {
            this.idField = idField;
        }
    }

    public static class ObjectWithNonNullIDField {
        @GraphQLName
        @GraphQLIDType
        @GraphQLNonNull
        private String idField;

        public ObjectWithNonNullIDField(final String idField) {
            this.idField = idField;
        }
    }

    public static class ObjectWithMapField {
        @GraphQLName
        @GraphQLNonNull
        @GraphQLType(EnumMapTypeBuilder.class)
        private Map<EnumMapTypeBuilderTest.KeyEnum, String> map;

        public ObjectWithMapField(final Map<EnumMapTypeBuilderTest.KeyEnum, String> map) {
            this.map = map;
        }
    }

    public static enum KeyEnum {
        @GraphQLName("key1")
        KEY1,
        KEY2,
    }
}
