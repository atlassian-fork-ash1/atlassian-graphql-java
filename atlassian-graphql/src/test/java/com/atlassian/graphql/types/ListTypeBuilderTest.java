package com.atlassian.graphql.types;

import com.atlassian.graphql.GraphQLTestTypeBuilderImpl;
import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.spi.GraphQLExtensions;
import com.atlassian.graphql.test.json.GraphQLJsonSerializer;
import com.atlassian.graphql.test.json.GraphQLJsonSerializerOptions;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.reflect.TypeToken;
import org.junit.Test;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import static org.junit.Assert.assertEquals;

public class ListTypeBuilderTest {
    private final GraphQLJsonSerializer serializer = new GraphQLJsonSerializer(
            new ObjectMapper(),
            EnumSet.of(GraphQLJsonSerializerOptions.ADD_ROOT_FIELD));

    @Test
    public void testEmptyList() throws Exception {
        final List<TestClass> list = Lists.newArrayList();
        assertEquals(
                "[ ]",
                serializer.serializeUsingGraphQL(new GraphQLTestTypeBuilderImpl(), new TypeToken<List<TestClass>>(){}.getType(), list));
    }

    @Test
    public void testPrimitiveList() throws Exception {
        final List<String> list = Lists.newArrayList("item1", "item2");
        assertEquals(
                "[ \"item1\", \"item2\" ]",
                serializer.serializeUsingGraphQL(new GraphQLTestTypeBuilderImpl(), new TypeToken<List<String>>(){}.getType(), list));
    }

    @Test
    public void testObjectList() throws Exception {
        final List<TestClass> list = Lists.newArrayList(new TestClass("field1", "field2"), new TestClass("field1", "field2"));
        assertEquals(
                "[ {\n" +
                        "  \"field1\" : \"field1\",\n" +
                        "  \"field2\" : \"field2\"\n" +
                        "}, {\n" +
                        "  \"field1\" : \"field1\",\n" +
                        "  \"field2\" : \"field2\"\n" +
                        "} ]",
                serializer.serializeUsingGraphQL(new GraphQLTestTypeBuilderImpl(), new TypeToken<List<TestClass>>(){}.getType(), list));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testConvertIterableToList() throws Exception {
        final ListTypeBuilder typeBuilder = new ListTypeBuilder(new RootGraphQLTypeBuilder());
        final Function<Object, Object> valueTransformer = typeBuilder.getValueTransformer(new TypeToken<Set<Iterable>>(){}.getType(), null);

        final List<String> list = (List<String>) valueTransformer.apply(Iterables.transform(Arrays.asList("item1", "item2"), item -> item));
        assertEquals("GraphQL only supports List, so Iterable must be converted", Lists.newArrayList("item1", "item2"), list);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testConvertSetToList() throws Exception {
        final ListTypeBuilder typeBuilder = new ListTypeBuilder(new RootGraphQLTypeBuilder());
        final Function<Object, Object> valueTransformer = typeBuilder.getValueTransformer(new TypeToken<Set<String>>(){}.getType(), null);

        final List<String> list = (List<String>) valueTransformer.apply(Sets.newLinkedHashSet(Arrays.asList("item1", "item2")));
        assertEquals("GraphQL only supports List, so Set must be converted", Lists.newArrayList("item1", "item2"), list);
    }

    @Test
    public void testWithValueTransformer() throws Exception {
        final List<TestClass> list = Lists.newArrayList(new TestClass("field1", "field2"), new TestClass("field1", "field2"));
        final GraphQLExtensions valueTransformer = new GraphQLExtensions() {
            @Override
            public Function<Object, Object> getValueTransformer(final Type type, final AnnotatedElement element) {
                if (type == TestClass.class) {
                    return value -> ImmutableMap.of("field1", "transformed1", "field2", "transformed2");
                }
                return null;
            }
        };
        assertEquals(
                "[ {\n" +
                "  \"field1\" : \"transformed1\",\n" +
                "  \"field2\" : \"transformed2\"\n" +
                "}, {\n" +
                "  \"field1\" : \"transformed1\",\n" +
                "  \"field2\" : \"transformed2\"\n" +
                "} ]",
                serializer.serializeUsingGraphQL(new GraphQLTestTypeBuilderImpl(valueTransformer), new TypeToken<List<TestClass>>(){}.getType(), list));
    }

    public static class TestClass {
        @GraphQLName("field1")
        public final String field1;

        @GraphQLName("field2")
        public final String field2;

        public TestClass(final String field1, final String field2) {
            this.field1 = field1;
            this.field2 = field2;
        }
    }
}
