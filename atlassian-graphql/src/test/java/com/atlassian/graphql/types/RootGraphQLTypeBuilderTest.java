package com.atlassian.graphql.types;

import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import graphql.Scalars;
import graphql.schema.GraphQLList;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLType;
import org.junit.Test;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Type;
import java.util.List;
import java.util.function.Function;

import static org.junit.Assert.assertEquals;

public class RootGraphQLTypeBuilderTest {
    @Test
    public void testGraphQLTypeAnnotation() {
        final RootGraphQLTypeBuilder typeBuilder = new RootGraphQLTypeBuilder();
        final GraphQLObjectType type = (GraphQLObjectType) typeBuilder.buildType(TestObjectWithField.class);

        final GraphQLList list = (GraphQLList) type.getFieldDefinitions().get(0).getType();
        assertEquals("Field type is List<String>", "String", list.getWrappedType().getName());
    }

    @Test
    public void testGraphQLTypeAnnotation_CustomTypeBuilder() {
        final RootGraphQLTypeBuilder typeBuilder = new RootGraphQLTypeBuilder();
        final GraphQLObjectType type = (GraphQLObjectType) typeBuilder.buildType(TestObjectWithCustomTypeBuilder.class);

        final GraphQLType graphqlType = type.getFieldDefinitions().get(0).getType();
        assertEquals("Custom type builder defines String field type", "String", graphqlType.getName());
    }

    public static class TestObjectWithField {
        @GraphQLName
        @com.atlassian.graphql.annotations.GraphQLType({List.class, String.class})
        private List field;
    }

    public static class TestObjectWithCustomTypeBuilder {
        @GraphQLName
        @com.atlassian.graphql.annotations.GraphQLType(CustomTypeBuilder.class)
        private Object field;
    }

    public static class CustomTypeBuilder implements GraphQLTypeBuilder {
        @Override
        public boolean canBuildType(final Type type, final AnnotatedElement element) {
            return true;
        }

        @Override
        public graphql.schema.GraphQLType buildType(
                final String typeName,
                final Type type,
                final AnnotatedElement element,
                final GraphQLTypeBuilderContext context) {

            return Scalars.GraphQLString;
        }

        @Override
        public Function<Object, Object> getValueTransformer(final Type type, final AnnotatedElement element) {
            return obj -> obj.toString();
        }
    }
}
