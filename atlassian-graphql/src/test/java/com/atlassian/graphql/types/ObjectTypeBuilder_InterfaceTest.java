package com.atlassian.graphql.types;

import com.atlassian.graphql.annotations.GraphQLInterface;
import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.annotations.GraphQLPossibleType;
import com.atlassian.graphql.schema.GraphQLTypeSchemaPrinter;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import graphql.schema.GraphQLInterfaceType;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ObjectTypeBuilder_InterfaceTest {
    @Test
    public void testBuildInterfaceType() {
        final GraphQLTypeSchemaPrinter printer = new GraphQLTypeSchemaPrinter(GraphQLTypeSchemaPrinter.Style.Flat);
        final GraphQLTypeBuilderContext context = new GraphQLTypeBuilderContext();
        final GraphQLInterfaceType interfaceType = (GraphQLInterfaceType) new RootGraphQLTypeBuilder().buildType(InterfaceType.class, context);

        assertEquals(String.join("\n", "",
                "interface InterfaceType {",
                "  interfaceField: String",
                "}",
                "",
                "type ConcreteClass1 implements InterfaceType {",
                "  interfaceField: String",
                "  concreteField1: String",
                "}",
                "",
                "type ConcreteClass2 implements InterfaceType {",
                "  interfaceField: String",
                "  concreteField2: String",
                "}").trim(), printer.print(interfaceType, context.getTypes()));
    }

    @Test
    public void testBuildInterfaceTypeWithParameterizedTypes() {
        final GraphQLTypeSchemaPrinter printer = new GraphQLTypeSchemaPrinter(GraphQLTypeSchemaPrinter.Style.Flat);
        final GraphQLTypeBuilderContext context = new GraphQLTypeBuilderContext();
        final GraphQLInterfaceType interfaceType = (GraphQLInterfaceType) new RootGraphQLTypeBuilder().buildType(InterfaceTypeWithTypes.class, context);

        assertEquals(String.join("\n", "",
                "interface InterfaceTypeWithTypes {\n" +
                        "  field: String\n" +
                        "}\n" +
                        "\n" +
                        "type ConcreteClass3 implements InterfaceTypeWithTypes {\n" +
                        "  field: String\n" +
                        "  concreteField3: String\n" +
                        "}\n" +
                        "\n" +
                        "type ParameterizedClass_String implements InterfaceTypeWithTypes {\n" +
                        "  field: String\n" +
                        "  value: String\n" +
                        "}").trim(), printer.print(interfaceType, context.getTypes()));
    }

    @GraphQLInterface(possibleTypes = {ConcreteClass1.class, ConcreteClass2.class})
    public static class InterfaceType {
        @GraphQLName
        private String interfaceField;
    }

    public static class ConcreteClass1 extends InterfaceType {

        @GraphQLName
        private String concreteField1;
    }

    public static class ConcreteClass2 extends InterfaceType {

        @GraphQLName
        private String concreteField2;
    }

    @GraphQLInterface
    @GraphQLPossibleType({ParameterizedClass.class, String.class})
    @GraphQLPossibleType({ConcreteClass3.class})
    static class InterfaceTypeWithTypes {
        @GraphQLName
        private String field;
    }

    static class ParameterizedClass<T> extends InterfaceTypeWithTypes {
        @GraphQLName
        private T value;
    }

    static class ConcreteClass3 extends InterfaceTypeWithTypes {
        @GraphQLName
        private String concreteField3;
    }
}
