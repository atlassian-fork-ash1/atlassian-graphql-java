package com.atlassian.graphql.types;

import com.atlassian.graphql.GraphQLTestTypeBuilderImpl;
import com.atlassian.graphql.test.json.GraphQLJsonSerializationTestHelper;
import com.google.common.reflect.TypeToken;
import org.junit.Test;

import java.lang.reflect.Type;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

public class OptionalTypeBuilderTest {
    private final GraphQLJsonSerializationTestHelper testHelper =
            new GraphQLJsonSerializationTestHelper(new GraphQLTestTypeBuilderImpl());

    @Test
    public void testSerializeValue() throws Exception {
        final Type type = new TypeToken<Optional<String>>(){}.getType();
        final Optional<String> obj = Optional.of("fieldValue");
        assertEquals("\"fieldValue\"", testHelper.serializeGraphToJson(type, obj, false, null));
    }

    @Test
    public void testSerializeNull() throws Exception {
        final Type type = new TypeToken<Optional<String>>(){}.getType();
        final Optional<String> obj = Optional.empty();
        assertEquals("null", testHelper.serializeGraphToJson(type, obj, false, null));
    }
}

