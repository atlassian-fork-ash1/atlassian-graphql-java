# Router for GraphQL Endpoints

### Summary

Built on top of [graphql-java](https://github.com/graphql-java/graphql-java), the GraphQLRouter class provides
the ability to route between remote endpoints or local schemas using a rules based approach.

### Getting Started

Add atlassian-graphql dependency to your pom.xml:

```maven
<dependency>
    <groupId>com.atlassian.graphql</groupId>
    <artifactId>atlassian-graphql-annotations</artifactId>
    <version>0.1.0</version>
</dependency>
<dependency>
    <groupId>com.atlassian.graphql</groupId>
    <artifactId>atlassian-graphql</artifactId>
    <version>0.1.0</version>
</dependency>
```

### Defining Routes

Routes are allocated to root field names of the incoming query.<br/>
<br/>
Defining routes and endpoints is easy, here's an example:

```java
final GraphQLRouteConfiguration config =
  GraphQLRouteConfiguration.builder()
  .addRoute(new LocalGraphQLRoute(GraphQLRouteRules.builder()
                                  .addFieldNames("content", "children")
                                  .build(),
            contentSchema))
  .addRoute(new LocalGraphQLRoute(GraphQLRouteRules.builder()
                                 .addFieldName("space")
                                 .build(),
            spaceSchema))
  .addRoute(new HttpGraphQLRoute(GraphQLRouteRules.builder()
                                 .allFields()
                                 .typeNamePrefix("Confluence")
                                 .addBlacklistFieldName("frontendbff")
                                 .addBlacklistFieldName("private")
                                 .build(),
            new URL("http://localhost:8080/wiki/rest/graph")))
  .build();
final GraphQLRouter router = new GraphQLRouter(config);
```

Another example:

```java
final graphql.GraphQL graphql = graphql.GraphQL.newGraphQL(localSchema).build();
final GraphQLRouteRules confluenceRouteRules = 
            GraphQLRouteRules.builder()
            .rootFieldName("confluence")
            .typeNamePrefix("Confluence")
            .allFields()
            .build();
final GraphQLRouteConfiguration config =
  GraphQLRouteConfiguration.builder()
  .addRoute(new HttpGraphQLRoute(confluenceRouteRules, new URL("http://localhost:8080/wiki/rest/graph")))
  .addRoute(new LocalGraphQLRoute(GraphQLRouteRules.builder().allFields().build(), graphql))
  .build();
final GraphQLRouter router = new GraphQLRouter(config);
```

LocalGraphQLRoute and HttpGraphQLRoute both implement the GraphQLRoute interface.<br/>
<br/>
If a different networking library is desired for your project, implement GraphQLRoute manually.

### Creating a GraphQL Router REST endpoint

There are two approaches to create an endpoint.
- Call GraphQLRouter.execute() manually, possibly from a framework such as GraphQL Spring boot starter
- Use the built in atlassian-graphql GraphQLRestEndpoint, for example:

```java
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class GraphResource {
    private final GraphQLRestEndpoint endpoint;

    public GraphResource(final GraphQLRouter router) {
        // see above for how to configure the GraphQLRouter
        this.endpoint = new GraphQLRestEndpoint(router, null);
    }

    @POST
    public Response query(
            final String requestString,
            @QueryParam("query") final String query) throws Exception {

        return Response.ok(endpoint.execute(query != null ? query : requestString, new GraphQLContext())).build();
    }
}
```
