package com.atlassian.graphql.router;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * GraphQL routing rules for {@link GraphQLRouter}.
 * Rules include a whitelist and a blacklist for root field names.
 * By default, all fields are included for the route.
 */
@SuppressWarnings("WeakerAccess")
public class GraphQLRouteRules {
    private final String rootFieldName;
    private final GraphQLRouteTypeNameStrategy typeNameStrategy;
    private final boolean isSchemaRoutingEnabled;
    private final boolean shouldTrimSchema;
    private final Set<String> fieldNames;
    private final Set<String> blacklistFieldNames;

    private GraphQLRouteRules(
            final String rootFieldName,
            final GraphQLRouteTypeNameStrategy typeNameStrategy,
            final boolean isSchemaRoutingEnabled,
            final boolean shouldTrimSchema,
            final Set<String> fieldNames,
            final Set<String> blacklistFieldNames) {

        this.rootFieldName = rootFieldName;
        this.typeNameStrategy = typeNameStrategy;
        this.isSchemaRoutingEnabled = isSchemaRoutingEnabled;
        this.shouldTrimSchema = shouldTrimSchema;
        this.fieldNames = fieldNames;
        this.blacklistFieldNames = blacklistFieldNames;
    }

    public static Builder builder() {
        return new Builder();
    }

    /**
     * True if the route handles a specific field name at the root of the graph.
     */
    public boolean matchesFieldName(final String fieldName) {
        return !blacklistFieldNames.contains(fieldName) &&
                (fieldNames.isEmpty() || fieldNames.contains(fieldName));
    }

    /**
     * Get the root field name under which the route fields will be nested under,
     * or null to add the fields to the root.
     */
    public String getRootFieldName() {
        return rootFieldName;
    }

    /**
     * Get the type name strategy.
     */
    public GraphQLRouteTypeNameStrategy getTypeNameStrategy() {
        return typeNameStrategy;
    }

    /**
     * True to merge schema definitions returned in query results (__schema and __typename fields).
     */
    public boolean isSchemaRoutingEnabled() {
        return isSchemaRoutingEnabled;
    }

    /**
     * True to remove unused types and root fields from schema results.
     */
    public boolean shouldTrimSchema() {
        return shouldTrimSchema;
    }

    @SuppressWarnings("unused")
    public static class Builder {
        private String rootFieldName;
        private GraphQLRouteTypeNameStrategy typeNameStrategy;
        private boolean isSchemaRoutingEnabled = true;
        private boolean shouldTrimSchema = true;
        private Set<String> fieldNames = new HashSet<>();
        private Set<String> blacklistFieldNames = new HashSet<>();

        public Builder rootFieldName(final String rootFieldName) {
            this.rootFieldName = rootFieldName;
            return this;
        }

        public Builder typeNamePrefix(final String typeNamePrefix) {
            return typeNameStrategy(new GraphQLRouteTypeNamePrefixStrategy(typeNamePrefix));
        }

        public Builder typeNameStrategy(final GraphQLRouteTypeNameStrategy typeNameStrategy) {
            this.typeNameStrategy = typeNameStrategy;
            return this;
        }

        public Builder disableSchemaRouting() {
            isSchemaRoutingEnabled = false;
            return this;
        }

        public Builder trimSchema(final boolean enabled) {
            shouldTrimSchema = enabled;
            return this;
        }

        public Builder allFields() {
            this.fieldNames.clear();
            return this;
        }

        public Builder addFieldName(final String fieldName) {
            this.fieldNames.add(fieldName);
            return this;
        }

        public Builder addFieldNames(final String... fieldNames) {
            return addFieldNames(Arrays.asList(fieldNames));
        }

        public Builder addFieldNames(final List<String> fieldNames) {
            this.fieldNames.addAll(fieldNames);
            return this;
        }

        public Builder addBlacklistFieldName(final String fieldName) {
            this.blacklistFieldNames.add(fieldName);
            return this;
        }

        public Builder addBlacklistFieldNames(final String... fieldNames) {
            return addBlacklistFieldNames(Arrays.asList(fieldNames));
        }

        public Builder addBlacklistFieldNames(final List<String> fieldNames) {
            this.blacklistFieldNames.addAll(fieldNames);
            return this;
        }

        public GraphQLRouteRules build() {
            return new GraphQLRouteRules(rootFieldName, typeNameStrategy, isSchemaRoutingEnabled, shouldTrimSchema, fieldNames, blacklistFieldNames);
        }
    }
}
