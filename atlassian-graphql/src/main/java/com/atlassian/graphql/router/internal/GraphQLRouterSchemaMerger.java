package com.atlassian.graphql.router.internal;

import com.atlassian.graphql.router.GraphQLRoute;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Merges two GraphQL schema data results together to form one unified schema.
 */
public class GraphQLRouterSchemaMerger {
    private static final String SCHEMA_FIELD = "__schema";
    private final String queryTypeName;

    public GraphQLRouterSchemaMerger(final String queryTypeName) {
        this.queryTypeName = queryTypeName;
    }

    @SuppressWarnings("unchecked")
    public Map<String, Object> mergeSchemas(
            final List<Map<String, Object>> data,
            final List<GraphQLRoute> routes) {

        final List<Map<String, Object>> schemas =
                data.stream()
                .map(item -> item != null ? (Map<String, Object>) item.get(SCHEMA_FIELD) : null)
                .collect(Collectors.toList());
        final List<Map<String, Object>> nonNullSchemas =
                schemas.stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        if (nonNullSchemas.isEmpty()) {
            return null;
        }

        // if there's only 1 schema, return it
        if (nonNullSchemas.size() == 1) {
            return nonNullSchemas.get(0);
        }

        // start with the first schema, other schemas will be added to it
        final Map<String, Object> schema = new LinkedHashMap<>(nonNullSchemas.get(0));

        // get the root query type
        final Map<String, Object> newQueryType = createCombinedQueryType(schemas, routes);

        // add types and composed queryType
        schema.put("types", getAllTypes(schemas, newQueryType));
        schema.put("queryType", ImmutableMap.of("name", newQueryType.get("name")));
        return schema;
    }

    private static List<Map<String, Object>> getAllTypes(
            final List<Map<String, Object>> schemas,
            final Map<String, Object> queryType) {

        final List<Map<String, Object>> types = new ArrayList<>();
        types.add(queryType);
        types.addAll(getAllTypes(schemas));
        return types;
    }

    @SuppressWarnings("unchecked")
    private static Collection<Map<String, Object>> getAllTypes(final List<Map<String, Object>> schemas) {
        final LinkedHashMap<String, Map<String, Object>> list = new LinkedHashMap<>();
        for (final Map<String, Object> schema : schemas) {
            if (schema != null) {
                final List<Map<String, Object>> types = (List<Map<String, Object>>) schema.get("types");
                for (final Map<String, Object> type : types) {
                    final String typeName = (String) type.get("name");
                    list.put(typeName, type);
                }
            }
        }
        return list.values();
    }

    @SuppressWarnings("unchecked")
    private Map<String, Object> createCombinedQueryType(
            final List<Map<String, Object>> schemas,
            final List<GraphQLRoute> routes) {

        final LinkedHashMap<String, Map<String, Object>> combinedFields = new LinkedHashMap<>();
        for (int i = 0; i < schemas.size(); i++) {
            final Map<String, Object> schema = schemas.get(i);
            final GraphQLRoute route = routes.get(i);

            if (schema != null && route.getRouteRules().isSchemaRoutingEnabled()) {
                final Map<String, Object> queryType = getQueryType(schema);
                if (queryType != null) {
                    addQueryTypeFields(schema, combinedFields, route, queryType);
                }
            }
        }

        final Map<String, Object> type = new LinkedHashMap<>();
        type.put("name", queryTypeName);
        type.put("kind", "OBJECT");
        type.put("fields", combinedFields.values());
        type.put("description", null);
        type.put("enumValues", null);
        type.put("inputFields", null);
        type.put("interfaces", Collections.emptyList());
        type.put("possibleTypes", null);
        return type;
    }

    private void addQueryTypeFields(
            final Map<String, Object> schema,
            final LinkedHashMap<String, Map<String, Object>> combinedFields,
            final GraphQLRoute route,
            final Map<String, Object> queryType) {

        if (!Strings.isNullOrEmpty(route.getRouteRules().getRootFieldName())) {
            addRootQueryField(combinedFields, route, queryType);
        } else {
            addQueryFieldsInline(schema, combinedFields, route, queryType);
        }
    }

    private void addRootQueryField(
            final LinkedHashMap<String, Map<String, Object>> combinedFields,
            final GraphQLRoute route,
            final Map<String, Object> queryType) {

        final String routeRootFieldName = route.getRouteRules().getRootFieldName();

        final Map<String, Object> fieldType = new LinkedHashMap<>();
        fieldType.put("kind", "OBJECT");
        fieldType.put("description", null);
        fieldType.put("name", queryType.get("name"));
        fieldType.put("isDeprecated", false);
        fieldType.put("deprecationReason", null);

        final Map<String, Object> field = new LinkedHashMap<>();
        field.put("name", routeRootFieldName);
        field.put("type", fieldType);
        field.put("args", Lists.newArrayList());

        combinedFields.put(routeRootFieldName, field);
    }

    @SuppressWarnings("unchecked")
    private void addQueryFieldsInline(
            final Map<String, Object> schema,
            final LinkedHashMap<String, Map<String, Object>> combinedFields,
            final GraphQLRoute route,
            final Map<String, Object> queryType) {

        validateRootQueryType(queryType);
        removeType(schema, (String) queryType.get("name"));

        final List<Map<String, Object>> fields = (List<Map<String, Object>>) queryType.get("fields");
        for (final Map<String, Object> field : fields) {
            final String fieldName = (String) field.get("name");
            if (route.getRouteRules().matchesFieldName(fieldName) && !combinedFields.containsKey(fieldName)) {
                combinedFields.put(fieldName, field);
            }
        }
    }

    @SuppressWarnings("unchecked")
    private static void removeType(final Map<String, Object> schema, final String typeName) {
        final List<Map<String, Object>> types = (List<Map<String, Object>>) schema.get("types");
        for (final Map<String, Object> type : types) {
            if (type.get("name").equals(typeName)) {
                types.remove(type);
                break;
            }
        }
    }

    private static void validateRootQueryType(final Map<String, Object> queryType) {
        if (queryType.get("enumValues") != null) {
            throw new RuntimeException("'enumValues' not supported for root query type");
        }
        if (queryType.get("inputFields") != null) {
            throw new RuntimeException("'inputFields' not supported for root query type");
        }
        if (queryType.get("interfaces") != null && !((List) queryType.get("interfaces")).isEmpty()) {
            throw new RuntimeException("'interfaces' not supported for root query type");
        }
        if (queryType.get("possibleTypes") != null && !((List) queryType.get("possibleTypes")).isEmpty()) {
            throw new RuntimeException("'possibleTypes' not supported for root query type");
        }
        if (!"OBJECT".equals(queryType.get("kind"))) {
            throw new RuntimeException("'kind' must be OBJECT for root query type");
        }
    }

    @SuppressWarnings("unchecked")
    private static Map<String, Object> getQueryType(final Map<String, Object> schema) {
        final Map<String, Object> queryType = (Map<String, Object>) schema.get("queryType");
        if (queryType != null) {
            final String typeName = (String) queryType.get("name");
            final List<Map<String, Object>> types = (List<Map<String, Object>>) schema.get("types");
            return types.stream()
                   .filter(type -> type.get("name").equals(typeName))
                   .findFirst()
                   .orElse(null);
        }
        return null;
    }
}
