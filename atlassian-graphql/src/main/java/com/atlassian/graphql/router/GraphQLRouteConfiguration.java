package com.atlassian.graphql.router;

import java.util.stream.Stream;

/**
 * Configuration for GraphQL routes used by the {@link GraphQLRouter}
 *
 * @param <C> the context type
 */
public interface GraphQLRouteConfiguration<C> {

    String DEFAULT_NAME = "ApplicationQueryType";

    Stream<GraphQLRoute<C>> getRoutes(C context);

    default String getQueryTypeName(C context) {
        return DEFAULT_NAME;
    }

    /**
     * Creates a builder for "static" route configurations
     *
     * @param <C> the type of the GraphQL context
     * @return a route configuration builder
     */
    static <C> GraphQLRouteConfigurationBuilder<C> builder() {
        return new GraphQLRouteConfigurationBuilder<>();
    }
}
