package com.atlassian.graphql.router;

import java.util.Collections;
import java.util.Set;

/**
 * A strategy for adding a prefix to the type names used for a GraphQLRouter route.
 */
public class GraphQLRouteTypeNamePrefixStrategy implements GraphQLRouteTypeNameStrategy {
    private final String typeNamePrefix;
    private final Set<String> excludeTypeNames;

    public GraphQLRouteTypeNamePrefixStrategy(final String typeNamePrefix) {
        this(typeNamePrefix, Collections.emptySet());
    }

    public GraphQLRouteTypeNamePrefixStrategy(final String typeNamePrefix, final Set<String> excludeTypeNames) {
        this.typeNamePrefix = typeNamePrefix;
        this.excludeTypeNames = excludeTypeNames;
    }

    @Override
    public String translateTypeNameClientToRoute(final String typeName) {
        if (typeName.startsWith(typeNamePrefix)
                && typeName.length() > typeNamePrefix.length()
                && !excludeTypeNames.contains(typeName)) {

            return typeName.substring(typeNamePrefix.length());
        }
        return typeName;
    }

    @Override
    public String translateTypeNameRouteToClient(final String typeName) {
        return !excludeTypeNames.contains(typeName)
               ? typeNamePrefix + typeName
               : typeName;
    }
}
