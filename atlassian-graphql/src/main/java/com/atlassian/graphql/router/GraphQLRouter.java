package com.atlassian.graphql.router;

import com.atlassian.graphql.GraphQLExecutor;
import com.atlassian.graphql.rest.GraphQLRestRequest;
import com.atlassian.graphql.router.internal.GraphQLRouterSchemaMerger;
import com.atlassian.graphql.router.internal.GraphQLRouterSchemaSimplifier;
import com.atlassian.graphql.router.internal.GraphQLRouterTypeRenamer;
import com.google.common.base.Strings;
import com.google.common.base.Throwables;
import com.google.common.collect.Lists;
import graphql.ExecutionResult;
import graphql.ExecutionResultImpl;
import graphql.GraphQLError;
import graphql.language.Definition;
import graphql.language.Document;
import graphql.language.Field;
import graphql.language.OperationDefinition;
import graphql.language.Selection;
import graphql.language.SelectionSet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

/**
 * A router for GraphQL query requests.
 * Incoming requests are partitioned by root field name and are routed to individual endpoints as required.
 */
public class GraphQLRouter<C> implements GraphQLExecutor {
    private static final String SCHEMA_FIELD = "__schema";

    private final GraphQLRouteConfiguration<C> config;
    private final Function<C, GraphQLRouterSchemaMerger> schemaMerger;

    private final GraphQLRouterTypeRenamer schemaTypeRenamer = new GraphQLRouterTypeRenamer();
    private final GraphQLRouterSchemaSimplifier schemaSimplifier = new GraphQLRouterSchemaSimplifier();

    public GraphQLRouter(GraphQLRouteConfiguration<C> config) {
        this(config, context -> new GraphQLRouterSchemaMerger(config.getQueryTypeName(context)));
    }

    public GraphQLRouter(GraphQLRouteConfiguration<C> config, Function<C, GraphQLRouterSchemaMerger> schemaMerger) {
        this.config = requireNonNull(config);
        this.schemaMerger = requireNonNull(schemaMerger);
    }

    @Override
    public ExecutionResult execute(final GraphQLRestRequest request, final Object context) throws IOException {
        return join(asyncExecute(request, (C) context));
    }

    /**
     * Partition a GraphQL query request by it's root field names, execute GraphQL requests over configured routes.
     */
    @SuppressWarnings({"unchecked", "WeakerAccess"})
    public CompletableFuture<ExecutionResult> asyncExecute(final GraphQLRestRequest request, final C context) throws IOException {
        final Map<String, GraphQLRoute<C>> routedFieldNames = new HashMap<>();
        final RouteResponsePair[] futuresAndRoutes =
                config.getRoutes(context)
                        .map(route -> new RouteResponsePair(executeIfNotNull(request, route, routedFieldNames, context), route))
                        .filter(pair -> pair.future != null)
                        .toArray(RouteResponsePair[]::new);

        final CompletableFuture<Object>[] futures = Arrays.stream(futuresAndRoutes).map(pair -> pair.future).toArray(CompletableFuture[]::new);
        final GraphQLRoute<C>[] routes = Arrays.stream(futuresAndRoutes).map(pair -> pair.route).toArray(GraphQLRoute[]::new);
        final CompletableFuture<Void> all = CompletableFuture.allOf(futures);
        return all.thenApply(__ -> mergeResults(getAll(futures), routes, context));
    }

    private static class RouteResponsePair<C> {
        final CompletableFuture<Object> future;
        final GraphQLRoute<C> route;

        RouteResponsePair(final CompletableFuture<Object> future, final GraphQLRoute<C> route) {
            this.future = future;
            this.route = route;
        }
    }

    private CompletableFuture<Object> executeIfNotNull(
            final GraphQLRestRequest request,
            final GraphQLRoute<C> route,
            final Map<String, GraphQLRoute<C>> routedFieldNames, C context) {

        final GraphQLRestRequest partitionedRequest = partitionRequest(request, route, routedFieldNames);
        if (partitionedRequest == null) {
            return null;
        }
        try {
            return route.route(partitionedRequest, context);
        } catch (final IOException ex) {
            throw Throwables.propagate(ex);
        }
    }

    private static Object[] getAll(final CompletableFuture<Object>[] futures) {
        return Arrays.stream(futures).map(GraphQLRouter::join).toArray(Object[]::new);
    }

    private static <T> T join(final CompletableFuture<T> future) {
        try {
            return future.join();
        } catch (CompletionException e) {
            if (e.getCause() instanceof RuntimeException) {
                throw (RuntimeException) e.getCause();
            } else {
                throw e;
            }
        }
    }

    private GraphQLRestRequest partitionRequest(
            final GraphQLRestRequest request,
            final GraphQLRoute<C> route,
            final Map<String, GraphQLRoute<C>> routedFieldNames) {

        final Document query = partitionQuery(request.getQueryDocument(), request.getOperationName(), route, routedFieldNames);
        return query != null ? new GraphQLRestRequest(query, request.getVariables(), request.getOperationName()) : null;
    }

    private Document partitionQuery(
            final Document query,
            final String operationName,
            final GraphQLRoute<C> route,
            final Map<String, GraphQLRoute<C>> routedFieldNames) {

        final List<Definition> definitions = new ArrayList<>();
        for (final Definition definition : query.getDefinitions()) {
            if (definition instanceof OperationDefinition &&
                    operationNameMatches((OperationDefinition) definition, operationName)) {

                Definition newDefinition = partitionOperationDefinition((OperationDefinition) definition, route, routedFieldNames);
                if (newDefinition == null) {
                    return null;
                }
                definitions.add(newDefinition);
            } else {
                definitions.add(definition);
            }
        }
        final Document paritionedQuery = new Document(definitions);
        schemaTypeRenamer.renameQueryTypes(route.getRouteRules().getTypeNameStrategy(), paritionedQuery);
        return paritionedQuery;
    }

    private static boolean operationNameMatches(final OperationDefinition definition, final String operationName) {
        return operationName == null ||
                (definition.getName() != null && Objects.equals(definition.getName(), operationName));
    }

    private static <C> Definition partitionOperationDefinition(
            final OperationDefinition definition,
            final GraphQLRoute<C> route,
            final Map<String, GraphQLRoute<C>> routedFieldNames) {

        if (definition.getSelectionSet() == null || definition.getSelectionSet().getSelections() == null) {
            return null;
        }
        final SelectionSet newSelectionSet = partitionSelectionSet(definition.getSelectionSet(), route, routedFieldNames);
        if (newSelectionSet == null) {
            return null;
        }
        return OperationDefinition.newOperationDefinition()
                .name(definition.getName())
                .operation(definition.getOperation())
                .variableDefinitions(definition.getVariableDefinitions())
                .selectionSet(newSelectionSet)
                .build();
    }

    private static <C> SelectionSet partitionSelectionSet(final SelectionSet selectionSet, final GraphQLRoute<C> route, final Map<String, GraphQLRoute<C>> routedFieldNames) {
        final GraphQLRouteRules rules = route.getRouteRules();
        final List<Selection> newSelections = selectionSet.getSelections().stream().filter(selection -> {
            final Field field = getSelectionAsField(selection);
            final String rootFieldName = field.getName();
            final boolean matchesRoute =
                    !hasBeenRoutedByOtherRoute(routedFieldNames, rootFieldName, route)
                            &&
                            (rootFieldName.equals(rules.getRootFieldName())
                                    || (rules.getRootFieldName() == null && route.getRouteRules().matchesFieldName(rootFieldName)));
            if (matchesRoute) {
                routedFieldNames.put(rootFieldName, route);
            }

            // __schema query is always sent to allFields routes
            return matchesRoute || (route.getRouteRules().isSchemaRoutingEnabled() && rootFieldName.equals(SCHEMA_FIELD));
        }).collect(Collectors.toList());

        // if the route is under it's own field, strip off the route field from the query
        if (!Strings.isNullOrEmpty(rules.getRootFieldName())) {
            spreadChildFieldsInline(rules.getRootFieldName(), newSelections);
        }
        return newSelections.isEmpty() ? null : new SelectionSet(newSelections);
    }

    private static <C> boolean hasBeenRoutedByOtherRoute(Map<String, GraphQLRoute<C>> routedFieldNames,
                                                         String fieldName, GraphQLRoute<C> route) {
        return routedFieldNames.get(fieldName) != null && !Objects.equals(routedFieldNames.get(fieldName), route);
    }

    private static void spreadChildFieldsInline(
            final String fieldName,
            final List<Selection> selections) {

        for (final Selection selection : selections) {
            final Field field = getSelectionAsField(selection);
            if (field.getName().equals(fieldName)) {
                selections.remove(selection);
                if (field.getSelectionSet() != null) {
                    selections.addAll(field.getSelectionSet().getSelections());
                }
                break;
            }
        }
    }

    private static Field getSelectionAsField(final Selection selection) {
        if (!(selection instanceof Field)) {
            throw new RuntimeException("Inline fragment and fragment spread selections aren't yet supported at the root level of a query");
        }
        return (Field) selection;
    }

    private ExecutionResult mergeResults(final Object[] results, final GraphQLRoute[] routes, C context) {
        return new ExecutionResultImpl(
                mergeResultData(results, routes, context),
                mergeErrors(Arrays.stream(results).map(GraphQLRouterErrorFactory::getErrors).collect(Collectors.toList())));
    }

    @SuppressWarnings("unchecked")
    private Object mergeResultData(final Object[] results, final GraphQLRoute[] routes, C context) {
        final List<Map<String, Object>> data = new ArrayList<>();
        for (int i = 0; i < results.length; i++) {
            final Map<String, Object> routeData = getData(results[i], routes[i]);
            if (routeData != null) {
                final GraphQLRouteRules routeRules = routes[i].getRouteRules();
                schemaTypeRenamer.renameDataResultTypeNames(routeRules.getTypeNameStrategy(), routeData);
                if (routeRules.shouldTrimSchema() && routeData.get(SCHEMA_FIELD) != null) {
                    schemaSimplifier.trimSchema(routeRules, (Map<String, Object>) routeData.get(SCHEMA_FIELD));
                }
                data.add(routeData);
            }
        }
        return mergeResultData(data, Lists.newArrayList(routes), context);
    }

    @SuppressWarnings("unchecked")
    private static Map<String, Object> getData(final Object response, final GraphQLRoute route) {
        Map<String, Object> result;
        if (response instanceof ExecutionResult) {
            result = ((ExecutionResult) response).getData();
        } else if (response instanceof Map) {
            result = (Map<String, Object>) ((Map) response).get("data");
        } else {
            throw new IllegalArgumentException("Expected a graphql.ExecutionResult or Map as the response, but got '" + response.getClass().getName() + "'");
        }

        final GraphQLRouteRules rules = route.getRouteRules();
        if (result != null && !Strings.isNullOrEmpty(rules.getRootFieldName())) {
            final Map<String, Object> newMap = new LinkedHashMap<>();
            newMap.put(rules.getRootFieldName(), result);
            if (result.containsKey(SCHEMA_FIELD)) {
                newMap.put(SCHEMA_FIELD, result.get(SCHEMA_FIELD));
                result.remove(SCHEMA_FIELD);
            }
            result = newMap;
        }
        return result;
    }

    private Object mergeResultData(final List<Map<String, Object>> data, final List<GraphQLRoute> routes, C context) {
        final Map<String, Object> result = new HashMap<>();
        for (final Map<String, Object> map : data) {
            if (map != null) {
                result.putAll(map);
            }
        }
        if (result.containsKey(SCHEMA_FIELD)) {
            result.put(SCHEMA_FIELD, schemaMerger.apply(context).mergeSchemas(data, routes));
        }
        return result;
    }

    private static List<GraphQLError> mergeErrors(final List<List<GraphQLError>> errors) {
        final List<GraphQLError> result = new ArrayList<>();
        for (final List<GraphQLError> current : errors) {
            if (current != null) {
                result.addAll(current);
            }
        }
        return result.isEmpty() ? null : result;
    }

}
