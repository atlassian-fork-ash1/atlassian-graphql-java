package com.atlassian.graphql.types;

import java.lang.reflect.Type;


/**
 * Represents a Non-Nullable field that can be used while building a schema at run time
 * using @{Link DynamicTypeBuilder}.
 */
public class NonNullWrapperType implements Type {

    private final Type wrappedType;

    NonNullWrapperType(Type wrappedType) {
        this.wrappedType = wrappedType;
    }

    public static NonNullWrapperType of(Type wrappedType) {
        return new NonNullWrapperType(wrappedType);
    }

    @Override
    public String getTypeName() {
        return wrappedType.getTypeName();
    }

    public Type getWrappedType() {
        return wrappedType;
    }
}
