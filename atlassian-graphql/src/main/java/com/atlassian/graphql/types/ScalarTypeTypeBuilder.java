package com.atlassian.graphql.types;

import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import graphql.Scalars;
import graphql.schema.GraphQLType;
import org.joda.time.DateTime;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URI;
import java.time.ZonedDateTime;
import java.util.function.Function;

import static com.atlassian.graphql.utils.ReflectionUtils.getClazz;
import static java.util.Objects.requireNonNull;

/**
 * A builder for GraphQLScalarType objects for scalar types.
 */
public class ScalarTypeTypeBuilder implements GraphQLTypeBuilder {
    @Override
    public boolean canBuildType(final Type type, final AnnotatedElement element) {
        requireNonNull(type);
        return getScalarType(getClazz(type), element) != null;
    }

    @Override
    public GraphQLType buildType(final String typeName, final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
        requireNonNull(type);
        return getScalarType(getClazz(type), element);
    }

    private GraphQLType getScalarType(final Class clazz, final AnnotatedElement element) {
        if (clazz == DateTime.class) {
            return Scalars.GraphQLString;
        }
        if (clazz == ZonedDateTime.class) {
            return Scalars.GraphQLString;
        }
        if (clazz == URI.class) {
            return Scalars.GraphQLString;
        }

        if (clazz == String.class) {
            return Scalars.GraphQLString;
        }
        if (clazz == Integer.class || clazz == int.class) {
            return Scalars.GraphQLInt;
        }
        if (clazz == Long.class || clazz == long.class) {
            return Scalars.GraphQLLong;
        }
        if (clazz == Character.class || clazz == char.class) {
            return Scalars.GraphQLChar;
        }
        if (clazz == Byte.class || clazz == byte.class) {
            return Scalars.GraphQLByte;
        }
        if (clazz == BigInteger.class) {
            return Scalars.GraphQLBigInteger;
        }
        if (clazz == BigDecimal.class) {
            return Scalars.GraphQLBigDecimal;
        }
        if (clazz == Boolean.class || clazz == boolean.class) {
            return Scalars.GraphQLBoolean;
        }
        if (clazz == Float.class || clazz == float.class ||
                clazz == Double.class || clazz == double.class) {
            return Scalars.GraphQLFloat;
        }
        return null;
    }

    @Override
    public Function<Object, Object> getValueTransformer(final Type type, final AnnotatedElement element) {
        requireNonNull(type);
        if (getClazz(type).equals(ZonedDateTime.class)) {
            return dateTime -> (dateTime != null ? dateTime.toString() : null);
        }
        if (getClazz(type).equals(DateTime.class)) {
            return dateTime -> (dateTime != null ? dateTime.toString() : null);
        }
        if (getClazz(type).equals(URI.class)) {
            return uri -> (uri != null ? uri.toString() : null);
        }
        return null;
    }
}
