package com.atlassian.graphql.types;

import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import graphql.schema.GraphQLList;
import graphql.schema.GraphQLType;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

import static com.atlassian.graphql.utils.ReflectionUtils.getClazz;
import static java.util.Objects.requireNonNull;

/**
 * A GraphQLList builder for List or Iterable types.
 */
public class ListTypeBuilder implements GraphQLTypeBuilder {
    private final GraphQLTypeBuilder elementTypeBuilder;

    public ListTypeBuilder(final GraphQLTypeBuilder elementTypeBuilder) {
        requireNonNull(elementTypeBuilder);
        this.elementTypeBuilder = elementTypeBuilder;
    }

    @Override
    public boolean canBuildType(final Type type, final AnnotatedElement element) {
        requireNonNull(type);
        final Class clazz = getClazz(type);
        return type instanceof ParameterizedType &&
               (Collection.class.isAssignableFrom(clazz) || clazz == Iterable.class);
    }

    @Override
    public GraphQLType buildType(final String typeName, final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
        requireNonNull(type);
        requireNonNull(context);

        final Type elementType = getListElementType(type);
        return context.updateFieldType(elementType, () -> {
            final GraphQLType graphqlElementType = elementTypeBuilder.buildType(elementType, context);
            return graphqlElementType != null ? new GraphQLList(graphqlElementType) : null;
        });
    }

    @Override
    public Function<Object, Object> getValueTransformer(final Type type, final AnnotatedElement element) {
        requireNonNull(type);
        final Type elementType = getListElementType(type);
        final Function<Object, Object> elementValueTransformer = elementTypeBuilder.getValueTransformer(elementType, null);
        return obj -> transformList(elementValueTransformer, obj);
    }

    protected Object transformList(final Function<Object, Object> elementValueTransformer, final Object obj) {
        if (obj == null || elementValueTransformer == null) {
            return toList((Iterable) obj);
        }
        return toList(Iterables.transform((Iterable) obj, item -> elementValueTransformer.apply(item)));
    }

    /**
     * Iterable not supported by graph-ql, convert to a List
     */
    private static List toList(final Iterable iterable) {
        return iterable == null || iterable instanceof List
               ? (List) iterable
               : Lists.newArrayList(iterable);
    }

    private static Type getListElementType(final Type type) {
        return type instanceof ParameterizedType
               ? ((ParameterizedType) type).getActualTypeArguments()[0]
               : Object.class;
    }
}
