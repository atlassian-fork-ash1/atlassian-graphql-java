package com.atlassian.graphql.types;

import com.atlassian.graphql.annotations.GraphQLDeprecate;
import com.atlassian.graphql.annotations.GraphQLDescription;
import com.atlassian.graphql.annotations.GraphQLIgnore;
import com.atlassian.graphql.annotations.GraphQLInterface;
import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.annotations.GraphQLNonNull;
import com.atlassian.graphql.annotations.GraphQLPossibleTypes;
import com.atlassian.graphql.annotations.GraphQLTypeResolver;
import com.atlassian.graphql.annotations.GraphQLUnion;
import com.atlassian.graphql.datafetcher.FieldDataFetcher;
import com.atlassian.graphql.datafetcher.MethodDataFetcher;
import com.atlassian.graphql.spi.GraphQLExtensions;
import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.atlassian.graphql.utils.AnnotationsHelper;
import com.atlassian.graphql.utils.DataFetcherFactory;
import com.atlassian.graphql.utils.GraphQLSchemaMetadata;
import com.atlassian.graphql.utils.ObjectTypeBuilderHelper;
import com.atlassian.graphql.utils.OrderedTypeElement;
import com.atlassian.graphql.utils.ReflectionUtils;
import com.google.common.base.Throwables;
import com.google.common.primitives.Primitives;
import graphql.GraphQLException;
import graphql.schema.DataFetcher;
import graphql.schema.GraphQLArgument;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLInputType;
import graphql.schema.GraphQLInterfaceType;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLOutputType;
import graphql.schema.GraphQLType;
import graphql.schema.GraphQLTypeReference;
import graphql.schema.GraphQLUnionType;
import graphql.schema.TypeResolver;

import java.lang.annotation.Annotation;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.function.Function;

import static com.atlassian.graphql.utils.AnnotationsHelper.hasAnnotation;
import static com.atlassian.graphql.utils.GraphQLUtils.unwrap;
import static com.atlassian.graphql.utils.ReflectionUtils.createParameterizedType;
import static com.atlassian.graphql.utils.ReflectionUtils.defaultValueFromAnnotation;
import static com.atlassian.graphql.utils.ReflectionUtils.getClazz;
import static java.util.Arrays.stream;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Stream.concat;

/**
 * A builder for GraphQLObjectType objects that are generated from java types with
 * {@link GraphQLName} annotated fields.
 */
public class ObjectTypeBuilder implements GraphQLTypeBuilder {
    private final GraphQLTypeBuilder typeBuilder;
    private final GraphQLExtensions extensions;

    public ObjectTypeBuilder(
            final GraphQLTypeBuilder typeBuilder,
            final GraphQLExtensions extensions) {

        this.typeBuilder = requireNonNull(typeBuilder);
        this.extensions = extensions;
    }

    protected GraphQLTypeBuilder getTypeBuilder() {
        return typeBuilder;
    }

    @Override
    public String getTypeName(final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
        return ObjectTypeBuilderHelper.buildDefaultTypeName(type, element, context, extensions);
    }

    @Override
    public boolean canBuildType(final Type type, final AnnotatedElement element) {
        return !(element instanceof Parameter);
    }

    @Override
    public GraphQLType buildType(final String typeName, final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
        requireNonNull(typeName);
        requireNonNull(type);
        requireNonNull(context);
        return buildFromTypes(typeName, new Type[]{type}, element, context);
    }

    public GraphQLType buildFromTypes(
            final String typeName,
            final Type[] types,
            final AnnotatedElement element,
            final GraphQLTypeBuilderContext context) {

        requireNonNull(typeName);
        requireNonNull(types);
        requireNonNull(context);

        // if the type has already been built
        final GraphQLUnion graphqlUnion = AnnotationsHelper.getAnnotation(getClazz(types[0]), GraphQLUnion.class);
        final GraphQLInterface graphqlInterface = AnnotationsHelper.getAnnotation(getClazz(types[0]), GraphQLInterface.class);
        if (context.hasTypeOrIsBuilding(typeName)) {
            if (graphqlInterface != null) {
                return new GraphQLTypeReference(typeName);
            } else if (graphqlUnion != null) {
                return new GraphQLTypeReference(typeName);
            }
            return new GraphQLTypeReference(typeName);
        }
        context.typeBuildStarted(typeName);

        // build union type
        final GraphQLDescription description = AnnotationsHelper.getAnnotation(getClazz(types[0]), GraphQLDescription.class);
        if (graphqlUnion != null) {
            return buildUnionType(typeName, types[0], graphqlUnion, description, context);
        }

        // allow extensions to contribute new fields and type name
        final LinkedHashMap<String, GraphQLFieldDefinition> fields = new LinkedHashMap<>();
        if (extensions != null) {
            for (final Type type : types) {
                final List<GraphQLFieldDefinition> extensionFields = new ArrayList<>();
                extensions.contributeFields(typeName, type, extensionFields, context);
                for (final GraphQLFieldDefinition field : extensionFields) {
                    fields.put(field.getName(), field);
                }
            }
        }

        // build the primary fields
        for (final Type type : types) {
            buildFields(fields, element, type, context);
        }
        if (fields.isEmpty()) {
            return null;
        }

        // build the type
        final GraphQLType graphqlType = buildObjectOrInterfaceType(
                typeName, types, fields, graphqlInterface, description, context);
        context.addType(graphqlType);

        if (element != null && ObjectTypeBuilderHelper.isGraphQLExperimental(element)) {
            GraphQLSchemaMetadata.markTypeExperimental(graphqlType);
        }

        return graphqlType;
    }

    private GraphQLType buildObjectOrInterfaceType(
            final String typeName,
            final Type[] types,
            final Map<String, GraphQLFieldDefinition> fields,
            final GraphQLInterface graphqlInterface,
            final GraphQLDescription description,
            final GraphQLTypeBuilderContext context) {

        final Class clazz = getClazz(types[0]);
        final GraphQLTypeResolver typeResolver = AnnotationsHelper.getAnnotation(clazz, GraphQLTypeResolver.class);

        return graphqlInterface != null
                ? buildInterfaceType(typeName, types[0], fields, graphqlInterface, typeResolver, description, context)
                : buildObjectType(typeName, types, fields, description, context);
    }

    private GraphQLType buildObjectType(
            final String typeName,
            final Type[] types,
            final Map<String, GraphQLFieldDefinition> fields,
            final GraphQLDescription description,
            final GraphQLTypeBuilderContext context) {

        // build object type
        final GraphQLObjectType.Builder builder =
                GraphQLObjectType.newObject()
                .description(description != null ? description.value() : null)
                .name(typeName);
        for (final GraphQLFieldDefinition field : fields.values()) {
            builder.field(field);
        }

        // implement interfaces
        final Collection<GraphQLOutputType> interfaceTypes = getImplementedInterfaces(types, context);
        for (final GraphQLOutputType interfaceType : interfaceTypes) {
            if (interfaceType instanceof GraphQLTypeReference) {
                builder.withInterface((GraphQLTypeReference) interfaceType);
            } else {
                builder.withInterface((GraphQLInterfaceType) interfaceType);
            }
        }
        return builder.build();
    }

    private Collection<GraphQLOutputType> getImplementedInterfaces(
            final Type[] types,
            final GraphQLTypeBuilderContext context) {

        final LinkedHashMap<String, GraphQLOutputType> interfaceTypes = new LinkedHashMap<>();
        for (final Type type : types) {
            final Class clazz = getClazz(type);
            getImplementedInterfaces(interfaceTypes, type, clazz, context);
        }
        return interfaceTypes.values();
    }

    private void getImplementedInterfaces(
            final LinkedHashMap<String, GraphQLOutputType> interfaceTypes,
            final Type originalType,
            final Class clazz,
            final GraphQLTypeBuilderContext context) {

        if (clazz == null || clazz == Object.class) {
            return;
        }
        final GraphQLInterface intf = AnnotationsHelper.getAnnotation(clazz, GraphQLInterface.class);

        // walk up the class hierarchy until we find a @GraphQLInterface
        if (intf == null) {
            getImplementedInterfaces(interfaceTypes, originalType, clazz.getSuperclass(), context);
            for (final Class interfaceType : clazz.getInterfaces()) {
                getImplementedInterfaces(interfaceTypes, originalType, interfaceType, context);
            }
            return;
        }

        // implement the interface, if our class is specified on @GraphQLInterface.possibleTypes
        if (Arrays.asList(getPossibleTypes(intf, clazz)).contains(originalType)) {
            final GraphQLOutputType interfaceType = (GraphQLOutputType) typeBuilder.buildType(clazz, context);
            interfaceTypes.put(interfaceType.getName(), interfaceType);
        }
    }

    private static Type[] getPossibleTypes(GraphQLInterface intf, Class clazz) {
        final GraphQLPossibleTypes possibleTypes = AnnotationsHelper.getAnnotation(clazz, GraphQLPossibleTypes.class);

        if (possibleTypes != null) {
            return concat(
                    stream(possibleTypes.value())
                            .filter(type -> type.value().length > 0)
                            .map(type -> type.value().length == 1
                                    ? type.value()[0]
                                    : createParameterizedType(type.value()[0], stream(type.value()).skip(1).toArray(Class[]::new))
                            )
                    , stream(intf.possibleTypes())
            ).toArray(Type[]::new);
        } else {
            return intf.possibleTypes();
        }
    }

    private GraphQLType buildUnionType(
            final String typeName,
            final Type type,
            final GraphQLUnion graphqlUnion,
            final GraphQLDescription description,
            final GraphQLTypeBuilderContext context) {

        final GraphQLTypeResolver typeResolver = AnnotationsHelper.getAnnotation(getClazz(type), GraphQLTypeResolver.class);
        final GraphQLUnionType.Builder builder =
                GraphQLUnionType.newUnionType()
                .name(typeName)
                .description(description != null ? description.value() : null)
                .typeResolver(buildTypeResolver(type, typeResolver, graphqlUnion.possibleTypes(), context));

        for (final Class possibleType : graphqlUnion.possibleTypes()) {
            final GraphQLType unionType = typeBuilder.buildType(possibleType, context);
            if (unionType instanceof GraphQLTypeReference) {
                builder.possibleType((GraphQLTypeReference) unionType);
            } else {
                builder.possibleType((GraphQLObjectType) unionType);
            }
        }
        return builder.build();
    }

    private GraphQLType buildInterfaceType(
            final String typeName,
            final Type type,
            final Map<String, GraphQLFieldDefinition> fields,
            final GraphQLInterface graphqlInterface,
            final GraphQLTypeResolver typeResolver,
            final GraphQLDescription description,
            final GraphQLTypeBuilderContext context) {

        final GraphQLInterfaceType.Builder builder =
                GraphQLInterfaceType.newInterface()
                .name(typeName)
                .description(description != null ? description.value() : null)
                .typeResolver(buildTypeResolver(type, typeResolver, getPossibleTypes(graphqlInterface, getClazz(type)), context));

        for (final GraphQLFieldDefinition field : fields.values()) {
            builder.field(field);
        }
        return builder.build();
    }

    /**
     * Build a graphql interface {@link TypeResolver} object, which chooses an interface implementation
     * type from a java object.
     */
    private TypeResolver buildTypeResolver(
            final Type type,
            final GraphQLTypeResolver typeResolver,
            final Type[] annotationPossibleTypes,
            final GraphQLTypeBuilderContext context) {

        // by annotation
        if (typeResolver != null) {
            try {
                return (TypeResolver) typeResolver.value().newInstance();
            } catch (final ReflectiveOperationException ex) {
                throw Throwables.propagate(ex);
            }
        }

        // default
        final Map<Type, GraphQLOutputType> possibleTypes = buildPossibleTypesMap(annotationPossibleTypes, context);
        return buildDefaultTypeResolver(type, possibleTypes, context);
    }

    /**
     * Generates a {@link TypeResolver} that just checks the types of the java objects.
     */
    protected TypeResolver buildDefaultTypeResolver(
            final Type type,
            final Map<Type, GraphQLOutputType> possibleTypes,
            final GraphQLTypeBuilderContext context) {

        return env -> {
            for (final Map.Entry<Type, GraphQLOutputType> entry : possibleTypes.entrySet()) {
                Class clazz = getClazz(entry.getKey());
                if (clazz.isInstance(env.getObject())) {
                    return (GraphQLObjectType) unwrap(entry.getValue(), context.getTypes());
                }
            }
            return null;
        };
    }

    private Map<Type, GraphQLOutputType> buildPossibleTypesMap(
            final Type[] annotationPossibleTypes,
            final GraphQLTypeBuilderContext context) {

        final Map<Type, GraphQLOutputType> possibleTypes = new HashMap<>();
        for (final Type possibleType : annotationPossibleTypes) {
            final GraphQLType graphqlType = typeBuilder.buildType(possibleType, context);
            if (graphqlType instanceof GraphQLObjectType || graphqlType instanceof GraphQLTypeReference) {
                possibleTypes.put(possibleType, (GraphQLOutputType) graphqlType);
            }
        }
        return possibleTypes;
    }

    protected void buildFields(
            final Map<String, GraphQLFieldDefinition> fields,
            final AnnotatedElement element,
            final Type type,
            final GraphQLTypeBuilderContext context) {

        buildFields(fields, type, getClazz(type), context);
    }

    protected void buildFields(
            final Map<String, GraphQLFieldDefinition> fields,
            final Type type,
            final Type currentType,
            final GraphQLTypeBuilderContext context) {

        if (currentType == null || currentType == Object.class) {
            return;
        }

        // get superclass fields
        final Class superclass = getClazz(currentType).getSuperclass();
        if (superclass != null) {
            buildFields(fields, type, ReflectionUtils.bindGenericType(type, superclass), context);
        }

        // get class fields
        final Map<String, Member> members = new LinkedHashMap<>();
        for (final Member member : ObjectTypeBuilderHelper.getDeclaredFieldsAndMethodsInOrder(getClazz(currentType))) {
            if (!isIgnored(getClazz(type), member)) {
                final String fieldName = getFieldName(member);
                if (fieldName != null) {
                    members.put(fieldName, member);
                }
            }
        }
        for (final Member member : members.values()) {
            final GraphQLFieldDefinition fieldDefinition = buildField(type, member, fields, context);
            if (fieldDefinition != null) {
                fields.put(fieldDefinition.getName(), fieldDefinition);
            }
        }
    }

    private static boolean isIgnored(final Class currentClass, final Member member) {
        return AnnotationsHelper.getAnnotationIncludingInherited(
                currentClass,
                (AnnotatedElement) member,
                GraphQLIgnore.class) != null;
    }

    protected GraphQLFieldDefinition buildField(
            final Type type,
            final Member accessor,
            final Map<String, GraphQLFieldDefinition> fields,
            final GraphQLTypeBuilderContext context) {

        final String fieldName = getFieldName(accessor);
        if (fieldName != null && !fields.containsKey(fieldName)) {
            final Type fieldType = ObjectTypeBuilderHelper.getFieldType(type, accessor);
            return buildField(fieldName, fieldType, accessor, context);
        }
        return null;
    }

    protected GraphQLFieldDefinition buildField(
            final String fieldName,
            final Type fieldType,
            final Member accessor,
            final GraphQLTypeBuilderContext context) {

        return buildField(fieldName, fieldType, accessor, null, typeBuilder, context, extensions);
    }

    /**
     * Build a {@link GraphQLFieldDefinition} from a Method or Field.
     */
    public static GraphQLFieldDefinition buildField(
            final String fieldName,
            final Type fieldType,
            final Member accessor,
            final Object source,
            final GraphQLTypeBuilder typeBuilder,
            final GraphQLTypeBuilderContext context,
            final GraphQLExtensions extensions) {

        context.enterField(fieldName, fieldType);
        try {
            // build the field type
            final GraphQLOutputType graphqlFieldType = (GraphQLOutputType) typeBuilder.buildType(fieldType, (AnnotatedElement) accessor, context);
            if (graphqlFieldType == null) {
                throw new GraphQLException(String.format(
                        "Field '%s' of type '%s' could not be built, possibly due to lack of @GraphQLName annotated fields",
                        fieldName,
                        fieldType.getTypeName()));
            }

            // build the field
            final GraphQLFieldDefinition.Builder fieldDefinitionBuilder =
                    GraphQLFieldDefinition.newFieldDefinition()
                    .name(fieldName)
                    .type(graphqlFieldType)
                    .dataFetcher(createDataFetcher(fieldName, fieldType, accessor, source, graphqlFieldType, typeBuilder, context, extensions));
            if (accessor instanceof Method) {
                buildMethodFieldArguments(typeBuilder, context, fieldDefinitionBuilder, (Method) accessor);
            }
            applyCommonFieldAnnotations(accessor, fieldDefinitionBuilder);
            final GraphQLFieldDefinition fieldDefinition = fieldDefinitionBuilder.build();

            GraphQLSchemaMetadata.applyFieldMetadata(accessor, fieldDefinition, extensions);

            if (accessor != null && ObjectTypeBuilderHelper.isGraphQLExperimental((AnnotatedElement) accessor)) {
                GraphQLSchemaMetadata.markFieldExperimental(fieldDefinition);
            }

            return fieldDefinition;
        } finally {
            context.exitField();
        }
    }

    protected String getFieldName(final Member accessor) {
        return ObjectTypeBuilderHelper.getFieldName(accessor);
    }

    private static DataFetcher createDataFetcher(
            final String fieldName,
            final Type fieldType,
            final Member accessor,
            final Object source,
            final GraphQLOutputType graphqlFieldType,
            final GraphQLTypeBuilder typeBuilder,
            final GraphQLTypeBuilderContext context,
            final GraphQLExtensions extensions) {

        ((AccessibleObject) accessor).setAccessible(true);
        return DataFetcherFactory.createDataFetcherAndValueTransformer(
                () -> getDefaultDataFetcher(graphqlFieldType, fieldName, accessor, source, context, extensions),
                fieldName,
                fieldType,
                accessor,
                source,
                graphqlFieldType,
                typeBuilder,
                context,
                extensions);
    }

    private static DataFetcher getDefaultDataFetcher(
            final GraphQLOutputType graphqlFieldType,
            final String fieldName,
            final Member accessor,
            final Object source,
            final GraphQLTypeBuilderContext context,
            final GraphQLExtensions extensions) {

        if (accessor instanceof Field) {
            return new FieldDataFetcher(accessor.getName(), (Field) accessor);
        } else if (accessor instanceof Method) {
            return new MethodDataFetcher(
                    (Method) accessor,
                    fieldName,
                    source,
                    graphqlFieldType,
                    context.getCurrentFieldPath(),
                    context.getTypes(),
                    extensions);
        }
        throw new IllegalArgumentException("Expected Field or Method for accessor with name '" + accessor.getName() + "'");
    }

    private static void applyCommonFieldAnnotations(final Member member, final GraphQLFieldDefinition.Builder field) {
        final GraphQLDescription description = AnnotationsHelper.getAnnotation((AnnotatedElement) member, GraphQLDescription.class);
        if (description != null) {
            field.description(description.value());
        }

        final GraphQLDeprecate deprecate = AnnotationsHelper.getAnnotation((AnnotatedElement) member, GraphQLDeprecate.class);
        if (deprecate != null) {
            field.deprecate(deprecate.value());
        }
    }

    private static void buildMethodFieldArguments(
            final GraphQLTypeBuilder typeBuilder,
            final GraphQLTypeBuilderContext context,
            final GraphQLFieldDefinition.Builder fieldBuilder,
            final Method method) {

        final Annotation[][] parameterAnnotations = method.getParameterAnnotations();
        for (final OrderedTypeElement orderedParameter : getParametersInOrder(method.getParameters(), parameterAnnotations)) {
            final Parameter parameter = (Parameter) orderedParameter.getElement();
            final GraphQLName graphQLName = orderedParameter.getGraphQLName();
            final int i = orderedParameter.getIndex();
            if (graphQLName == null || graphQLName.value().startsWith("../")) {
                continue;
            }

            final Object defaultValue = defaultValueFromAnnotation(parameter, parameterAnnotations[i]);
            final Type parameterType = parameter.getParameterizedType();
            final Function<Object, Object> valueTransformer = typeBuilder.getValueTransformer(parameterType, parameter);

            final GraphQLType graphqlType = buildInputType(typeBuilder, context, parameterType, parameter);

            Object transformedDefaultValue = convertParameterDefaultValue(defaultValue, parameter.getType());
            if (valueTransformer != null) {
                transformedDefaultValue = valueTransformer.apply(transformedDefaultValue);
            }
            fieldBuilder.argument(
                    GraphQLArgument.newArgument()
                    .name(graphQLName.value())
                    .type((GraphQLInputType) graphqlType)
                    .defaultValue(transformedDefaultValue));
        }
    }

    private static OrderedTypeElement[] getParametersInOrder(final Parameter[] parameters, final Annotation[][] parameterAnnotations) {
        final TreeSet<OrderedTypeElement> result = new TreeSet<>();
        for (int i = 0; i < parameters.length; i++) {
            final Parameter parameter = parameters[i];
            result.add(new OrderedTypeElement(parameter, parameter.getName(), i, AnnotationsHelper.getAnnotation(parameterAnnotations[i], GraphQLName.class)));
        }
        return result.stream().sorted().toArray(OrderedTypeElement[]::new);
    }

    private static Object convertParameterDefaultValue(final Object value, final Class clazz) {
        if (value == null || clazz.isInstance(value)) {
            return value;
        }
        try {
            final Method method = Primitives.wrap(clazz).getMethod("valueOf", new Class[]{value.getClass()});
            return method.invoke(null, value);
        } catch (final NoSuchMethodException ex) {
            return value;
        } catch (final ReflectiveOperationException ex) {
            throw Throwables.propagate(ex);
        }
    }

    private static GraphQLType buildInputType(
            final GraphQLTypeBuilder typeBuilder,
            final GraphQLTypeBuilderContext context,
            final Type type,
            final Parameter parameter) {

        String typeName = typeBuilder.getTypeName(type, parameter, context);

        GraphQLType graphQLType;
        if (typeName != null && context.hasTypeOrIsBuilding(typeName)) {
            graphQLType = new GraphQLTypeReference(typeName);
        } else {
            graphQLType = typeBuilder.buildInputType(typeName, type, parameter, context);
            if (!(graphQLType instanceof GraphQLInputType)) {
                throw new IllegalArgumentException("Invalid query parameter type: " + type.getTypeName());
            }
        }

        if (hasAnnotation(parameter, GraphQLNonNull.class)
                && !(graphQLType instanceof graphql.schema.GraphQLNonNull)) {
            return new graphql.schema.GraphQLNonNull(graphQLType);
        } else {
            return graphQLType;
        }
    }
}
