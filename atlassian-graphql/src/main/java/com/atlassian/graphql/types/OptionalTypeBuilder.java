package com.atlassian.graphql.types;

import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import graphql.schema.GraphQLType;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Optional;
import java.util.function.Function;

import static com.atlassian.graphql.utils.ReflectionUtils.getClazz;
import static com.atlassian.graphql.utils.ReflectionUtils.unwrapWildcardType;
import static java.util.Objects.requireNonNull;

/**
 * A builder for GraphQLType objects for an {@link Optional} or
 * Optional-like java type.
 */
public class OptionalTypeBuilder<T> implements GraphQLTypeBuilder {
    private final GraphQLTypeBuilder typeBuilder;
    private final Class<T> optionalWrapperType;
    private final Function<T, Object> getOrNull;

    public OptionalTypeBuilder(
            final GraphQLTypeBuilder typeBuilder,
            final Class<T> optionalWrapperType,
            final Function<T, Object> getOrNull) {

        this.typeBuilder = requireNonNull(typeBuilder);
        this.optionalWrapperType = requireNonNull(optionalWrapperType);
        this.getOrNull = requireNonNull(getOrNull);
    }

    @Override
    public boolean canBuildType(final Type type, final AnnotatedElement element) {
        requireNonNull(type);
        return optionalWrapperType.isAssignableFrom(getClazz(type));
    }

    @Override
    public String getTypeName(final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
        requireNonNull(type);
        final Type wrappedType = getWrappedType(type);
        return typeBuilder.getTypeName(wrappedType, element, context);
    }

    @Override
    public GraphQLType buildType(final String typeName, final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
        requireNonNull(type);
        requireNonNull(context);

        final Type wrappedType = getWrappedType(type);
        return typeBuilder.buildType(typeName, wrappedType, element, context);
    }

    @Override
    public Function<Object, Object> getValueTransformer(final Type type, final AnnotatedElement element) {
        requireNonNull(type);
        final Type wrappedType = getWrappedType(type);
        final Function<Object, Object> transformer = typeBuilder.getValueTransformer(wrappedType, element);

        return obj -> {
            if (!(optionalWrapperType.isInstance(obj))) {
                return transformer != null ? transformer.apply(obj) : obj;
            }
            obj = getOrNull.apply((T) obj);
            return transformer != null ? transformer.apply(obj) : obj;
        };
    }

    private static Type getWrappedType(final Type type) {
        return unwrapWildcardType(((ParameterizedType) type).getActualTypeArguments()[0]);
    }
}
