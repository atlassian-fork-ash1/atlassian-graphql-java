package com.atlassian.graphql.types;

import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import graphql.Scalars;
import graphql.schema.GraphQLType;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Type;

public class GraphQLIDTypeBuilder implements GraphQLTypeBuilder {
    public boolean canBuildType(final Type type, final AnnotatedElement element) {
        return type == String.class
               || type == Integer.class || type == int.class
               || type == Long.class || type == long.class;
    }

    public GraphQLType buildType(final String typeName, final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
        return Scalars.GraphQLID;
    }
}
