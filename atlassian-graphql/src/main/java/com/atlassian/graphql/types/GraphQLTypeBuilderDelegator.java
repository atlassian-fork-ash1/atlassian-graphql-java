package com.atlassian.graphql.types;

import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import graphql.schema.GraphQLType;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Type;
import java.util.function.Function;

import static java.util.Objects.requireNonNull;

/**
 * An implementation of {@link GraphQLTypeBuilder} that delegates to an abstract factory method.
 */
public abstract class GraphQLTypeBuilderDelegator implements GraphQLTypeBuilder {
    @Override
    public boolean canBuildType(final Type type, final AnnotatedElement element) {
        requireNonNull(type);
        return true;
    }

    @Override
    public String getTypeName(final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
        requireNonNull(type);
        return getTypeBuilder(type, element).getTypeName(type, element, context);
    }

    @Override
    public GraphQLType buildType(final String typeName, final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
        requireNonNull(type);
        requireNonNull(context);
        return getTypeBuilder(type, element).buildType(typeName, type, element, context);
    }

    @Override
    public Function<Object, Object> getValueTransformer(final Type type, final AnnotatedElement element) {
        requireNonNull(type);
        return getTypeBuilder(type, element).getValueTransformer(type, element);
    }

    /**
     * The factory method that returns the {@link GraphQLTypeBuilder} for the java type.
     * @param type A java type, usually a Class or ParameterizedType
     * @param element The field, method or parameter
     * @return The graph-ql type builder for the type
     */
    protected abstract GraphQLTypeBuilder getTypeBuilder(final Type type, final AnnotatedElement element);
}
