package com.atlassian.graphql.types;

import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import graphql.schema.GraphQLType;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

import static com.atlassian.graphql.utils.ReflectionUtils.getClazz;
import static java.util.Objects.requireNonNull;

/**
 * A builder for GraphQL members that return types wrapped in CompletableFuture.
 */
public class CompletableFutureTypeBuilder implements GraphQLTypeBuilder {
    private final GraphQLTypeBuilder typeBuilder;

    public CompletableFutureTypeBuilder(final GraphQLTypeBuilder typeBuilder) {
        this.typeBuilder = typeBuilder;
    }

    @Override
    public String getTypeName(final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
        requireNonNull(type);
        return typeBuilder.getTypeName(getFutureType(type), element, context);
    }

    @Override
    public boolean canBuildType(final Type type, final AnnotatedElement element) {
        return CompletableFuture.class.isAssignableFrom(getClazz(type))
               && typeBuilder.canBuildType(getFutureType(type), element);
    }

    @Override
    public GraphQLType buildType(final String typeName, final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
        final Type futureType = getFutureType(type);
        return context.updateFieldType(futureType, () -> typeBuilder.buildType(typeName, futureType, element, context));
    }

    private static Type getFutureType(final Type type) {
        return type instanceof ParameterizedType
               ? ((ParameterizedType) type).getActualTypeArguments()[0]
               : Object.class;
    }
}
