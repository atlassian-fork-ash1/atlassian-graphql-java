package com.atlassian.graphql.types;

import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.atlassian.graphql.utils.AnnotationsHelper;
import com.google.common.base.Throwables;
import graphql.schema.GraphQLEnumType;
import graphql.schema.GraphQLType;
import graphql.schema.GraphQLTypeReference;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.Type;

import static com.atlassian.graphql.utils.ReflectionUtils.getClazz;

public class EnumTypeBuilder implements GraphQLTypeBuilder {
    @Override
    public boolean canBuildType(final Type type, final AnnotatedElement element) {
        return type instanceof Class && Enum.class.isAssignableFrom((Class) type);
    }

    @Override
    public GraphQLType buildType(
            final String typeName,
            final Type type,
            final AnnotatedElement element,
            final GraphQLTypeBuilderContext context) {

        if (context.hasTypeOrIsBuilding(typeName)) {
            return new GraphQLTypeReference(typeName);
        }

        final GraphQLEnumType.Builder builder = GraphQLEnumType.newEnum().name(typeName);
        for (final Field field : getClazz(type).getDeclaredFields()) {
            if (field.isEnumConstant()) {
                final GraphQLName graphQLName = AnnotationsHelper.getAnnotation(field, GraphQLName.class);
                try {
                    final String valueName = graphQLName != null ? graphQLName.value() : field.getName();
                    builder.value(valueName, field.get(null));
                } catch (ReflectiveOperationException ex) {
                    throw Throwables.propagate(ex);
                }
            }
        }
        final GraphQLEnumType graphqlType = builder.build();
        context.addType(graphqlType);
        return graphqlType;
    }
}
