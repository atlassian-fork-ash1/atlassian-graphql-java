package com.atlassian.graphql.types;

import com.atlassian.graphql.annotations.GraphQLDescription;
import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.spi.GraphQLExtensions;
import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.atlassian.graphql.utils.AnnotationsHelper;
import com.atlassian.graphql.utils.ObjectTypeBuilderHelper;
import graphql.schema.GraphQLInputObjectField;
import graphql.schema.GraphQLInputObjectType;
import graphql.schema.GraphQLInputType;
import graphql.schema.GraphQLType;
import graphql.schema.GraphQLTypeReference;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Member;
import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.atlassian.graphql.utils.ReflectionUtils.getClazz;
import static java.util.Objects.requireNonNull;

/**
 * A builder for GraphQLInputObjectType objects that are generated from java types with
 * {@link GraphQLName} annotated fields.
 */
public class InputObjectTypeBuilder implements GraphQLTypeBuilder {
    private final GraphQLTypeBuilder typeBuilder;
    private final GraphQLExtensions extensions;

    public InputObjectTypeBuilder(
            final GraphQLTypeBuilder typeBuilder,
            final GraphQLExtensions extensions) {

        this.typeBuilder = requireNonNull(typeBuilder);
        this.extensions = extensions;
    }

    @Override
    public String getTypeName(final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
        return ObjectTypeBuilderHelper.buildDefaultTypeName(type, element, context, extensions);
    }

    @Override
    public boolean canBuildType(final Type type, final AnnotatedElement element) {
        return true;
    }

    @Override
    public GraphQLType buildType(final String typeName, final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
        requireNonNull(typeName);
        requireNonNull(type);
        requireNonNull(context);

        if (context.hasTypeOrIsBuilding(typeName)) {
            return new GraphQLTypeReference(typeName);
        }

        // build union type
        final GraphQLDescription description = AnnotationsHelper.getAnnotation(getClazz(type), GraphQLDescription.class);

        // build the primary fields
        Map<String, GraphQLInputObjectField> fields = new LinkedHashMap<>();
        buildFields(fields, type, context);
        if (fields.isEmpty()) {
            return null;
        }

        // build the type
        final GraphQLInputObjectType graphqlType = buildInputObjectType(typeName, fields, description);
        context.addType(graphqlType);
        return graphqlType;
    }

    private GraphQLInputObjectType buildInputObjectType(
            final String typeName,
            final Map<String, GraphQLInputObjectField> fields,
            final GraphQLDescription description) {

        final GraphQLInputObjectType.Builder builder =
                GraphQLInputObjectType.newInputObject()
                .description(description != null ? description.value() : null)
                .name(typeName);
        for (final GraphQLInputObjectField field : fields.values()) {
            builder.field(field);
        }
        return builder.build();
    }

    protected void buildFields(
            final Map<String, GraphQLInputObjectField> fields,
            final Type type,
            final GraphQLTypeBuilderContext context) {

        buildFields(fields, type, getClazz(type), context);
    }

    protected void buildFields(
            final Map<String, GraphQLInputObjectField> fields,
            final Type type,
            final Class currentClass,
            final GraphQLTypeBuilderContext context) {

        if (currentClass == null || currentClass == Object.class) {
            return;
        }

        // get superclass fields
        buildFields(fields, type, currentClass.getSuperclass(), context);

        // get class fields
        for (final Member member : ObjectTypeBuilderHelper.getDeclaredFieldsAndMethodsInOrder(currentClass)) {
            final GraphQLInputObjectField field = buildField(type, member, fields, context);
            if (field != null) {
                fields.put(field.getName(), field);
            }
        }
    }

    protected GraphQLInputObjectField buildField(
            final Type type,
            final Member accessor,
            final Map<String, GraphQLInputObjectField> fields,
            final GraphQLTypeBuilderContext context) {

        final String fieldName = ObjectTypeBuilderHelper.getFieldName(accessor);
        if (fieldName != null && !fields.containsKey(fieldName)) {
            final Type fieldType = ObjectTypeBuilderHelper.getFieldType(type, accessor);
            return buildField(fieldName, fieldType, accessor, context);
        }
        return null;
    }

    protected GraphQLInputObjectField buildField(
            final String fieldName,
            final Type fieldType,
            final Member accessor,
            final GraphQLTypeBuilderContext context) {

        context.enterField(fieldName, fieldType);
        try {
            // build the field type
            final GraphQLInputType graphqlFieldType = (GraphQLInputType) typeBuilder.buildType(fieldType, (AnnotatedElement) accessor, context);
            if (graphqlFieldType == null) {
                return null;
            }

            // build the field
            final GraphQLInputObjectField.Builder fieldBuilder =
                    GraphQLInputObjectField.newInputObjectField()
                    .name(fieldName)
                    .type(graphqlFieldType);

            // add a description
            final GraphQLDescription description = AnnotationsHelper.getAnnotation((AnnotatedElement) accessor, GraphQLDescription.class);
            if (description != null) {
                fieldBuilder.description(description.value());
            }
            return fieldBuilder.build();
        } finally {
            context.exitField();
        }
    }
}
