package com.atlassian.graphql.types;

import java.lang.reflect.Type;
import java.util.Map;

import static java.util.Objects.requireNonNull;

/**
 * Provides a Type object with dynamically defined field types.
 */
public class DynamicType implements Type {
    private final String typeName;
    private final Map<String, Type> fieldTypes;

    public DynamicType(final String typeName, final Map<String, Type> fieldTypes) {
        requireNonNull(typeName);
        requireNonNull(fieldTypes);
        this.typeName = typeName;
        this.fieldTypes = fieldTypes;
    }

    public String getTypeName() {
        return typeName;
    }

    public Map<String, Type> getFieldTypes() {
        return fieldTypes;
    }
}
