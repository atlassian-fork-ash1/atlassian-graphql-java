package com.atlassian.graphql.types;

import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.atlassian.graphql.utils.ReflectionUtils;
import com.google.common.collect.ImmutableMap;
import com.google.common.reflect.TypeParameter;
import com.google.common.reflect.TypeToken;
import graphql.schema.GraphQLType;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static com.atlassian.graphql.utils.ReflectionUtils.getClazz;
import static java.util.Objects.requireNonNull;

/**
 * A GraphQLList type builder for key/value Map objects.
 */
public class MapTypeBuilder implements GraphQLTypeBuilder {
    private final GraphQLTypeBuilder typeBuilder;

    public MapTypeBuilder(final GraphQLTypeBuilder typeBuilder) {
        this.typeBuilder = requireNonNull(typeBuilder);
    }

    @Override
    public String getTypeName(final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
        final String typeName = ReflectionUtils.getAnnotatedGraphQLTypeName(type, element);
        if (typeName != null) {
            return typeName;
        }

        final ParameterizedType parameterizedType = (ParameterizedType) type;
        final Type[] typeArguments = parameterizedType.getActualTypeArguments();
        return "MapOf"
               + typeBuilder.getTypeName(typeArguments[0], null, context)
               + "To"
               + typeBuilder.getTypeName(typeArguments[1], null, context);
    }

    static boolean isMapType(final Type type) {
        requireNonNull(type);
        return type instanceof ParameterizedType
                && (Map.class.isAssignableFrom(getClazz(type))
                    || getClazz(type).getSimpleName().equals(ImmutableMap.class.getName()));
    }

    @Override
    public boolean canBuildType(final Type type, final AnnotatedElement element) {
        return isMapType(type);
    }

    @Override
    public GraphQLType buildType(final String typeName, final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
        requireNonNull(type);
        requireNonNull(context);

        final ParameterizedType parameterizedType = (ParameterizedType) type;
        final Type listTypeParameter = new DynamicType(
                typeName,
                ImmutableMap.of(
                    "key", parameterizedType.getActualTypeArguments()[0],
                    "value", parameterizedType.getActualTypeArguments()[1]));

        final Type listOfKeyValuePair = listOf(TypeToken.of(listTypeParameter));
        return typeBuilder.buildType(typeName, listOfKeyValuePair, element, context);
    }

    private static <T> Type listOf(final TypeToken<T> type) {
        return new TypeToken<List<T>>(){}
               .where(new TypeParameter<T>(){}, type)
               .getType();
    }

    @Override
    public Function<Object, Object> getValueTransformer(final Type type, final AnnotatedElement element) {
        requireNonNull(type);
        return MapTypeBuilder::mapToKeyValueList;
    }

    private static Object mapToKeyValueList(final Object obj) {
        if (!(obj instanceof Map)) {
            return null;
        }

        final Map<?, ?> map = (Map<?, ?>) obj;
        final List<Map> list = new ArrayList<>();
        for (final Map.Entry entry : map.entrySet()) {
            list.add(ImmutableMap.of("key", entry.getKey(), "value", entry.getValue()));
        }
        return list;
    }
}
