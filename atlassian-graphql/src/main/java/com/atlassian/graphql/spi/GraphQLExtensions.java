package com.atlassian.graphql.spi;

import graphql.execution.instrumentation.Instrumentation;
import graphql.execution.instrumentation.SimpleInstrumentation;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLOutputType;
import graphql.schema.GraphQLType;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Member;
import java.lang.reflect.Type;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;

/**
 * Provides an extension point for graphql schema generation and query handling.
 */
public interface GraphQLExtensions extends GraphQLTypeContributor {
    /**
     * Contribute a new name for a graphql {@link GraphQLObjectType} object.
     */
    default String contributeTypeName(
            final String typeName,
            final Type type,
            final GraphQLTypeBuilderContext context) {

        return null;
    }

    /**
     * Contribute additional fields for a {@link GraphQLObjectType} object.
     */
    default void contributeFields(
            final String typeName,
            final Type type,
            final List<GraphQLFieldDefinition> fields,
            final GraphQLTypeBuilderContext context) {
    }

    /**
     * Get an additional set of {@link GraphQLTypeBuilder} objects that are used to build
     * {@link GraphQLType} objects from java types.
     */
    default List<GraphQLTypeBuilder> getAdditionalTypeBuilders(
            final GraphQLTypeBuilder typeBuilder,
            final GraphQLExtensions extensions) {

        return null;
    }

    /**
     * Get a data fetcher that can inject logic before and after an existing data fetcher.
     */
    default DataFetcher getDataFetcherThunk(final Type type, final Member accessor, final DataFetcher dataFetcher) {
        return dataFetcher;
    }

    /**
     * Get the data fetcher that's invoked to get a field value or invoke a field getter method.
     */
    default DataFetcher getDataFetcher(
            final Supplier<DataFetcher> defaultDataFetcher,
            final String fieldName,
            final Member accessor,
            final Object source,
            final GraphQLOutputType responseType,
            final GraphQLTypeBuilder typeBuilder,
            final GraphQLTypeBuilderContext context,
            final GraphQLExtensions extensions) {

        return null;
    }

    /**
     * Create a value transformer for any field in the graph.
     */
    default Function<Object, Object> getValueTransformer(final Type type, final AnnotatedElement element) {
        return null;
    }

    /**
     * When evaluating an 'expand' parameter, get the paths from which the expansion fields
     * are evaluated from (for example the 'results' field of a PageResponse object).
     */
    default List<String> getExpansionRootPaths(final Type responseType) {
        return null;
    }

    /**
     * Get whether an accessor is an expansion field, that must be included in the
     * 'expand' parameter.
     */
    default boolean isExpansionField(final Member accessor) {
        return false;
    }

    /**
     * Get a GraphQL {@link Instrumentation} object.
     */
    default Instrumentation getInstrumentation() {
        return SimpleInstrumentation.INSTANCE;
    }

    /**
     * Adapt a {@link GraphQLTypeContributor} to a {@link GraphQLExtensions}.
     */
    static GraphQLExtensions of(final GraphQLTypeContributor typeContributor) {
        requireNonNull(typeContributor);
        if (typeContributor instanceof GraphQLExtensions) {
            return (GraphQLExtensions) typeContributor;
        }
        return new GraphQLExtensions() {
            @Override
            public String contributeTypeName(
                    final String typeName,
                    final Type type,
                    final GraphQLTypeBuilderContext context) {

                return typeContributor.contributeTypeName(typeName, type, context);
            }

            @Override
            public void contributeFields(
                    final String typeName,
                    final Type type,
                    final List<GraphQLFieldDefinition> fields,
                    final GraphQLTypeBuilderContext context) {

                typeContributor.contributeFields(typeName, type, fields, context);
            }
        };
    }
}
