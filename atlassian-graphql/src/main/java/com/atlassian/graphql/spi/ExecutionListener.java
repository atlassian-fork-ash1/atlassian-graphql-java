package com.atlassian.graphql.spi;

import graphql.execution.ExecutionContext;
import graphql.execution.ExecutionStrategyParameters;

/**
 * When implemented on the root object, is invoked before and after fields are resolved and executed.
 */
public interface ExecutionListener {
    /**
     * Called on the root object before field resolution begins.
     */
    void beforeResolveField(final ExecutionContext executionContext, final ExecutionStrategyParameters parameters);

    /**
     * Called on the root object after field resolution ends.
     */
    void afterResolveField(final ExecutionContext executionContext, final ExecutionStrategyParameters parameters);
}
