package com.atlassian.graphql;

import graphql.execution.ExecutionPath;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;

/**
 * This exception may be thrown from a {@link DataFetcher} to wrap the exception that's thrown from a method call.
 */
public class GraphQLFetcherException extends RuntimeException {
    private final ExecutionPath path;

    public GraphQLFetcherException(DataFetchingEnvironment env, String message) {
        this(getPath(env), message);
    }

    public GraphQLFetcherException(DataFetchingEnvironment env, Exception cause) {
        this(getPath(env), cause);
    }

    /**
     * Constructor
     *
     * @param path  The path to the field that caused the error
     * @param cause The exception that was thrown
     */
    public GraphQLFetcherException(final ExecutionPath path, final Exception cause) {
        super(cause.getMessage(), cause);
        this.path = path;
    }

    public GraphQLFetcherException(final ExecutionPath path, final String message) {
        super(message);
        this.path = path;
    }

    /**
     * Get the field path in which the error took place.
     */
    public ExecutionPath getPath() {
        return path;
    }

    private static ExecutionPath getPath(DataFetchingEnvironment env) {
        return env != null && env.getExecutionStepInfo() != null ? env.getExecutionStepInfo().getPath() : null;
    }
}
