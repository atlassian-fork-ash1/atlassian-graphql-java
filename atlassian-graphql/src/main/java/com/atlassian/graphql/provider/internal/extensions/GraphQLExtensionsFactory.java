package com.atlassian.graphql.provider.internal.extensions;

import com.atlassian.graphql.spi.GraphQLProviders;
import com.atlassian.graphql.provider.RootProviderGraphQLTypeBuilder;
import com.atlassian.graphql.provider.internal.ProviderFieldTree;
import com.atlassian.graphql.spi.GraphQLExtensions;
import com.atlassian.graphql.spi.GraphQLTypeContributor;

import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import static com.atlassian.graphql.spi.CombinedGraphQLExtensions.combine;
import static java.util.Objects.requireNonNull;

/**
 * Returns all of the {@link GraphQLExtensions} objects that will be used to build a graphql schema.
 */
public class GraphQLExtensionsFactory {
    public static GraphQLExtensions getExtensions(
            final RootProviderGraphQLTypeBuilder providerClassGraphQLTypeBuilder,
            final List<GraphQLProviders> providers,
            final ProviderFieldTree fieldTree,
            final GraphQLExtensions otherExtensions,
            final BiConsumer<Object, Exception> errorHandler) {

        requireNonNull(providerClassGraphQLTypeBuilder);
        requireNonNull(providers);
        requireNonNull(fieldTree);

        // get plugin extensions specified via <graphql>
        final List<GraphQLExtensions> extensions = getProviderGraphQLExtensions(providers);

        // support embedding a provider field within the response field tree of another provider field
        final ProviderFieldNestedInProviderMethodResponseExtension nestedProviderExtension =
                new ProviderFieldNestedInProviderMethodResponseExtension(
                        providerClassGraphQLTypeBuilder,
                        fieldTree,
                        errorHandler);
        extensions.add(nestedProviderExtension);

        // other extensions
        if (otherExtensions != null) {
            extensions.add(otherExtensions);
        }

        final GraphQLExtensions allExtensions = combine(extensions.toArray(new GraphQLExtensions[extensions.size()]));
        nestedProviderExtension.setAllExtensions(allExtensions);
        return allExtensions;
    }

    /**
     * Get all 'providers' that implements the {@link GraphQLTypeContributor} or
     * {@link GraphQLExtensions} interface.
     */
    public static List<GraphQLExtensions> getProviderGraphQLExtensions(final List<GraphQLProviders> providers) {
        return providers.stream()
               .map(GraphQLProviders::getExtensions)
               .filter(Objects::nonNull)
               .collect(Collectors.toList());
    }
}
