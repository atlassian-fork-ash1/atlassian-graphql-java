package com.atlassian.graphql.provider.internal;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * Represents a tree of provider methods that represent query fields.
 */
public class ProviderFieldTree {
    private final String fieldName;
    private String typeName;
    private Object provider;
    private Method providerMethod;
    private final List<ProviderFieldTree> fields = new ArrayList<>();

    public ProviderFieldTree(final String fieldName, final String typeName) {
        requireNonNull(fieldName);
        this.fieldName = fieldName;
        this.typeName = typeName;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(final String typeName) {
        this.typeName = typeName;
    }

    public Object getProvider() {
        return provider;
    }

    public void setProvider(final Object provider) {
        this.provider = provider;
    }

    public Method getProviderMethod() {
        return providerMethod;
    }

    public void setProviderMethod(final Method providerMethod) {
        this.providerMethod = providerMethod;
    }

    public List<ProviderFieldTree> getFields() {
        return fields;
    }

    public ProviderFieldTree getChildField(final String childFieldName) {
        for (final ProviderFieldTree field : fields) {
            if (field.getFieldName().equals(childFieldName)) {
                return field;
            }
        }
        return null;
    }

    public ProviderFieldTree getOrCreateChildField(final String childFieldName, final String typeName) {
        ProviderFieldTree field = getChildField(childFieldName);
        if (field == null) {
            field = new ProviderFieldTree(childFieldName, typeName);
            fields.add(field);
        } else if (typeName != null && field.getTypeName() != null && !field.getTypeName().equals(typeName)) {
            throw new IllegalArgumentException("typeName has changed from '" + field.getTypeName() + "' to '" + typeName + "'");
        }
        field.setTypeName(typeName != null ? typeName : field.getTypeName());
        return field;
    }

    public ProviderFieldTree getFieldFromPath(final List<String> fieldPath) {
        return getFieldFromPath(fieldPath, 0);
    }

    private ProviderFieldTree getFieldFromPath(final List<String> fieldPath, final int i) {
        if (i == fieldPath.size()) {
            return this;
        }
        final ProviderFieldTree childFieldTree = getChildField(fieldPath.get(i));
        if (childFieldTree == null) {
            return null;
        }
        return childFieldTree.getFieldFromPath(fieldPath, i + 1);
    }
}
