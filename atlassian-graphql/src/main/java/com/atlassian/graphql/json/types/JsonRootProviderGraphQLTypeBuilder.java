package com.atlassian.graphql.json.types;

import com.atlassian.graphql.provider.RootProviderGraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLExtensions;

/**
 * A builder for GraphQLObjectType objects with query fields for a set of provider object methods
 * possibly with Json (jackson) serialization annotations.
 *
 * @see JsonRootGraphQLTypeBuilder
 */
public class JsonRootProviderGraphQLTypeBuilder extends RootProviderGraphQLTypeBuilder {
    public JsonRootProviderGraphQLTypeBuilder() {
        this(null);
    }

    /**
     * Constructor
     * @param extensions Optionally provides extensions to the graph-ql type system
     */
    public JsonRootProviderGraphQLTypeBuilder(final GraphQLExtensions extensions) {
        super(JsonRootGraphQLTypeBuilder::new, extensions);
    }
}
