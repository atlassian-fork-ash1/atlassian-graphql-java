package com.atlassian.graphql.json.jersey;

/**
 * Handled enum conversion
 */
public final class EnumArgumentConverter implements ArgumentConverter {
    @Override
    public boolean test(Class<?> clazz, Object value) {
        return clazz.isEnum() && clazz.isAssignableFrom(value.getClass());
    }
}
