package com.atlassian.graphql.json.types;

import com.atlassian.graphql.spi.GraphQLExtensions;
import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.types.ObjectTypeBuilder;
import com.atlassian.graphql.types.RootGraphQLTypeBuilder;

import java.util.List;

/**
 * A builder for building GraphQLType objects from java types annotated with
 * Json (jackson) serialization annotations. The following annotation types are supported:
 *
 * - JsonProperty - fields only
 * - JsonTypeInfo - use=NAME, include=PROPERTY only
 * - JsonSubTypes
 * - JsonDeserialize
 * - JsonPropertyOrder
 * - JsonValue
 *
 * Unsupported jackson annotations will cause an exception to be thrown.
 */
public class JsonRootGraphQLTypeBuilder extends RootGraphQLTypeBuilder {
    private final JsonObjectTypeBuilder objectTypeBuilder;

    public JsonRootGraphQLTypeBuilder() {
        this(null);
    }

    public JsonRootGraphQLTypeBuilder(final GraphQLExtensions extensions) {
        super(extensions);
        objectTypeBuilder = new JsonObjectTypeBuilder(this, extensions);
    }

    @Override
    protected ObjectTypeBuilder getObjectTypeBuilder() {
        return objectTypeBuilder;
    }

    @Override
    protected List<GraphQLTypeBuilder> createTypeBuilders(final GraphQLTypeBuilder rootTypeBuilder, final boolean inputTypes) {
        final List<GraphQLTypeBuilder> typeBuilders = super.createTypeBuilders(rootTypeBuilder, inputTypes);
        typeBuilders.add(new JsonSerializeAnnotatedTypeBuilder(rootTypeBuilder));
        typeBuilders.add(new JsonValueOnMethodTypeBuilder(rootTypeBuilder));
        return typeBuilders;
    }
}
