package com.atlassian.graphql.json.jersey;

import static com.atlassian.graphql.utils.ReflectionUtils.getClazz;

/**
 * Handles same type conversion. I.e the value is <em>already</em> of the expected java type
 */
public final class SameTypeArgumentConverter implements ArgumentConverter {

    public boolean test(Class<?> type, Object value) {
        return value.getClass() == getClazz(type);
    }
}
