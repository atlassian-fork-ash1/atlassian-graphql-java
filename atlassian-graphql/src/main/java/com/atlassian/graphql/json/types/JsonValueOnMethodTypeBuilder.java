package com.atlassian.graphql.json.types;

import com.atlassian.graphql.GraphQLFetcherException;
import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.atlassian.graphql.utils.AnnotationsHelper;
import com.fasterxml.jackson.annotation.JsonValue;
import graphql.execution.ExecutionPath;
import graphql.schema.GraphQLType;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.WeakHashMap;
import java.util.function.Function;

import static com.atlassian.graphql.datafetcher.MethodDataFetcher.unwrapError;
import static com.atlassian.graphql.utils.ReflectionUtils.getClazz;
import static java.util.Objects.requireNonNull;

/**
 * A GraphQLType builder for jackson @JsonValue annotated java classes.
 */
public class JsonValueOnMethodTypeBuilder implements GraphQLTypeBuilder {
    private static final WeakHashMap<Class, Object> jsonValueMethodCache = new WeakHashMap<>();

    private final GraphQLTypeBuilder typeBuilder;

    public JsonValueOnMethodTypeBuilder(final GraphQLTypeBuilder typeBuilder) {
        this.typeBuilder = requireNonNull(typeBuilder);
    }

    @Override
    public boolean canBuildType(final Type type, final AnnotatedElement element) {
        requireNonNull(type);
        return getJsonValueMethod(getClazz(type)) != null;
    }

    @Override
    public GraphQLType buildType(final String typeName, final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
        requireNonNull(type);
        requireNonNull(context);
        final Class clazz = getClazz(type);
        return typeBuilder.buildType(typeName, getJsonValueType(clazz), getJsonValueMethod(clazz), context);
    }

    @Override
    public Function<Object, Object> getValueTransformer(final Type type, final AnnotatedElement element) {
        requireNonNull(type);

        final Class clazz = getClazz(type);
        final Method method = getJsonValueMethod(clazz);
        return obj -> {
            try {
                return obj == null || !method.getDeclaringClass().isInstance(obj)
                        ? obj
                        : method.invoke(obj);
            } catch (ReflectiveOperationException ex) {
                throw new GraphQLFetcherException((ExecutionPath) null, unwrapError(ex));
            }
        };
    }

    private Type getJsonValueType(final Class clazz) {
        final Method method = getJsonValueMethod(clazz);
        return method != null ? method.getGenericReturnType() : null;
    }

    private Method getJsonValueMethod(final Class clazz) {
        final Object cached = jsonValueMethodCache.get(clazz);
        if (cached != null) {
            return cached instanceof Method ? (Method) cached : null;
        }
        final Method method = getJsonValueMethodUncached(clazz);
        jsonValueMethodCache.put(clazz, method != null ? method : new Object());
        return method;
    }

    private Method getJsonValueMethodUncached(final Class clazz) {
        for (final Method method : clazz.getMethods()) {
            final JsonValue jsonValue = AnnotationsHelper.getAnnotation(method, JsonValue.class);
            if (jsonValue != null && jsonValue.value()) {
                return method;
            }
        }
        return null;
    }
}
