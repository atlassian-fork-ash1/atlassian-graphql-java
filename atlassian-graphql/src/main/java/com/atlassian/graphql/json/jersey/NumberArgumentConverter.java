package com.atlassian.graphql.json.jersey;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Handles numbers conversion
 */
public final class NumberArgumentConverter implements ArgumentConverter {
    @Override
    public boolean test(Class<?> clazz, Object value) {
        return value instanceof Number && Number.class.isAssignableFrom(clazz);
    }

    @Override
    public Object apply(Class<?> clazz, Object value) {
        return convertNumber((Number) value, clazz);
    }

    private static Object convertNumber(final Number number, final Class clazz) {
        if (clazz == Byte.class) {
            return number.byteValue();
        }
        if (clazz == Double.class) {
            return number.doubleValue();
        }
        if (clazz == Float.class) {
            return number.floatValue();
        }
        if (clazz == Short.class) {
            return number.shortValue();
        }
        if (clazz == Integer.class) {
            return number.intValue();
        }
        if (clazz == Long.class) {
            return number.longValue();
        }
        if (clazz == BigInteger.class) {
            return BigInteger.valueOf(number.longValue());
        }
        if (clazz == BigDecimal.class) {
            return number instanceof Long ? new BigDecimal(number.longValue()) : BigDecimal.valueOf(number.doubleValue());
        }
        throw new RuntimeException("Unsupported numeric type '" + clazz.getName() + "'");
    }
}
