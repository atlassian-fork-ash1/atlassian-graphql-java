package com.atlassian.graphql.json.jersey;

import com.atlassian.graphql.GraphQLFetcherException;
import com.atlassian.graphql.spi.GraphQLExtensions;
import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.google.common.collect.ImmutableList;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.GraphQLOutputType;

import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.List;
import java.util.function.Supplier;

import static com.atlassian.graphql.json.jersey.JerseyResourceMethodDataFetcher.DEFAULT_ARGUMENT_CONVERTERS;
import static java.util.Collections.emptyList;

/**
 * An extension that supports Jersey parameter semantics for provider objects.
 * See {@link JerseyResourceMethodDataFetcher} for more information.
 */
public class JerseyResourceMethodExtensions implements GraphQLExtensions {

    private final List<ArgumentConverter> argumentConverters;

    public JerseyResourceMethodExtensions() {
        this(emptyList());
    }

    public JerseyResourceMethodExtensions(List<ArgumentConverter> argumentConverters) {
        this.argumentConverters = argumentConverters.isEmpty() ?
                DEFAULT_ARGUMENT_CONVERTERS :
                ImmutableList.<ArgumentConverter>builder()
                        .addAll(argumentConverters) // prepend custom argument converters
                        .addAll(DEFAULT_ARGUMENT_CONVERTERS)
                        .build();
    }

    @Override
    public DataFetcher getDataFetcher(
            final Supplier<DataFetcher> defaultDataFetcher,
            final String fieldName,
            final Member accessor,
            final Object source,
            final GraphQLOutputType responseType,
            final GraphQLTypeBuilder typeBuilder,
            final GraphQLTypeBuilderContext context,
            final GraphQLExtensions extensions) {

        if (!(accessor instanceof Method)) {
            return null;
        }
        return new JerseyResourceMethodDataFetcher(
                (Method) accessor,
                fieldName,
                source,
                responseType,
                context.getCurrentFieldPath(),
                context.getTypes(),
                extensions,
                argumentConverters) {

            @Override
            protected Object transformResponseStatus(final DataFetchingEnvironment env, final Object result, final int status) {
                return JerseyResourceMethodExtensions.this.transformResponseStatus(env, result, status);
            }
        };
    }

    /**
     * Override to return a different response or throw an exception based on the value of Response.getStatus().
     */
    protected Object transformResponseStatus(final DataFetchingEnvironment env, final Object response, final int status) {
        if (status != 200) {
            throw new GraphQLFetcherException(
                    env.getExecutionStepInfo() != null ? env.getExecutionStepInfo().getPath() : null,
                    new GraphQLRestException("Error code " + status, status));
        }
        return response;
    }
}
