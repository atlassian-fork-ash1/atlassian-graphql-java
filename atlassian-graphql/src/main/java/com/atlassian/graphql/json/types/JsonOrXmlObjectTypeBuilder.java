package com.atlassian.graphql.json.types;

import com.atlassian.graphql.spi.GraphQLExtensions;
import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.utils.AnnotationsHelper;
import com.atlassian.graphql.utils.ObjectTypeBuilderHelper;
import com.google.common.base.Strings;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Member;

/**
 * A builder for GraphQLObjectType objects that are generated from java types annotated with
 * Json (jackson), {@link XmlElement} and {@link XmlAttribute} serialization annotations.
 */
public class JsonOrXmlObjectTypeBuilder extends JsonObjectTypeBuilder {
    public JsonOrXmlObjectTypeBuilder(
            final GraphQLTypeBuilder typeBuilder,
            final GraphQLExtensions extensions) {

        super(typeBuilder, extensions);
    }

    @Override
    protected String getFieldName(final Member accessor) {
        final XmlElement xmlElement = AnnotationsHelper.getAnnotation((AnnotatedElement) accessor, XmlElement.class);
        if (xmlElement != null) {
            return !Strings.isNullOrEmpty(xmlElement.name()) && !xmlElement.name().equals("##default")
                    ? xmlElement.name()
                    : ObjectTypeBuilderHelper.getDefaultFieldName(accessor);
        }
        final XmlAttribute xmlAttribute = AnnotationsHelper.getAnnotation((AnnotatedElement) accessor, XmlAttribute.class);
        if (xmlAttribute != null) {
            return !Strings.isNullOrEmpty(xmlAttribute.name()) && !xmlAttribute.name().equals("##default")
                    ? xmlAttribute.name()
                    : ObjectTypeBuilderHelper.getDefaultFieldName(accessor);
        }
        return super.getFieldName(accessor);
    }
}
