package com.atlassian.graphql.json.jersey;

import java.lang.reflect.Method;
import java.util.Optional;

/**
 * Handles conversion using the {@code static} {@code valueOf} method of an object.
 */
public final class ValueOfMethodArgumentConverter implements ArgumentConverter {
    @Override
    public boolean test(Class<?> clazz, Object value) {
        return getValueOfMethod(clazz, value).isPresent();
    }

    @Override
    public Object apply(Class<?> clazz, Object value) {
        return getValueOfMethod(clazz, value)
                .map(m -> invokeValueOfMethod(value, m))
                .orElseThrow(() -> new ArgumentConversionException(
                        "Could not find `valueOf` method on object of type: " + value.getClass(), 500));
    }

    private Object invokeValueOfMethod(Object value, Method m) {
        try {
            return m.invoke(null, value);
        } catch (final ReflectiveOperationException ex) {
            throw new ArgumentConversionException(
                    "Exception invoking `valueOf` method on object of type " + value.getClass(), ex, 500);
        }
    }

    private static Optional<Method> getValueOfMethod(Class<?> clazz, Object value) {
        try {
            return Optional.of(clazz.getMethod("valueOf", value.getClass()));
        } catch (NoSuchMethodException e) {
            return Optional.empty();
        }
    }
}
