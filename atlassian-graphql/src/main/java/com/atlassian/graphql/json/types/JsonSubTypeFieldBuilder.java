package com.atlassian.graphql.json.types;

import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.atlassian.graphql.utils.AnnotationsHelper;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import graphql.Scalars;
import graphql.schema.GraphQLFieldDefinition;

import java.lang.reflect.AnnotatedElement;
import java.util.Map;

import static java.util.Objects.requireNonNull;

/**
 * A helper class for building the fields of {@link JsonSubTypes} annotated fields and field types.
 */
class JsonSubTypeFieldBuilder {
    private final JsonObjectTypeBuilder objectTypeBuilder;

    public JsonSubTypeFieldBuilder(final JsonObjectTypeBuilder objectTypeBuilder) {
        this.objectTypeBuilder = requireNonNull(objectTypeBuilder);
    }

    public void buildDiscriminatorField(
            final Map<String, GraphQLFieldDefinition> fields,
            final Class clazz) {

        if (clazz == null || clazz == Object.class) {
            return;
        }

        final JsonSubTypes jsonSubTypes = AnnotationsHelper.getAnnotation(clazz, JsonSubTypes.class);
        final JsonTypeInfo jsonTypeInfo = AnnotationsHelper.getAnnotation(clazz, JsonTypeInfo.class);
        if (jsonTypeInfo != null) {
            buildDiscriminatorField(fields, jsonSubTypes, jsonTypeInfo);
        }
        buildDiscriminatorField(fields, clazz.getSuperclass());
    }

    private void buildDiscriminatorField(
            final Map<String, GraphQLFieldDefinition> fields,
            final JsonSubTypes jsonSubTypes,
            final JsonTypeInfo jsonTypeInfo) {

        if (jsonTypeInfo.use() != JsonTypeInfo.Id.NAME) {
            throw new IllegalArgumentException("Unsupported JsonTypeInfo.Id value '" + jsonTypeInfo.use() + "'");
        }
        if (jsonTypeInfo.include() != JsonTypeInfo.As.PROPERTY) {
            throw new IllegalArgumentException("Unsupported JsonTypeInfo.As value '" + jsonTypeInfo.use() + "'");
        }

        final String fieldName = jsonTypeInfo.property();
        if (!fields.containsKey(fieldName)) {
            fields.put(fieldName, buildDiscriminatorField(fieldName, jsonSubTypes));
        }
    }

    private GraphQLFieldDefinition buildDiscriminatorField(
            final String fieldName,
            final JsonSubTypes jsonSubTypes) {

        return GraphQLFieldDefinition.newFieldDefinition()
                .name(fieldName)
                .type(Scalars.GraphQLString)
                .dataFetcher(env -> {
                    final Object obj = env.getSource();
                    for (final JsonSubTypes.Type type : jsonSubTypes.value()) {
                        if (type.value().isInstance(obj)) {
                            return type.name();
                        }
                    }
                    if (obj instanceof Map) {
                        return ((Map) obj).get(fieldName);
                    }
                    return null;
                })
                .build();
    }

    public void buildSubtypeFields(
            final Map<String, GraphQLFieldDefinition> fields,
            final AnnotatedElement element,
            final Class clazz,
            final GraphQLTypeBuilderContext context) {

        // find fields on types specified by the parent field @JsonSubTypes annotation
        if (element != null) {
            final JsonSubTypes jsonSubTypes = AnnotationsHelper.getAnnotation(element, JsonSubTypes.class);
            if (jsonSubTypes != null) {
                buildFields(fields, element, context, jsonSubTypes);
            }
        }

        // find fields on types specified by the type @JsonSubTypes annotation
        final JsonSubTypes jsonSubTypes = AnnotationsHelper.getAnnotation(clazz, JsonSubTypes.class);
        if (jsonSubTypes != null) {
            buildFields(fields, element, context, jsonSubTypes);
        }
    }

    private void buildFields(
            final Map<String, GraphQLFieldDefinition> fields,
            final AnnotatedElement element,
            final GraphQLTypeBuilderContext context,
            final JsonSubTypes jsonSubTypes) {

        // add the fields of the subtypes
        for (final JsonSubTypes.Type type : jsonSubTypes.value()) {
            objectTypeBuilder.buildFields(fields, element, type.value(), context, false, false);
        }
    }
}
