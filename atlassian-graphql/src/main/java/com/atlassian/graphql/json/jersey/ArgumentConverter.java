package com.atlassian.graphql.json.jersey;

import java.util.function.BiFunction;
import java.util.function.BiPredicate;

/**
 * Interface to implement for argument conversion, the {@link BiPredicate#test(Object, Object)} method is used to test
 * whether a type and value can be handled by the converter and the {@link BiFunction#apply(Object, Object)} is used to
 * transform the value into the expected type. This method can only be called if the predicate method returns true,
 * otherwise it shall throw an {@link ArgumentConversionException}
 */
public interface ArgumentConverter extends BiPredicate<Class<?>, Object>, BiFunction<Class<?>, Object, Object> {

    @Override
    default Object apply(Class<?> aClass, Object value) {
        return value;
    }
}
