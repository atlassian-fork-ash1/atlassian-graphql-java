package com.atlassian.graphql.json.types;

import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.atlassian.graphql.utils.AnnotationsHelper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import graphql.schema.GraphQLType;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Type;

import static com.atlassian.graphql.utils.ReflectionUtils.getClazz;
import static java.util.Objects.requireNonNull;

/**
 * A GraphQLType builder for jackson @JsonSerialize annotated java classes and fields.
 */
public class JsonSerializeAnnotatedTypeBuilder implements GraphQLTypeBuilder {
    private final GraphQLTypeBuilder typeBuilder;

    public JsonSerializeAnnotatedTypeBuilder(final GraphQLTypeBuilder typeBuilder) {
        this.typeBuilder = requireNonNull(typeBuilder);
    }

    @Override
    public boolean canBuildType(final Type type, final AnnotatedElement element) {
        requireNonNull(type);
        final JsonSerialize serialize = getAnnotation(type, element);
        if (serialize == null) {
            return false;
        }
        if (!hasValue(serialize.as()) || serialize.as() == getClazz(type)) {
            return false;
        }
        if (hasValue(serialize.using())
            || hasValue(serialize.contentUsing())
            || hasValue(serialize.keyUsing())
            || hasValue(serialize.nullsUsing())
            || hasValue(serialize.keyAs())
            || hasValue(serialize.contentAs())
            || serialize.typing() != JsonSerialize.Typing.DEFAULT_TYPING
            || hasValue(serialize.converter())
            || hasValue(serialize.contentConverter())) {

            throw new IllegalArgumentException("Only JsonSerialize.as() is supported for graph-ql");
        }
        return true;
    }

    private static boolean hasValue(final Class clazz) {
        return clazz != null
                && clazz != Void.class
                && !clazz.getSimpleName().equals("None")
                && !clazz.getSimpleName().equals("NoClass");
    }

    @Override
    public GraphQLType buildType(final String typeName, final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
        final JsonSerialize jsonSerialize = getAnnotation(type, element);
        return typeBuilder.buildType(typeName, jsonSerialize.as(), element, context);
    }

    private JsonSerialize getAnnotation(final Type type, final AnnotatedElement element) {
        JsonSerialize jsonSerialize =
                element != null
                ? AnnotationsHelper.getAnnotation(element, JsonSerialize.class)
                : null;
        if (jsonSerialize == null) {
            jsonSerialize = AnnotationsHelper.getAnnotation(getClazz(type), JsonSerialize.class);
        }
        return jsonSerialize;
    }
}
