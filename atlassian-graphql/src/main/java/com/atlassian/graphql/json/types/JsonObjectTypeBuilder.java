package com.atlassian.graphql.json.types;

import com.atlassian.graphql.annotations.GraphQLInterface;
import com.atlassian.graphql.spi.GraphQLExtensions;
import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.atlassian.graphql.types.ObjectTypeBuilder;
import com.atlassian.graphql.utils.AnnotationsHelper;
import com.atlassian.graphql.utils.ObjectTypeBuilderHelper;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Strings;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLOutputType;
import graphql.schema.TypeResolver;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeSet;

import static com.atlassian.graphql.utils.GraphQLUtils.unwrap;
import static com.atlassian.graphql.utils.ReflectionUtils.createParameterizedType;
import static com.atlassian.graphql.utils.ReflectionUtils.getClazz;
import static com.atlassian.graphql.utils.ReflectionUtils.unwrapWildcardType;
import static java.util.Objects.requireNonNull;

/**
 * A builder for GraphQLObjectType objects that are generated from java types annotated with
 * Json (jackson) serialization annotations.
 */
public class JsonObjectTypeBuilder extends ObjectTypeBuilder {
    private final JsonSubTypeFieldBuilder subTypeFieldBuilder;

    public JsonObjectTypeBuilder(
            final GraphQLTypeBuilder typeBuilder,
            final GraphQLExtensions extensions) {

        super(typeBuilder, extensions);
        this.subTypeFieldBuilder = new JsonSubTypeFieldBuilder(this);
    }

    @Override
    public boolean canBuildType(final Type type, final AnnotatedElement element) {
        return true;
    }

    @Override
    protected void buildFields(
            final Map<String, GraphQLFieldDefinition> fields,
            final AnnotatedElement element,
            final Type type,
            final GraphQLTypeBuilderContext context) {

        final Class clazz = getClazz(requireNonNull(type));
        checkNoUnsupportedAnnotations(clazz);
        for (final Method method : clazz.getMethods()) {
            checkNoUnsupportedAnnotations(method);
        }

        final boolean isInterface = AnnotationsHelper.hasAnnotation(getClazz(type), GraphQLInterface.class);
        buildFields(fields, element, type, context, true, !isInterface);
    }

    void buildFields(
            final Map<String, GraphQLFieldDefinition> fields,
            final AnnotatedElement element,
            final Type type,
            final GraphQLTypeBuilderContext context,
            final boolean includeDiscriminator,
            final boolean includeSubtypes) {

        requireNonNull(fields);
        requireNonNull(type);
        requireNonNull(context);

        final Class clazz = getClazz(type);

        // build the subtype discriminator field, if JsonTypeInfo is specified
        if (includeDiscriminator) {
            subTypeFieldBuilder.buildDiscriminatorField(fields, clazz);
        }

        // build fields on the type
        buildFields(fields, type, clazz, context);

        // build fields on any sub-types, if JsonSubTypes is specified
        if (includeSubtypes) {
            subTypeFieldBuilder.buildSubtypeFields(fields, element, clazz, context);
        }

        orderPropertiesIfRequired(type, fields);
    }

    @Override
    protected GraphQLFieldDefinition buildField(
            final Type type,
            final Member accessor,
            final Map<String, GraphQLFieldDefinition> fields,
            final GraphQLTypeBuilderContext context) {

        if (accessor instanceof Field) {
            final Field field = (Field) accessor;
            checkNoUnsupportedAnnotations(field);
        }
        if (accessor instanceof Method && ((Method) accessor).getReturnType() == void.class) {
            return null;
        }

        final String jsonFieldName = getFieldName(accessor);
        if (jsonFieldName != null && !fields.containsKey(jsonFieldName)) {
            final Type fieldType = getJsonFieldType(type, accessor);
            return buildField(jsonFieldName, fieldType, accessor, context);
        }
        return super.buildField(type, accessor, fields, context);
    }

    /**
     * Get the GraphQL name of an accessor (field or method), or null if the member isn't an accessor.
     */
    @Override
    protected String getFieldName(final Member accessor) {
        final JsonProperty jsonProperty = AnnotationsHelper.getAnnotation((AnnotatedElement) accessor, JsonProperty.class);
        if (jsonProperty == null) {
            return super.getFieldName(accessor);
        }
        return !Strings.isNullOrEmpty(jsonProperty.value())
               ? jsonProperty.value()
               : ObjectTypeBuilderHelper.getDefaultFieldName(accessor);
    }

    private static Type getJsonFieldType(final Type declaringType, final Member accessor) {
        Type type = ObjectTypeBuilderHelper.getFieldType(declaringType, accessor);
        type = unwrapWildcardType(type);

        final JsonDeserialize jsonDeserialize = AnnotationsHelper.getAnnotation((AnnotatedElement) accessor, JsonDeserialize.class);
        if (jsonDeserialize != null
                && !jsonDeserialize.as().getSimpleName().equals("NoClass")
                && !jsonDeserialize.as().equals(Void.class)) {

            // match the type parameters of the return type
            final Class clazz = jsonDeserialize.as();
            if (getClazz(type).getTypeParameters().length == clazz.getTypeParameters().length) {
                return createParameterizedType(clazz, (ParameterizedType) type);
            }
            return clazz;
        }
        return type;
    }

    @Override
    protected TypeResolver buildDefaultTypeResolver(
            final Type type,
            final Map<Type, GraphQLOutputType> possibleTypes,
            final GraphQLTypeBuilderContext context) {

        final Class clazz = getClazz(type);
        final JsonTypeInfo jsonTypeInfo = AnnotationsHelper.getAnnotation(clazz, JsonTypeInfo.class);
        final JsonSubTypes jsonSubtypes = AnnotationsHelper.getAnnotation(clazz, JsonSubTypes.class);

        final TypeResolver defaultTypeResolver = super.buildDefaultTypeResolver(type, possibleTypes, context);
        if (jsonTypeInfo != null && jsonSubtypes != null) {
            return buildTypeResolverByJsonSubtypeDiscriminator(possibleTypes, jsonTypeInfo, jsonSubtypes, defaultTypeResolver, context);
        }
        return defaultTypeResolver;
    }

    /**
     * If the object passed to TypeResolver ends up being a Map instead of an object, then
     * type resolution will have to happen using the discriminator field values specified by the
     * {@link JsonSubTypes} type annotation.
     */
    private TypeResolver buildTypeResolverByJsonSubtypeDiscriminator(
            final Map<Type, GraphQLOutputType> possibleTypes,
            final JsonTypeInfo jsonTypeInfo,
            final JsonSubTypes jsonSubtypes,
            final TypeResolver defaultTypeResolver,
            final GraphQLTypeBuilderContext context) {

        final String discriminatorField = jsonTypeInfo.property();
        final Map<Object, GraphQLOutputType> possibleTypesByDiscriminator = new HashMap<>();
        for (final JsonSubTypes.Type subtype : jsonSubtypes.value()) {
            if (possibleTypes.containsKey(subtype.value())) {
                possibleTypesByDiscriminator.put(subtype.name(), possibleTypes.get(subtype.value()));
            }
        }

        return env -> {
            GraphQLOutputType result = defaultTypeResolver.getType(env);
            if (result == null && env.getObject() instanceof Map) {
                final Object discriminator = ((Map) env.getObject()).get(discriminatorField);
                result = possibleTypesByDiscriminator.get(discriminator);
            }
            return result != null ? (GraphQLObjectType) unwrap(result, context.getTypes()) : null;
        };
    }

    private static void orderPropertiesIfRequired(
            final Type type,
            final Map<String, GraphQLFieldDefinition> fields) {

        final JsonPropertyOrder propertyOrder = AnnotationsHelper.getAnnotation(getClazz(type), JsonPropertyOrder.class);
        if (propertyOrder == null) {
            return;
        }

        final LinkedHashMap<String, GraphQLFieldDefinition> newFields = new LinkedHashMap<>();
        if (propertyOrder.alphabetic()) {
            // order alphabetically
            for (final String property : new TreeSet<>(fields.keySet())) {
                newFields.put(property, fields.get(property));
            }
        } else {
            // add ordered fields first
            for (final String property : propertyOrder.value()) {
                final GraphQLFieldDefinition field = fields.get(property);
                if (field != null) {
                    newFields.put(property, fields.get(property));
                }
            }

            // add the other fields
            for (final Map.Entry<String, GraphQLFieldDefinition> entry : newFields.entrySet()) {
                if (!newFields.containsKey(entry.getKey())) {
                    newFields.put(entry.getKey(), entry.getValue());
                }
            }
        }

        // replace the current fields
        fields.clear();
        fields.putAll(newFields);
    }

    private static void checkNoUnsupportedAnnotations(final Class clazz) {
        checkUnsupportedAnnotationNotDefined(clazz, JsonAutoDetect.class);
        checkUnsupportedAnnotationNotDefined(clazz, JsonFilter.class);
        checkUnsupportedAnnotationNotDefined(clazz, JsonIgnoreType.class);
    }

    private static void checkNoUnsupportedAnnotations(final Field field) {
        checkUnsupportedAnnotationNotDefined(field, JsonFormat.class);
        checkUnsupportedAnnotationNotDefined(field, JsonUnwrapped.class);
        checkUnsupportedAnnotationNotDefined(field, JsonPropertyOrder.class);
    }

    private static void checkNoUnsupportedAnnotations(final Method method) {
        for (final Annotation annotation : method.getAnnotations()) {
            final Class annotationType = annotation.annotationType();
            if (AnnotationsHelper.isJsonAnnotation(annotationType)
                    && !annotationType.getSimpleName().equals(JsonProperty.class.getSimpleName())
                    && !annotationType.getSimpleName().equals(JsonCreator.class.getSimpleName())
                    && !annotationType.getSimpleName().equals(JsonValue.class.getSimpleName())
                    && !annotationType.getSimpleName().equals(JsonIgnore.class.getSimpleName())
                    && !annotationType.getSimpleName().equals(JsonSerialize.class.getSimpleName())
                    && !annotationType.getSimpleName().equals(JsonRawValue.class.getSimpleName())) {

                throw new IllegalArgumentException(
                        "Json method annotation '" + annotationType.getSimpleName()
                        + "' declared on method '" + method.getDeclaringClass().getName()
                        + "." + method.getName() + "' is not supported by graphql");
            }
        }
    }

    private static void checkUnsupportedAnnotationNotDefined(final Class clazz, final Class annotationType) {
        if (AnnotationsHelper.hasAnnotation(clazz, annotationType)) {
            throw new IllegalArgumentException(
                    "Json class annotation '" + annotationType.getSimpleName()
                    + "' declared on class '" + clazz.getName() + "' is not supported by graphql");
        }
    }

    private static void checkUnsupportedAnnotationNotDefined(final Field field, final Class annotationType) {
        if (AnnotationsHelper.hasAnnotation(field, annotationType)) {
            throw new IllegalArgumentException(
                    "Json field annotation '" + annotationType.getSimpleName()
                    + "' declared on field '" + field.getDeclaringClass().getName()
                    + "." + field.getName() + "' is not supported by graphql");
        }
    }
}
