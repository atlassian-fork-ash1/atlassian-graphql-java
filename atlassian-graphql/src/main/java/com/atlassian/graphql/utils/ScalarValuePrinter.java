package com.atlassian.graphql.utils;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Prints scalar values
 */
public interface ScalarValuePrinter {
    String print(String value);

    String print(BigInteger value);

    String print(BigDecimal value);

    String print(boolean value);

    ScalarValuePrinter DEFAULT = new ScalarValuePrinter() {
        @Override
        public String print(String value) {
            return value;
        }

        @Override
        public String print(BigInteger value) {
            return String.valueOf(value);
        }

        @Override
        public String print(BigDecimal value) {
            return String.valueOf(value);
        }

        @Override
        public String print(boolean value) {
            return String.valueOf(value);
        }
    };
}
