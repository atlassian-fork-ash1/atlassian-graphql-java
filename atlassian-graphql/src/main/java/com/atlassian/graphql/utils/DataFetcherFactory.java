package com.atlassian.graphql.utils;

import com.atlassian.graphql.spi.GraphQLExtensions;
import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import graphql.schema.DataFetcher;
import graphql.schema.GraphQLOutputType;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Member;
import java.lang.reflect.Type;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;
import java.util.function.Supplier;

public class DataFetcherFactory {
    /**
     * Create a combined DataFetcher and value transformer, using behaviour from the type builder and extensions
     * appropriately. This method returns a DataFetcher that performs the following actions:
     * - Runs the function returned from GraphQLExtensions.getBeforeDataFetch()
     * - Runs the DataFetcher returned from GraphQLExtensions.getDataFetcher() or defaultDataFetcher.get()
     * - Runs the transformer returned from GraphQLTypeBuilder.getValueTransformer()
     */
    public static DataFetcher createDataFetcherAndValueTransformer(
            final Supplier<DataFetcher> defaultDataFetcher,
            final String fieldName,
            final Type fieldType,
            final Member accessor,
            final Object source,
            final GraphQLOutputType graphqlFieldType,
            final GraphQLTypeBuilder typeBuilder,
            final GraphQLTypeBuilderContext context,
            final GraphQLExtensions extensions) {

        final Function<Object, Object> valueTransformer =
                typeBuilder.getValueTransformer(fieldType, (AnnotatedElement) accessor);
        final DataFetcher dataFetcher = createExtensionsOrDefaultDataFetcher(
                defaultDataFetcher,
                fieldName,
                accessor,
                source,
                graphqlFieldType,
                typeBuilder,
                context,
                extensions);
        final DataFetcher finalDataFetcher =
                valueTransformer != null
                ? env -> futureValueTransform(valueTransformer).apply(dataFetcher.get(env))
                : dataFetcher;
        return applyBeforeDataFetch(finalDataFetcher, extensions, fieldType, accessor);
    }

    // Apply transform to future value of CompletableFuture rather than the CompletableFuture itself
    private static Function<Object,Object> futureValueTransform(Function<Object, Object> transform) {
        return obj -> obj instanceof CompletionStage
                ? ((CompletionStage) obj).thenApply(transform)
                : transform.apply(obj);
    }

    private static DataFetcher createExtensionsOrDefaultDataFetcher(
            final Supplier<DataFetcher> defaultDataFetcher,
            final String fieldName,
            final Member accessor,
            final Object source,
            final GraphQLOutputType graphqlFieldType,
            final GraphQLTypeBuilder typeBuilder,
            final GraphQLTypeBuilderContext context,
            final GraphQLExtensions extensions) {

        if (extensions != null) {
            final DataFetcher dataFetcher = extensions.getDataFetcher(
                    defaultDataFetcher,
                    fieldName,
                    accessor,
                    source,
                    graphqlFieldType,
                    typeBuilder,
                    context,
                    extensions);
            if (dataFetcher != null) {
                return dataFetcher;
            }
        }
        return defaultDataFetcher.get();
    }

    private static DataFetcher applyBeforeDataFetch(
            final DataFetcher dataFetcher,
            final GraphQLExtensions extensions,
            final Type type,
            final Member accessor) {

        if (extensions == null) {
            return dataFetcher;
        }
        return extensions.getDataFetcherThunk(type, accessor, dataFetcher);
    }
}
