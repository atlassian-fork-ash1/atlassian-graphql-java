package com.atlassian.graphql.utils;

import static java.util.Objects.requireNonNull;

/**
 * Options to customize query printing
 */
public class GraphQLQueryPrinterOptions {
    private final int indent;
    private final ScalarValuePrinter scalarValuePrinter;

    private GraphQLQueryPrinterOptions(int indent, ScalarValuePrinter scalarValuePrinter) {
        this.indent = indent;
        this.scalarValuePrinter = requireNonNull(scalarValuePrinter);
    }

    public static GraphQLQueryPrinterOptionsBuilder builder() {
        return new GraphQLQueryPrinterOptionsBuilder();
    }

    public ScalarValuePrinter getScalarValuePrinter() {
        return scalarValuePrinter;
    }

    public int getIndent() {
        return indent;
    }


    public static class GraphQLQueryPrinterOptionsBuilder {
        private int indent = 4;
        private ScalarValuePrinter scalarValuePrinter = ScalarValuePrinter.DEFAULT;

        private GraphQLQueryPrinterOptionsBuilder() {
        }

        public GraphQLQueryPrinterOptionsBuilder indent(int indent) {
            this.indent = indent;
            return this;
        }

        public GraphQLQueryPrinterOptionsBuilder scalarValuePrinter(ScalarValuePrinter scalarValuePrinter) {
            this.scalarValuePrinter = requireNonNull(scalarValuePrinter);
            return this;
        }

        public GraphQLQueryPrinterOptions build() {
            return new GraphQLQueryPrinterOptions(indent, scalarValuePrinter);
        }
    }

}
