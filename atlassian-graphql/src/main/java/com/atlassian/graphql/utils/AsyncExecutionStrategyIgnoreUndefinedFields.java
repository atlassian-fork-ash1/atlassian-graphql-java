package com.atlassian.graphql.utils;

import com.atlassian.graphql.instrumentation.SuppressValidationInstrumentation;
import graphql.ExecutionResultImpl;
import graphql.GraphQLException;
import graphql.execution.ExecutionContext;
import graphql.execution.ExecutionStrategy;
import graphql.execution.ExecutionStrategyParameters;
import graphql.execution.FieldValueInfo;
import graphql.language.Field;
import graphql.validation.ValidationError;
import graphql.validation.ValidationErrorType;

import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * An {@link ExecutionStrategy} that adds a @{link ValidationErrorType.FieldUndefined} warning if a field
 * can't be found. This should be used in conjunction with {@link SuppressValidationInstrumentation} to
 * transform undefined query field errors into warnings.
 */
public class AsyncExecutionStrategyIgnoreUndefinedFields extends AsyncExecutionStrategyWithExecutionListenerSupport {
    @Override
    protected CompletableFuture<FieldValueInfo> resolveFieldWithInfo(
            final ExecutionContext executionContext,
            final ExecutionStrategyParameters parameters) {

        try {
            return super.resolveFieldWithInfo(executionContext, parameters);
        } catch (final GraphQLException ex) {
            final Field field = parameters.getField().getSingleField();
            if (isFieldUndefined(executionContext, parameters, field)) {
                final String message = String.format("Field '%s' is undefined", field.getName());
                final ValidationError error = new ValidationError(
                        ValidationErrorType.FieldUndefined, field.getSourceLocation(), message) {
                    @Override
                    public List<Object> getPath() {
                        return parameters.getPath().toList();
                    }
                };
                executionContext.addError(error, parameters.getPath());
                return CompletableFuture.completedFuture(
                        FieldValueInfo.newFieldValueInfo(FieldValueInfo.CompleteValueType.NULL)
                        .fieldValue(CompletableFuture.completedFuture(new ExecutionResultImpl(error)))
                        .build());
            }
            throw ex;
        }
    }

    private boolean isFieldUndefined(
            final ExecutionContext executionContext,
            final ExecutionStrategyParameters parameters,
            final Field field) {

        try {
            getFieldDef(executionContext, parameters, field);
            return false;
        } catch (final GraphQLException ex) {
            return true;
        }
    }
}
