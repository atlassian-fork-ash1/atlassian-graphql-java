package com.atlassian.graphql.utils;

import com.atlassian.graphql.spi.ExecutionListener;
import com.google.common.base.Supplier;
import graphql.execution.AsyncExecutionStrategy;
import graphql.execution.ExecutionContext;
import graphql.execution.ExecutionStrategyParameters;
import graphql.execution.FieldValueInfo;

import java.util.concurrent.CompletableFuture;

/**
 * A {@link AsyncExecutionStrategy} that calls {@link ExecutionListener} methods when implemented
 * on the root object.
 */
public class AsyncExecutionStrategyWithExecutionListenerSupport extends AsyncExecutionStrategy {
    @Override
    protected CompletableFuture<FieldValueInfo> resolveFieldWithInfo(
            final ExecutionContext executionContext,
            final ExecutionStrategyParameters parameters) {

        return resolveField(executionContext, parameters, () -> super.resolveFieldWithInfo(executionContext, parameters));
    }

    public static CompletableFuture<FieldValueInfo> resolveField(
            final ExecutionContext executionContext,
            final ExecutionStrategyParameters parameters,
            final Supplier<CompletableFuture<FieldValueInfo>> callSuper) {

        if (executionContext.getContext() instanceof ExecutionListener) {
            ((ExecutionListener) executionContext.getContext()).beforeResolveField(executionContext, parameters);
        }
        final CompletableFuture<FieldValueInfo> result = callSuper.get();
        if (executionContext.getContext() instanceof ExecutionListener) {
            ((ExecutionListener) executionContext.getContext()).afterResolveField(executionContext, parameters);
        }
        return result;
    }
}
