package com.atlassian.graphql.utils;

import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.ExecutionResultImpl;
import graphql.GraphQL;
import graphql.execution.AsyncExecutionStrategy;
import graphql.execution.AsyncSerialExecutionStrategy;
import graphql.execution.Execution;
import graphql.execution.ExecutionId;
import graphql.execution.ExecutionStrategy;
import graphql.execution.instrumentation.Instrumentation;
import graphql.execution.instrumentation.InstrumentationContext;
import graphql.execution.instrumentation.InstrumentationState;
import graphql.execution.instrumentation.SimpleInstrumentation;
import graphql.execution.instrumentation.parameters.InstrumentationCreateStateParameters;
import graphql.execution.instrumentation.parameters.InstrumentationExecutionParameters;
import graphql.execution.instrumentation.parameters.InstrumentationValidationParameters;
import graphql.language.Document;
import graphql.language.Field;
import graphql.language.FragmentDefinition;
import graphql.language.FragmentSpread;
import graphql.language.InlineFragment;
import graphql.language.Node;
import graphql.language.SelectionSet;
import graphql.schema.GraphQLModifiedType;
import graphql.schema.GraphQLSchema;
import graphql.schema.GraphQLType;
import graphql.schema.GraphQLTypeReference;
import graphql.validation.ValidationError;
import graphql.validation.Validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.CompletableFuture;

import static java.util.Objects.requireNonNull;

public class GraphQLUtils {
    private static final WeakHashMap<Document, Object> validQueriesCache = new WeakHashMap<>();

    /**
     * Unwrap a {@link GraphQLModifiedType} or {@link GraphQLTypeReference} type.
     */
    public static GraphQLType unwrap(final GraphQLType type, final Map<String, GraphQLType> allTypes) {
        requireNonNull(type);
        requireNonNull(allTypes);

        if (type instanceof GraphQLModifiedType) {
            return unwrap(((GraphQLModifiedType) type).getWrappedType(), allTypes);
        } else if (type instanceof GraphQLTypeReference) {
            GraphQLType resolvedType = resolveType(type, allTypes);
            return unwrap(resolvedType, allTypes);
        }
        return type;
    }

    private static GraphQLType resolveType(final GraphQLType type, final Map<String, GraphQLType> allTypes) {
        final GraphQLType resolvedType = allTypes != null ? allTypes.get(type.getName()) : null;
        if (resolvedType == null) {
            throw new IllegalArgumentException("Failed to resolve type '" + type.getName() + "'");
        }
        return resolvedType;
    }

    /**
     * Get the child fields of a graphql {@link Field} query object.
     */
    public static List<Field> getSubFields(final Map<String, FragmentDefinition> fragmentsByName, final Field field) {
        requireNonNull(fragmentsByName);
        requireNonNull(field);
        return getFields(fragmentsByName, field.getChildren());
    }

    private static List<Field> getFields(final Map<String, FragmentDefinition> fragmentsByName, final List<Node> nodes) {
        final List<Field> list = new ArrayList<>();
        for (final Node child : nodes) {
            if (child instanceof Field) {
                list.add((Field) child);
            } else if (child instanceof SelectionSet) {
                list.addAll(getFields(fragmentsByName, child.getChildren()));
            } else if (child instanceof FragmentSpread) {
                final FragmentDefinition fragment = fragmentsByName.get(((FragmentSpread) child).getName());
                list.addAll(getFields(fragmentsByName, fragment.getSelectionSet().getChildren()));
            } else if (child instanceof InlineFragment) {
                list.addAll(getFields(fragmentsByName, ((InlineFragment) child).getSelectionSet().getChildren()));
            }
        }
        return list;
    }

    /**
     * Executes a GraphQL query in the same way that {@link GraphQL}, accepting a pre-built query {@link Document} object.
     */
    public static CompletableFuture<ExecutionResult> executeDocument(
            final GraphQLSchema schema,
            final Document query,
            final String operationName,
            final Object context,
            final Map<String, Object> variables) {

        return executeDocument(
                schema,
                query,
                new ExecutionInput(null, operationName, context, null, variables),
                new AsyncExecutionStrategy(),
                new AsyncSerialExecutionStrategy(),
                SimpleInstrumentation.INSTANCE);
    }

    /**
     * Executes a GraphQL query in the same way that {@link GraphQL}, accepting a pre-built query {@link Document} object.
     */
    @SuppressWarnings("unchecked")
    public static CompletableFuture<ExecutionResult> executeDocument(
            final GraphQLSchema schema,
            final Document query,
            final ExecutionInput executionInput,
            final ExecutionStrategy queryStrategy,
            final ExecutionStrategy mutationStrategy,
            final Instrumentation instrumentation) {

        final InstrumentationState instrumentationState = instrumentation.createState(new InstrumentationCreateStateParameters(schema, executionInput));
        final InstrumentationExecutionParameters instrumentationParameters = new InstrumentationExecutionParameters(executionInput, schema, instrumentationState);
        final InstrumentationContext<ExecutionResult> executionInstrumentation = instrumentation.beginExecution(instrumentationParameters);

        boolean isValidCached;
        synchronized (validQueriesCache) {
            isValidCached = validQueriesCache.containsKey(query);
        }
        if (!isValidCached) {
            final GraphQLSchema instrumentedSchema = instrumentation.instrumentSchema(schema, instrumentationParameters);
            final List<ValidationError> validationErrors = validate(instrumentedSchema, query, executionInput, instrumentation, instrumentationState);
            if (validationErrors.size() > 0) {
                return CompletableFuture.completedFuture(new ExecutionResultImpl(validationErrors));
            }
            synchronized (validQueriesCache) {
                validQueriesCache.put(query, null);
            }
        }

        final ExecutionId executionId = ExecutionId.generate();
        final Execution execution = new Execution(
                queryStrategy != null ? queryStrategy : new AsyncExecutionStrategy(),
                mutationStrategy != null ? mutationStrategy : new AsyncSerialExecutionStrategy(),
                new AsyncExecutionStrategy(),
                instrumentation);

        return execution.execute(
                query,
                schema,
                executionId,
                executionInput,
                instrumentationState)
                .whenComplete(executionInstrumentation::onCompleted)
                .thenCompose(result -> instrumentation.instrumentExecutionResult(result, instrumentationParameters));
    }

    private static List<ValidationError> validate(
            final GraphQLSchema schema,
            final Document query,
            final ExecutionInput executionInput,
            final Instrumentation instrumentation,
            final InstrumentationState instrumentationState) {

        final InstrumentationValidationParameters validationParams = new InstrumentationValidationParameters(executionInput, query, schema, instrumentationState);
        final InstrumentationContext<List<ValidationError>> validationCtx = instrumentation.beginValidation(validationParams);
        final List<ValidationError> validationErrors = new Validator().validateDocument(schema, query);
        validationCtx.onCompleted(validationErrors, null);
        return validationErrors;
    }
}
