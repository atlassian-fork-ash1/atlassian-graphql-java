package com.atlassian.graphql.utils;

import com.atlassian.graphql.annotations.GraphQLDefaultValue;
import com.atlassian.graphql.annotations.GraphQLTypeName;
import com.atlassian.graphql.types.DynamicType;
import com.google.common.base.Throwables;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

/**
 * Reflection helper methods that are used while binding objects to graphql.
 */
public class ReflectionUtils {
    private static final WeakHashMap<Type, String> graphqlObjectTypeNameCache = new WeakHashMap<>();
    private static final WeakHashMap<Field, Type> fieldGenericTypeCache = new WeakHashMap<>();
    private static final WeakHashMap<Method, Type> methodGenericReturnTypeCache = new WeakHashMap<>();
    private static final WeakHashMap<ClassAndMethod, Object> methodOnClassCache = new WeakHashMap<>();

    /**
     * Get the value from a @DefaultValue or @GraphQLDefaultValue annotation.
     * A @DefaultValue annotation may come from any java package, including javax.ws.rs.DefaultValue.
     */
    public static Object defaultValueFromAnnotation(final Parameter parameter, final Annotation[] annotations) {
        requireNonNull(parameter);

        for (final Annotation annotation : annotations) {
            if (annotation.annotationType().getName().equals(GraphQLDefaultValue.class.getName()) ||
                    annotation.annotationType().getSimpleName().equals("DefaultValue")) {

                try {
                    final Method valueMethod = annotation.getClass().getMethod("value");
                    return valueMethod.invoke(annotation);
                } catch (ReflectiveOperationException ex) {
                    throw Throwables.propagate(ex);
                }
            }
        }
        return null;
    }

    /**
     * Get the name of a graphql type from a @GraphQLTypeName annotation.
     */
    public static String getAnnotatedGraphQLTypeName(final Type type, final AnnotatedElement element) {
        requireNonNull(type);
        if (element != null) {
            final GraphQLTypeName typeNameAnnotation =
                    AnnotationsHelper.getAnnotationIncludingInherited(null, element, GraphQLTypeName.class);
            if (typeNameAnnotation != null) {
                return getTypeNameFromAnnotation(type, typeNameAnnotation);
            }
        }
        return null;
    }

    /**
     * Get the name to use for a graphql type from its java type.
     */
    public static String getGraphqlObjectTypeName(final Type type, final AnnotatedElement element) {
        requireNonNull(type);
        final String annotatedTypeName = getAnnotatedGraphQLTypeName(type, element);
        if (annotatedTypeName != null) {
            return annotatedTypeName;
        }
        return graphqlObjectTypeNameCache.computeIfAbsent(type, ReflectionUtils::getGraphqlObjectTypeNameUncached);
    }

    private static String getGraphqlObjectTypeNameUncached(Type type) {
        type = unwrapWildcardType(type);

        final Class clazz = getClazz(type);
        final GraphQLTypeName typeNameAnnotation = AnnotationsHelper.getAnnotation(clazz, GraphQLTypeName.class);
        if (typeNameAnnotation != null) {
            return getTypeNameFromAnnotation(type, typeNameAnnotation);
        }

        if (type instanceof ParameterizedType) {
            final ParameterizedType parameterizedType = (ParameterizedType) type;

            // get the simple name
            String name = ((Class) parameterizedType.getRawType()).getSimpleName();

            // add type parameters (for example, List<JsonObject> becomes List_JsonObject
            for (final Type param : parameterizedType.getActualTypeArguments()) {
                name += "_" + getGraphqlObjectTypeName(param, null);
            }
            return name;
        }
        if (type instanceof Class) {
            return ((Class) type).getSimpleName();
        }
        return type.getTypeName();
    }

    private static String getTypeNameFromAnnotation(final Type type, GraphQLTypeName typeNameAnnotation) {
        String result = typeNameAnnotation.value();
        if (!(type instanceof ParameterizedType)) {
            return result;
        }

        final ParameterizedType parameterizedType = (ParameterizedType) type;
        final Type[] typeArguments = parameterizedType.getActualTypeArguments();
        for (int i = 0; i < typeArguments.length; i++) {
            final String token = "{T" + (i + 1) + "}";
            if (typeNameAnnotation.value().contains(token)) {
                result = result.replace(token, getGraphqlObjectTypeName(typeArguments[i], null));
            }
        }
        return result;
    }

    /**
     * Get the {@link Class} object for a {@link Type} object.
     * This means unwrapping ParameterizedType, WildcardType or DynamicType objects.
     */
    public static Class<?> getClazz(final Type type) {
        requireNonNull(type);

        if (type instanceof ParameterizedType) {
            return (Class) ((ParameterizedType) type).getRawType();
        }
        if (type instanceof WildcardType) {
            final Type[] upperBounds = ((WildcardType) type).getUpperBounds();
            return upperBounds.length > 0 ? getClazz(upperBounds[0]) : Object.class;
        }
        if (type instanceof DynamicType) {
            return Map.class;
        }
        return (Class) type;
    }

    /**
     * Get the type of a field, binding type parameters if necessary so that we're left
     * without any unbound {@link TypeVariable} objects.
     */
    public static Type getRealFieldType(
            final Type declaringType,
            final Field field) {

        requireNonNull(field);
        return ReflectionUtils.bindGenericType(declaringType, getGenericTypeCached(field));
    }

    /**
     * Get the return type of a method, binding type parameters if necessary so that we're left
     * without any unbound {@link TypeVariable} objects.
     */
    public static Type getRealMethodReturnType(
            final Type declaringType,
            final Method method) {

        requireNonNull(method);
        return ReflectionUtils.bindGenericType(declaringType, getGenericReturnTypeCached(method));
    }

    private static Type getGenericTypeCached(final Field field) {
        return fieldGenericTypeCache.computeIfAbsent(field, Field::getGenericType);
    }

    private static Type getGenericReturnTypeCached(final Method method) {
        return methodGenericReturnTypeCache.computeIfAbsent(method, Method::getGenericReturnType);
    }

    /**
     * Bind the type parameters of a generic type if necessary so that we're left
     * without any unbound {@link TypeVariable} objects.
     */
    public static Type bindGenericType(final Type declaringType, final Type genericType) {
        requireNonNull(declaringType);
        requireNonNull(genericType);
        if (genericType instanceof TypeVariable) {
            return bindGenericTypeVariable(declaringType, (TypeVariable) genericType);
        }
        if (genericType instanceof ParameterizedType) {
            return bindGenericParameterizedType(declaringType, (ParameterizedType) genericType);
        }
        return genericType;
    }

    private static Type bindGenericTypeVariable(final Type declaringType, final TypeVariable genericType) {
        if (!(declaringType instanceof ParameterizedType)) {
            return Object.class;
        }

        final Class declaringClass = getClazz(declaringType);
        final TypeVariable[] typeVariables = declaringClass.getTypeParameters();
        for (int i = 0; i < typeVariables.length; i++) {
            if (typeVariables[i] == genericType) {
                return ((ParameterizedType) declaringType).getActualTypeArguments()[i];
            }
        }

        final Type supertype = bindGenericType(declaringType, getClazz(declaringType).getGenericSuperclass());
        if (supertype != Object.class) {
            return bindGenericTypeVariable(supertype, genericType);
        }
        return genericType;
    }

    private static Type bindGenericParameterizedType(final Type declaringType, final ParameterizedType genericType) {
        if (!(declaringType instanceof ParameterizedType)) {
            final Type superclass =
                    declaringType instanceof Class
                    ? ((Class) declaringType).getGenericSuperclass()
                    : null;
            return superclass != null
                   ? bindGenericParameterizedType(superclass, genericType)
                   : genericType;
        }

        final Type[] newTypeArguments =
                Arrays.stream(genericType.getActualTypeArguments())
                .map(type -> bindGenericType(declaringType, type))
                .collect(Collectors.toList())
                .toArray(new Type[0]);

        return !Arrays.equals(newTypeArguments, genericType.getActualTypeArguments())
               ? createParameterizedType(genericType.getRawType(), newTypeArguments)
               : genericType;
    }

    /**
     * Unwrap a 'T extends ?' type parameter, and return the upper bounds of the type.
     */
    public static Type unwrapWildcardType(final Type type) {
        if (type instanceof WildcardType) {
            final Type[] upperBounds = ((WildcardType) type).getUpperBounds();
            return upperBounds.length > 0 ? upperBounds[0] : Object.class;
        }
        return type;
    }

    /**
     * Get the method with the same name and parameter types from a class.
     * This method is optimized for being called in repeated succession.
     *
     * The result of this operation is cached in a WeakHashMap, for a minor performance
     * improvement (~4x) when called multiple times is fast succession.
     *
     * @param clazz The class to get the method from
     * @param method The method
     * @return The method on the class
     */
    public static Method getMethodOnClass(final Class clazz, final Method method) {
        final ClassAndMethod key = new ClassAndMethod(clazz, method);
        Object result = methodOnClassCache.get(key);
        if (result == null) {
            try {
                result = clazz.getMethod(method.getName(), method.getParameterTypes());
            } catch (NoSuchMethodException ex) {
                result = "";
            }
            methodOnClassCache.put(key, result);
        }
        return result instanceof Method ? (Method) result : null;
    }

    /**
     * Create a {@link ParameterizedType} from the raw type and the type arguments of another {@link ParameterizedType}.
     */
    public static ParameterizedType createParameterizedType(final Type rawType, final ParameterizedType typeWithTypeArguments) {
        requireNonNull(typeWithTypeArguments);
        return createParameterizedType(rawType, typeWithTypeArguments.getActualTypeArguments());
    }

    /**
     * Create a {@link ParameterizedType}.
     */
    public static ParameterizedType createParameterizedType(final Type rawType, final Type[] typeArguments) {
        requireNonNull(rawType);
        requireNonNull(typeArguments);

        return new ParameterizedType() {
            @Override
            public String getTypeName() {
                return rawType.getTypeName();
            }

            @Override
            public Type getRawType() {
                return rawType;
            }

            @Override
            public Type getOwnerType() {
                return null;
            }

            @Override
            public Type[] getActualTypeArguments() {
                return typeArguments;
            }

            @Override
            public boolean equals(final Object obj) {
                if (!(obj instanceof ParameterizedType)) {
                    return false;
                }

                final ParameterizedType parameterizedType = (ParameterizedType) obj;
                return getRawType().equals(parameterizedType.getRawType()) &&
                       Arrays.equals(getActualTypeArguments(), parameterizedType.getActualTypeArguments());
            }

            @Override
            public int hashCode() {
                return getRawType().hashCode();
            }
        };
    }

    private static final class ClassAndMethod {
        final Class clazz;
        final Method method;

        public ClassAndMethod(final Class clazz, final Method method) {
            this.clazz = clazz;
            this.method = method;
        }

        @Override
        public boolean equals(final Object obj) {
            if (!(obj instanceof ClassAndMethod)) {
                return false;
            }
            final ClassAndMethod rhs = (ClassAndMethod) obj;
            return clazz.equals(rhs.clazz) && method.equals(rhs.method);
        }

        @Override
        public int hashCode() {
            return method.hashCode();
        }
    }
}
