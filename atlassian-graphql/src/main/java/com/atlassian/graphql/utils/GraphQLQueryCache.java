package com.atlassian.graphql.utils;

import com.atlassian.annotations.tenancy.TenancyScope;
import com.atlassian.annotations.tenancy.TenantAware;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.Weigher;
import graphql.ExecutionInput;
import graphql.execution.preparsed.PreparsedDocumentEntry;
import graphql.execution.preparsed.PreparsedDocumentProvider;
import graphql.language.Document;
import graphql.parser.InvalidSyntaxException;

import java.util.function.Function;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

/**
 * A cache for parsed GraphQL query {@link Document} objects.
 */
public class GraphQLQueryCache implements PreparsedDocumentProvider {
    @TenantAware(TenancyScope.TENANTLESS)
    private Cache<String, Document> cache;

    /**
     * Constructor
     * @param maximumWeight The maximum size of the cache, in terms of the total size of the cached query strings
     */
    public GraphQLQueryCache(final int maximumWeight) {
        cache = CacheBuilder.<String, Document>newBuilder()
                .weigher((Weigher<String, Document>) (key, value) -> key.length())
                .maximumWeight(maximumWeight)
                .build();
    }

    @Override
    public PreparsedDocumentEntry getDocument(ExecutionInput executionInput, Function<ExecutionInput, PreparsedDocumentEntry> function) {
        final Document document = cache.getIfPresent(executionInput.getQuery());
        if (document != null) {
            return new PreparsedDocumentEntry(document);
        }

        final PreparsedDocumentEntry result;
        try {
            result = function.apply(executionInput);
        } catch (final InvalidSyntaxException ex) {
            return new PreparsedDocumentEntry(ex.toInvalidSyntaxError());
        }
        if (result == null || result.hasErrors()) {
            return result;
        }
        put(executionInput.getQuery(), result.getDocument());
        return result;
    }

    protected void put(String query, Document queryDocument) {
        cache.put(query, queryDocument);
    }

}
