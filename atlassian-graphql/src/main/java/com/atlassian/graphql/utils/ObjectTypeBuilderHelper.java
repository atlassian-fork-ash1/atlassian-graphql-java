package com.atlassian.graphql.utils;

import com.atlassian.graphql.annotations.GraphQLExperimental;
import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.spi.GraphQLExtensions;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.google.common.base.Strings;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.TreeSet;

import static com.atlassian.graphql.utils.ReflectionUtils.getGraphqlObjectTypeName;
import static com.atlassian.graphql.utils.ReflectionUtils.unwrapWildcardType;
import static java.util.Objects.requireNonNull;

/**
 * A helper for build graph-ql type builders for object types.
 */
public class ObjectTypeBuilderHelper {
    /**
     * Build the default type name using reflection and extensions.
     */
    public static String buildDefaultTypeName(
            final Type type,
            final AnnotatedElement element,
            final GraphQLTypeBuilderContext context,
            final GraphQLExtensions extensions) {

        requireNonNull(type);

        String typeName = getGraphqlObjectTypeName(type, element);
        final String newTypeName = extensions != null ? extensions.contributeTypeName(typeName, type, context) : null;
        if (newTypeName != null) {
            typeName = newTypeName;
        }
        return typeName;
    }

    /**
     * Get the fields and methods in order using GraphQLName.order() as the sort order,
     * or the field/parameter layout order or method field name as the default sort order.
     */
    public static Member[] getDeclaredFieldsAndMethodsInOrder(final Class clazz) {
        int i = 0;
        final TreeSet<OrderedTypeElement> result = new TreeSet<>();
        for (final Field field : clazz.getDeclaredFields()) {
            result.add(new OrderedTypeElement(field, field.getName(), i++, AnnotationsHelper.getAnnotation(field, GraphQLName.class)));
        }
        for (final Method method : clazz.getDeclaredMethods()) {
            if (!method.isBridge()) {
                result.add(new OrderedTypeElement(method, getMethodFieldName(method.getName()), i++, AnnotationsHelper.getAnnotation(method, GraphQLName.class)));
            }
        }
        return result.stream().map(OrderedTypeElement::getElement).toArray(Member[]::new);
    }

    /**
     * Get the methods in order using GraphQLName.order() as the sort order,
     * or the method field name as the default sort order.
     */
    public static Method[] getMethodsInOrder(final Class clazz) {
        int i = 0;
        final TreeSet<OrderedTypeElement> result = new TreeSet<>();
        for (final Method method : clazz.getMethods()) {
            if (!method.isBridge()) {
                result.add(new OrderedTypeElement(method, getMethodFieldName(method.getName()), i++, AnnotationsHelper.getAnnotation(method, GraphQLName.class)));
            }
        }
        return result.stream().map(OrderedTypeElement::getElement).toArray(Method[]::new);
    }

    /**
     * Determine if a java class, field or method is annotated with @GraphQLExperimental.
     */
    public static boolean isGraphQLExperimental(final AnnotatedElement accessor) {
        return AnnotationsHelper.getAnnotation(accessor, GraphQLExperimental.class) != null;
    }

    /**
     * Get the graph-ql name of a java class, field or method.
     */
    public static String getFieldName(final Member accessor) {
        final GraphQLName graphqlName = AnnotationsHelper.getAnnotation((AnnotatedElement) accessor, GraphQLName.class);
        if (graphqlName == null) {
            return null;
        }
        return !Strings.isNullOrEmpty(graphqlName.value()) ? graphqlName.value() : getDefaultFieldName(accessor);
    }

    /**
     * Get the default field name of a Field or Method accessor.
     */
    public static String getDefaultFieldName(final Member accessor) {
        return accessor instanceof Method
               ? getMethodFieldName(accessor.getName())
               : accessor.getName();
    }

    private static String getMethodFieldName(final String methodName) {
        if (methodName.length() > 3 && methodName.startsWith("get")) {
            return Character.toLowerCase(methodName.charAt(3)) + methodName.substring(4);
        } else if (methodName.length() > 2 && methodName.startsWith("is")) {
            return Character.toLowerCase(methodName.charAt(2)) + methodName.substring(3);
        }
        return methodName;
    }

    /**
     * Get the actual java type of a java class, field or method, after binding
     * generics types.
     */
    public static Type getFieldType(final Type declaringType, final Member accessor) {
        if (accessor instanceof Field) {
            return unwrapWildcardType(ReflectionUtils.getRealFieldType(declaringType, (Field) accessor));
        } else if (accessor instanceof Method) {
            return unwrapWildcardType(ReflectionUtils.getRealMethodReturnType(declaringType, (Method) accessor));
        }
        throw new IllegalArgumentException("member must be a field or method");
    }
}
