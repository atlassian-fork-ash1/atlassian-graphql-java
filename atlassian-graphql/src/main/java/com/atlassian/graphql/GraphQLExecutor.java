package com.atlassian.graphql;

import com.atlassian.graphql.rest.GraphQLRestRequest;
import graphql.ExecutionResult;

import java.io.IOException;

/**
 * An interface for running GraphQL query requests.
 * Possible implementations of this interface are to run the query locally, across the network or via a router.
 */
public interface GraphQLExecutor {
    /**
     * Execute a GraphQL query request.
     * @param request The graph-ql request (query, variables, operationName)
     * @param context Optional context, if the query is being run over a local schema
     * @return The results of the query
     */
    ExecutionResult execute(final GraphQLRestRequest request, final Object context) throws IOException;
}
