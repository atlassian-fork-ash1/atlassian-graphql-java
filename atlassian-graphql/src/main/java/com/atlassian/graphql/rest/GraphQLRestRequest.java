package com.atlassian.graphql.rest;

import com.atlassian.graphql.utils.GraphQLQueryPrinter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import graphql.language.Document;
import graphql.parser.Parser;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static java.util.Objects.requireNonNull;

/**
 * Represents a graph-ql REST request, in application/graphql mime format.
 */
@JsonPropertyOrder({"id", "query", "variables", "operationName"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GraphQLRestRequest {
    @JsonProperty
    private String id;
    private String query;
    @JsonIgnore
    private Document queryDocument;

    @JsonProperty
    private Object variables;
    @JsonProperty
    private String operationName;

    public GraphQLRestRequest() {
    }

    public GraphQLRestRequest(final String query) {
        this(query, null, null);
    }

    public GraphQLRestRequest(final String query, final Object variables, final String operationName) {
        this.query = requireNonNull(query);
        this.variables = variables;
        this.operationName = operationName;
    }

    public GraphQLRestRequest(final Document queryDocument, final Object variables, final String operationName) {
        this.queryDocument = requireNonNull(queryDocument);
        this.variables = variables;
        this.operationName = operationName;
    }

    /**
     * Get the graph-ql query id if specified.
     */
    public String getId() {
        return id;
    }

    /**
     * Get the graph-ql query string.
     */
    @JsonProperty
    public String getQuery() {
        if (query == null && queryDocument != null) {
            query = new GraphQLQueryPrinter().print(queryDocument);
        }
        return query;
    }

    /**
     * Get the graph-ql query string.
     */
    @JsonProperty
    private void setQuery(final String value) {
        query = value;
        queryDocument = null;
    }

    /**
     * Get the graph-ql query as a graphql-java {@link Document} object.
     */
    public Document getQueryDocument() {
        if (queryDocument == null) {
            queryDocument = new Parser().parseDocument(query);
        }
        return queryDocument;
    }

    /**
     * Set the graph-ql query as a graphql-java {@link Document} object,
     * without modifying the original query string if set.
     * This is useful when maintaining a cache of pre-compiled query {@link Document} objects.
     */
    public void setQueryDocument(final Document value) {
        queryDocument = value;
    }

    /**
     * Get the variables to use with the graph-ql query.
     */
    public Map<String, Object> getVariables() {
        if (variables == null) {
            return Collections.emptyMap();
        }
        return (Map<String, Object>) variables;
    }

    /**
     * Get the operation name.
     */
    public String getOperationName() {
        return !Strings.isNullOrEmpty(operationName) ? operationName : null;
    }

    /**
     * Parse a single graph-ql request in json format.
     * @param requestString The request string with a single requests in json format
     * @param objectMapper The json serialization object mapper
     * @return The rest request object
     * @throws IOException If an error happened during deserialization
     * @throws JsonMappingException If a specific mapping error happened
     *                              (say if the request isn't in 'single request' format)
     */
    public static GraphQLRestRequest parseSingle(final String requestString, final ObjectMapper objectMapper)
            throws IOException, JsonMappingException {

        requireNonNull(requestString);
        requireNonNull(objectMapper);

        final GraphQLRestRequest request = objectMapper.readValue(requestString, GraphQLRestRequest.class);
        request.parseVariables(objectMapper);
        return request;
    }

    /**
     * Parse a multiple graph-ql request in json format.
     * @param requestString The request string with multiple requests in json format
     * @param objectMapper The json serialization object mapper
     * @return The rest request objects
     * @throws IOException If an error happened during deserialization
     * @throws JsonMappingException If a specific mapping error happened
     *                              (say if the request isn't in 'multiple request' format)
     */
    public static List<GraphQLRestRequest> parseMultiple(final String requestString, final ObjectMapper objectMapper)
            throws IOException, JsonMappingException {

        requireNonNull(requestString);
        requireNonNull(objectMapper);

        final List<GraphQLRestRequest> requests = objectMapper.readValue(requestString, new TypeReference<List<GraphQLRestRequest>>() {});
        for (final GraphQLRestRequest request : requests) {
            request.parseVariables(objectMapper);
        }
        return requests;
    }

    private void parseVariables(final ObjectMapper objectMapper) throws IOException {
        if (variables instanceof String) {
            if (((String)variables).length() > 0) {
                variables = (Map<String, Object>) objectMapper.readValue((String) variables, Map.class);
            } else {
                variables = Collections.emptyMap();
            }
        }
    }
}
