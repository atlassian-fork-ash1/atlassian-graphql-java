package com.atlassian.plugin.graphql.api;

import com.atlassian.graphql.spi.GraphQLProviders;
import com.atlassian.graphql.annotations.GraphQLExtensions;
import com.atlassian.graphql.annotations.GraphQLProvider;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.atlassian.plugin.module.ContainerAccessor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import com.atlassian.plugins.graphql.api.GraphQLProvidersModuleDescriptor;
import com.google.common.collect.Lists;
import graphql.schema.GraphQLFieldDefinition;
import org.dom4j.dom.DOMElement;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.Bundle;
import org.osgi.framework.wiring.BundleWiring;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.osgi.framework.wiring.BundleWiring.LISTRESOURCES_LOCAL;
import static org.osgi.framework.wiring.BundleWiring.LISTRESOURCES_RECURSE;

@RunWith(MockitoJUnitRunner.class)
public class GraphQLProvidersModuleDescriptorTest {
    @Mock
    private ModuleFactory moduleFactory;
    @Mock
    private OsgiPlugin plugin;
    @Mock
    private Bundle bundle;
    @Mock
    private BundleWiring bundleWiring;
    @Mock
    private ContainerAccessor containerAccessor;

    private GraphQLProvidersModuleDescriptor moduleDescriptor;

    @Before
    public void setUp() {
        moduleDescriptor = new GraphQLProvidersModuleDescriptor(moduleFactory);
        when(plugin.getBundle()).thenReturn(bundle);
        when(bundle.adapt(BundleWiring.class)).thenReturn(bundleWiring);
        when(containerAccessor.createBean(Mockito.any())).thenAnswer(i -> ((Class) i.getArguments()[0]).newInstance());
        when(plugin.getContainerAccessor()).thenReturn(containerAccessor);
    }

    @Test
    public void testGetModuleClass() {
        assertEquals(GraphQLProviders.class, moduleDescriptor.getModuleClass());
    }

    @Test
    public void testGetModule() throws Exception {
        final DOMElement element = buildConfigElement();
        mockClasses(TestProvider.class, TestExtension.class, NonGraphQLType.class);

        moduleDescriptor.init(plugin, element);
        final GraphQLProviders module = moduleDescriptor.getModule();
        assertEquals("Root graph path for providers in the package", "root", module.getPath());
        assertEquals("1 provider class", 1, module.getProviders().size());
        assertEquals("Provider class", TestProvider.class, module.getProviders().get(0).getClass());
        assertEquals("A GraphQLExtensions class", TestExtension.class, module.getExtensions().getClass());
    }

    @Test
    public void testGetModuleSortedProviders() throws Exception {
        final DOMElement element = buildConfigElement();
        mockClasses(TestProvider.class, TestOtherProvider.class);

        moduleDescriptor.init(plugin, element);
        final GraphQLProviders module = moduleDescriptor.getModule();
        assertEquals("2 provider class", 2, module.getProviders().size());
        assertEquals("Provider class", TestOtherProvider.class, module.getProviders().get(0).getClass());
    }

    private void mockClasses(Class... types) throws ClassNotFoundException {
        final String resourceNamePrefix = "com/atlassian/confluence/graphql/";
        final List<Class> typesInPackage = Lists.newArrayList(asList(types));

        final Collection<String> bundleResourceNames = typesInPackage.stream().map(type -> resourceNamePrefix + type.getSimpleName()).collect(Collectors.toList());
        when(bundleWiring.listResources("/", "*.class", LISTRESOURCES_LOCAL | LISTRESOURCES_RECURSE)).thenReturn(bundleResourceNames);
        for (final Class type : typesInPackage) {
            when(bundle.loadClass("com.atlassian.confluence.graphql." + type.getSimpleName())).thenReturn(type);
        }
    }

    private static DOMElement buildConfigElement() {
        final DOMElement element = new DOMElement("graphql");
        element.setAttribute("path", "root");

        final DOMElement packageElement = new DOMElement("package");
        packageElement.setNodeValue("com.atlassian.confluence.graphql");
        element.appendChild(packageElement);
        return element;
    }

    @GraphQLProvider
    public static class TestProvider {
    }

    @GraphQLProvider
    public static class TestOtherProvider {
    }

    @GraphQLExtensions
    public static class TestExtension implements com.atlassian.graphql.spi.GraphQLExtensions {
        @Override
        public String contributeTypeName(
                final String typeName,
                final Type type,
                final GraphQLTypeBuilderContext context) {
            return null;
        }

        @Override
        public void contributeFields(
                final String typeName,
                final Type type,
                final List<GraphQLFieldDefinition> fields,
                final GraphQLTypeBuilderContext context) {
        }
    }

    public static class NonGraphQLType {
    }
}
